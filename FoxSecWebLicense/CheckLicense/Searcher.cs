﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace FoxSecWebLicense
{
    public static class Searcher
    {
        public static ManagementObject First(this ManagementObjectSearcher searcher)
        {
            ManagementObject result = null;
            foreach (ManagementObject item in searcher.Get())
            {
                result = item;
                break;
            }
            return result;
        }
        //public static string ConvertStringToHex(string asciiString)
        //{
        //    string hex = "";
        //    foreach (char c in asciiString)
        //    {
        //        int tmp = c;
        //        hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
        //    }
        //    return hex;
        //}
    }
}
