﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoxSecWebLicense
{
    class Spec
    {
       // private static Native platform;
      //  private static bool licensed = false;
      //  private static int licensed;
        const string NEW_LINE = "\r\n";

        public int licensed
        {
            get;
            internal set;
        }
        //public static int licensed;
        public static int CheckLicense(int licensed)

        {



            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
          //  int licensed;
          //  string sernum = Form1.serialnember.PadRight(15, '0').Substring(0, 15);
            try
            {
                string sernum = Form1.deviceID.PadRight(15, '0').Substring(0, 15);


                string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

                if (appPath.Substring(0, 6).Equals("file:\\"))
                    appPath = appPath.Substring(6);
                Form1.licenseFilePath = Path.Combine(appPath + "\\FoxSecLicense.ini");
                string[] lines = FileReader.ReadLines(Form1.licenseFilePath);

                //  string[] lines = FileReader.ReadLines(Form1.licenseFilePath);
                for (i = 0; i < lines.Length; i++)
                {
                    line = lines[i];
                    if (line.Length > 15)
                    {
                        if (string.Compare(line.Substring(0, 16), "EncryptLicenceNr", true) == 0)
                        {
                            intEncryptLicNrIdx = i;
                            strEncryptLicNr = line.Substring(17, line.Length - 17);
                        }

                        if (string.Compare(line.Substring(0, 10), "CompUniqNr", true) == 0)
                        {
                            intUniqNrIdx = i;
                            strUniqNr = line.Substring(11, line.Length - 11);
                            if (!strUniqNr.Equals(sernum))
                            {
                                strUniqNr = sernum.ToString();
                                lines[i] = "CompUniqNr=" + strUniqNr;
                                FileWriter.WriteToFile(Form1.licenseFilePath,
                                    String.Join(NEW_LINE, lines));
                            }
                        }
                    }
                    if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                        break;
                }

                if (intEncryptLicNrIdx < 1 | intUniqNrIdx < 1)
                {
                    FileWriter.WriteDemoLicense(sernum.ToString());
                    strEncryptLicNr = "Demo";
                }

                switch (strEncryptLicNr[0])
                {
                    case 'A':
                        decryptedLicense =
                            DecryptLicense("FA9" + sernum + "F9", sernum)
                            + DecryptLicense("FA9ABCAABCA91456F9AF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 1;
                      
                        break;
                    case 'B':
                        decryptedLicense =
                            DecryptLicense("FB9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9ABCDFBCF49956F9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 2;
                       
                        break;
                    case 'C':
                        decryptedLicense =
                            DecryptLicense("FB9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9AB32FBC566956F9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 3;
                      
                        break;
                    case 'E':
                        decryptedLicense =
                            DecryptLicense("FB9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9BB3EFBC5EE956E9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 4;
                    
                        break;
                    case 'F':
                        decryptedLicense =
                            DecryptLicense("FC9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9BB355BC5EBB56E9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 5;
                      
                        break;
                    default:
                        //ReadWrite.WriteDemoLicense(sernum);
                        licensed = 0;
                        MessageBox.Show("Licence not found!\n" +
                       "Now running in demo mode\n" +
                       "OY Hardmeier\n" + "Pärnu mnt. 102\n" + "10129 Tallinn\n" + "Tel. 6661400\n" +
                       "Email: info@foxsec.eu\n");
                       
                        break;
                }
                return licensed;
            }
                 
            catch (Exception e) { MessageBox.Show(e.ToString()); }
             throw new Exception("Impossible!");
        }
        public static int CheckLicenseFlash(int licensed)


        {
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;

         
            try
            {
                string sernum = Form1.deviceID.PadRight(15, '0').Substring(0, 15);


                //string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

                //if (appPath.Substring(0, 6).Equals("file:\\"))
                //    appPath = appPath.Substring(6);
                //Form1.licenseFilePath = Path.Combine(appPath + "\\FoxSecLicense.ini");
                string[] lines = FileReader.ReadLines(Form1.licenseFilePath);

                //  string[] lines = FileReader.ReadLines(Form1.licenseFilePath);
                for (i = 0; i < lines.Length; i++)
                {
                    line = lines[i];
                    if (line.Length > 15)
                    {
                        if (string.Compare(line.Substring(0, 16), "EncryptLicenceNr", true) == 0)
                        {
                            intEncryptLicNrIdx = i;
                            strEncryptLicNr = line.Substring(17, line.Length - 17);
                        }

                        if (string.Compare(line.Substring(0, 10), "CompUniqNr", true) == 0)
                        {
                            intUniqNrIdx = i;
                            strUniqNr = line.Substring(11, line.Length - 11);
                            if (!strUniqNr.Equals(sernum))
                            {
                                strUniqNr = sernum.ToString();
                                lines[i] = "CompUniqNr=" + strUniqNr;
                                FileWriter.WriteToFile(Form1.licenseFilePath,
                                    String.Join(NEW_LINE, lines));
                            }
                        }
                    }
                    if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                        break;
                }

                if (intEncryptLicNrIdx < 1 | intUniqNrIdx < 1)
                {
                    FileWriter.WriteDemoLicense(sernum.ToString());
                    strEncryptLicNr = "Demo";
                }

                switch (strEncryptLicNr[0])
                {
                    case 'A':
                        decryptedLicense =
                            DecryptLicense("FA9" + sernum + "F9", sernum)
                            + DecryptLicense("FA9ABCAABCA91456F9AF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 1;
                 
                        break;
                    case 'B':
                        decryptedLicense =
                            DecryptLicense("FB9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9ABCDFBCF49956F9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 2;
                      
                        break;
                    case 'C':
                        decryptedLicense =
                            DecryptLicense("FB9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9AB32FBC566956F9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 3;
                   
                        break;
                    case 'E':
                        decryptedLicense =
                            DecryptLicense("FB9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9BB3EFBC5EE956E9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 4;
         
                        break;
                    case 'F':
                        decryptedLicense =
                            DecryptLicense("FC9" + sernum + "F9", sernum)
                            + DecryptLicense("FB9BB355BC5EBB56E9FF", sernum);

                        if (strEncryptLicNr.Substring(1, 20) == decryptedLicense)
                            licensed = 5;
             
                        break;
                    default:
                        //ReadWrite.WriteDemoLicense(sernum);
                        licensed = 0;
                        MessageBox.Show("Licence not found!\n" +
                       "Now running in demo mode\n" +
                       "OY Hardmeier\n" + "Pärnu mnt. 102\n" + "10129 Tallinn\n" + "Tel. 6661400\n" +
                       "Email: info@foxsec.eu\n");
                        break;
                        
                }
                return licensed;
            }
            catch (Exception e) { MessageBox.Show(e.ToString()); }
            throw new Exception("Impossible!");
        }
        /// <returns>Value of modified number</returns>
        internal static bool BitValue(UInt64 source, ushort bit)
        {
            return Convert.ToBoolean(source & (ulong)Math.Pow(2, bit));
        }
        internal static ulong ResetBit(UInt64 source, ushort bit)
        {
            if (BitValue(source, bit))
                source -= (ulong)Math.Pow(2, bit);
            return source;
        }
        internal static ulong SetBit(UInt64 source, ushort bit)
        {
            source |= (ulong)Math.Pow(2, bit);
            return source;
        }
        private static string DecryptLicense(string level, string pword)
        {
            int KasutajaLevelx12 = 0;
            int KasutajaLevelx11 = 0;
            int KasutajaLevelx13 = 0;
            int KasutajaLevelx14 = 0;
            string parool = null;
            int PswLen = 0;
            int Sala = 0;
            int[] AsciiK = new int[21];
            char[] See_chrx = new char[11];
            int j = 0;

            //
            KasutajaLevelx11 = Convert.ToInt32(level.Substring(0, 5), 16);
            KasutajaLevelx12 = Convert.ToInt32(level.Substring(5, 5), 16);
            KasutajaLevelx13 = Convert.ToInt32(level.Substring(10, 5), 16);
            KasutajaLevelx14 = Convert.ToInt32(level.Substring(15, 5), 16);

            if (pword.Length <= 10)
            {
                PswLen = pword.Length;
                parool = pword.Substring(0, 10);
            }
            else
            {
                PswLen = 10;
                parool = pword;
            }
            for (j = 1; j <= parool.Length; j++)
            {
                AsciiK[j] = System.Convert.ToInt32(parool[j - 1]);
                Sala = Sala + (((AsciiK[j] * j) % 300) * KasutajaLevelx14) % 8745 + 7 * ((((AsciiK[j] * 3 * j) % 73) * KasutajaLevelx13 + 9) % 117) + (((KasutajaLevelx12 + 87) % 32 + KasutajaLevelx11 + 7) % 69);
            }
            //Kasutame Parooli nüüd krypteeritud paroolina
            parool = "";
            for (j = 1; j <= 10; j++)
            {
                See_chrx[j] = Convert.ToChar(((Sala * (AsciiK[j] + AsciiK[1]) + PswLen * 18 * j) % 93) + 33);
                PswLen = Convert.ToInt32(See_chrx[j]);
                parool = parool + See_chrx[j];
            }
            return parool;
        }
    }
}
