﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.IO;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Reflection;
using Microsoft.Win32;
using System.Text.RegularExpressions;
using SetupWebSetup;


namespace FoxSecWebLicense
{
    public partial class Form1 : Form
    {
        private static bool appIsLocked = true;
        public static string deviceID = string.Empty;
        private const RegexOptions regexOptions =
        RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture;
        private const string dateRegexp = "(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])";
        private static readonly Regex dateRegex =
        new Regex(dateRegexp, regexOptions);
        private const string digitsRegexPattern = @"^[0-9]+$";
        private static readonly Regex digitsRegex = new Regex(digitsRegexPattern, regexOptions);
        private static readonly string CR = Environment.NewLine;
        public static string serialnember = string.Empty;
        public static string licenseFilePath = "";
        private static string appPath = string.Empty;
    
        public Form1()
        {

            //string appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

            //if (appPath.Substring(0, 6).Equals("file:\\"))
            //    appPath = appPath.Substring(6);


            //InitializeComponent();
            int licflash = 0;
         //   int lichard = 0;

            GetSerFlashDisk();
          //  GetSerHardDisk(); 
         
              
                RecognizeAppState();
                int returnlicflash = Spec.CheckLicenseFlash(licflash);
          //      int returnlichard = Spec.CheckLicense(lichard);

                switch (returnlicflash)
                {
                    case '1':

                        MessageBox.Show("Only Demo project can be used!\n" +
                       "OY Hardmeier\n" + "Pärnu mnt. 102\n" + "10129 Tallinn\n" + "Tel. 6661400\n" +
                       "Email: info@foxsec.eu\n");
                        break;
                    case 2:


                        break;
                    case 3:


                        break;
                    case 4:


                        break;
                    case 5:


                        break;
                    case 0:
                     
                        MessageBox.Show("Licence not found!\n" +
                       "Now running in demo mode\n" +
                       "OY Hardmeier\n" + "Pärnu mnt. 102\n" + "10129 Tallinn\n" + "Tel. 6661400\n" +
                       "Email: info@foxsec.eu\n");

                        break;
                }

     

        }
        public string identifier(string wmiClass, string wmiProperty)
        //Return a hardware identifier
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }
        //private void GetAllDiskDrives()
        //{
        //    string drive = "C";
        //    ManagementObject disk = new ManagementObject("win32_logicaldisk.deviceid=\"" + drive + ":\"");
        //    disk.Get();
        //    string sernum = disk["VolumeSerialNumber"].ToString();
        //    string sz = disk["Size"].ToString();
        //    string md = disk["Model"].ToString();
        // //   Label3.Text = "VolumeSerialNumber=" + disk["VolumeSerialNumber"].ToString();

        //    var searcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");

        //    foreach (ManagementObject wmi_HD in searcher.Get())
        //    {
        //        HardDrive hd = new HardDrive();
        //        hd.Model = wmi_HD["Model"].ToString();
        //        hd.InterfaceType = wmi_HD["InterfaceType"].ToString();
        //        hd.Caption = wmi_HD["Caption"].ToString();

        //        hd.SerialNo = wmi_HD.GetPropertyValue("SerialNumber").ToString();//get the serailNumber of diskdrive

        //       // hdCollection.Add(hd);
        //    }

        //}
        public void GetSerFlashDisk() 
        {
            string diskName = string.Empty;
            string testser = string.Empty;
            var numint = string.Empty;
            StringBuilder volumename = new StringBuilder(256);
            //listBox1.Items.Clear();
            //listBox2.Items.Clear();
       
            try
            {
                foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
                {

                    // ManagementObject partition = new ManagementObjectSearcher(String.Format("associators of {{Win32_DiskDrive.DeviceID='{0}'}} where AssocClass = Win32_DiskDriveToDiskPartition", drive["DeviceID"])).First();
                    foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
                    {

                        foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
                        {

                            diskName = disk["Name"].ToString().Trim();
                            testser = disk["VolumeSerialNumber"].ToString().Trim();
                            numint = Convert.ToInt64(testser, 16).ToString();

                            //  MessageBox.Show(testser);
                        }

                        licenseFilePath = Path.Combine(diskName + "//FoxSecLicense.ini");
                        if (File.Exists(licenseFilePath))
                        {
                            if (partition != null)
                            {
                                // associate partitions with logical disks (drive letter volumes)
                                ManagementObject logical = new ManagementObjectSearcher(String.Format(
                                    "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
                                    partition["DeviceID"])).First();

                                if (logical != null)
                                {
                                    List<string> list = new List<string>();

                                    ManagementObject volume = new ManagementObjectSearcher(String.Format(
                                        "select FreeSpace, Size from Win32_LogicalDisk where Name='{0}'",
                                        logical["Name"])).First();

                                    UsbDisk disk = new UsbDisk(logical["Name"].ToString());
                                    //  listBox1.Items.Add("Letter=" + diskName);

                                    //  disk.Volume = volume["VolumeName"].ToString();

                                    DriveInfo[] allDrives = DriveInfo.GetDrives();
                                    foreach (DriveInfo d in allDrives)
                                    {
                                        string namedisk = diskName + @"\";
                                        string name = d.Name;
                                        if (namedisk == name)
                                        {
                                            if (d.IsReady == true)
                                            {
                                                d.VolumeLabel = "FoxSec";
                                            }
                                        }

                                    }
                                    //String Manufacturer = drive["Manufacturer"].ToString().Trim();
                                    //listBox1.Items.Add("Manufacturer:" + Manufacturer);

                                    string pnpdeviceid = parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim());
                                    //   listBox1.Items.Add("pnpdeviceid=" + pnpdeviceid);
                                    var conpnp = pnpdeviceid.Substring(0, 3);

                                    var pnpdevidint = Convert.ToUInt64(conpnp, 16).ToString();

                                    list.Add(pnpdevidint.Substring(0, 3));


                                    //   listBox1.Items.Add("pnpdeviceid=" + pnpdevidint.Substring(0, 3));
                                    //StringBuilder builder2 = new StringBuilder();
                                    //foreach (string cat in list) // Loop through all strings
                                    //{
                                    //    builder2.Append(cat).Append(""); // Append string to StringBuilder
                                    //}
                                    //string result122 = builder2.ToString();
                                    //string volum = disk.Volume.ToString();
                                    //volum = volume["VolumeName"].ToString();

                                    //   var volnumint = Convert.ToInt32(disk.Volume);

                                    //byte[] ba = System.Text.Encoding.ASCII.GetBytes(disk.Volume);
                                    //string ba0=ba[0].ToString();
                                    //string ba1= ba[1].ToString();
                                    //string ba2 = ba[2].ToString();

                                    //int intba0 = Convert.ToInt32(ba0) % 10;
                                    //int intba1 = Convert.ToInt32(ba1) % 10;
                                    //int intba2 = Convert.ToInt32(ba2) % 10;
                                    //string intstrba0 = intba0.ToString();
                                    //string intstrba1 = intba1.ToString();
                                    //string intstrba2 = intba2.ToString();

                                    //list.Add(intstrba0);
                                    //list.Add(intstrba1);
                                    //list.Add(intstrba2);

                                    //StringBuilder builder1 = new StringBuilder();
                                    //foreach (string cat in list) 
                                    //{
                                    //    builder1.Append(cat).Append(""); 
                                    //}
                                    //string result1 = builder1.ToString();

                                    //    listBox1.Items.Add("Volume Name=" + result1);


                                    disk.Size = (ulong)volume["Size"];
                                    string size = disk.Size.ToString();
                                    size = volume["Size"].ToString();
                                    list.Add(size.Substring(0, 4));

                                    //  listBox1.Items.Add("Size=" + size.Substring(0, 4));

                                    //  string serial = drive["SerialNumber"].ToString().Trim();

                                    //    var numint = Convert.ToInt64(serial, 16).ToString();


                                    list.Add(numint.Substring(2, 8));
                                    StringBuilder builder = new StringBuilder();
                                    foreach (string cat in list)
                                    {
                                        builder.Append(cat).Append("");
                                    }
                                    string result = builder.ToString();

                                    //    listBox1.Items.Add("Serial number=" + numint.Substring(2, 8));
                                    //listBox2.Items.Add(result);
                                    deviceID = result;

                                }

                              //  WriteSerialNumber(serialnember.ToString());
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
            if (deviceID == string.Empty) 
            { 
               // MessageBox.Show("Not found any flash disk drives!!!"); 
            }
        }
       
        public void GetSerHardDisk()
        {
            try
            {
                List<string> list = new List<string>();
                string model = identifier("Win32_DiskDrive", "Model");

                byte[] ba = System.Text.Encoding.ASCII.GetBytes(model);
                string ba0 = ba[0].ToString();
                string ba1 = ba[1].ToString();
                string ba2 = ba[2].ToString();

                int intba0 = Convert.ToInt32(ba0) % 10;
                int intba1 = Convert.ToInt32(ba1) % 10;
                int intba2 = Convert.ToInt32(ba2) % 10;
                string intstrba0 = intba0.ToString();
                string intstrba1 = intba1.ToString();
                string intstrba2 = intba2.ToString();
                // List<string> listba = new List<string>();
                list.Add(intstrba0);
                list.Add(intstrba1);
                list.Add(intstrba2);
                //StringBuilder builder1 = new StringBuilder();
                //foreach (string cat in list) // Loop through all strings
                //{
                //    builder1.Append(cat).Append(""); // Append string to StringBuilder
                //}
                //string result1 = builder1.ToString();

                //list.Add(result1.Substring(0, 3));
                string name = identifier("Win32_LogicalDisk", "Name");

                //  licenseFilePath = Path.Combine(name + "//FoxSecLicense.ini");
                string Size = identifier("Win32_DiskDrive", "Size");
                list.Add(Size.Substring(0, 4));
                string serial = identifier("Win32_DiskDrive", "SerialNumber");

                var numint = serial.Substring(2, 8);

                var numint1 = Convert.ToInt64(numint, 16).ToString();

                list.Add(numint1.Substring(0, 8));
                //   list.Add(numint.Substring(2, 8));
                StringBuilder builder = new StringBuilder();
                foreach (string cat in list) // Loop through all strings
                {
                    builder.Append(cat).Append(""); // Append string to StringBuilder
                }
                string result = builder.ToString();
                deviceID = result;
                //listBox1.Items.Add(deviceID);
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
        }
   

        
        //private static void GetSerialHard()
        //{
        //    ManagementObject disk1 = new ManagementObject("win32_logicaldisk.deviceid=\"c:\"");
        //    disk1.Get();
        //    Console.WriteLine("Logical Disk Size = " + disk1["Size"] + " bytes");
        //    //Console.WriteLine("Logical Disk FreeSpace = " + disk1["FreeSpace"] + "bytes");

        //    string diskName = string.Empty;
        //    StringBuilder volumename = new StringBuilder(256);
          

        //    foreach (System.Management.ManagementObject drive in new System.Management.ManagementObjectSearcher("select DeviceID, SerialNumber, Model from Win32_DiskDrive").Get())
        //    {

        //        try
        //        {
        //            // ManagementObject partition = new ManagementObjectSearcher(String.Format("associators of {{Win32_DiskDrive.DeviceID='{0}'}} where AssocClass = Win32_DiskDriveToDiskPartition", drive["DeviceID"])).First();
        //            foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
        //            {


        //                foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
        //                {

        //                    diskName = disk["Name"].ToString().Trim();


        //                    //  MessageBox.Show(diskName + "Exist");

        //                }

        //                licenseFilePath = Path.Combine(diskName + "//FoxSecLicense.ini");
        //                //  if (File.Exists(licenseFilePath))
        //                // {
        //                if (partition != null)
        //                {
        //                    // associate partitions with logical disks (drive letter volumes)
        //                    ManagementObject logical = new ManagementObjectSearcher(String.Format(
        //                        "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
        //                        partition["DeviceID"])).First();

        //                    if (logical != null)
        //                    {
        //                        List<string> list = new List<string>();
        //                        // finally find the logical disk entry to determine the volume name
        //                        ManagementObject volume = new ManagementObjectSearcher(String.Format(
        //                            "select FreeSpace, Size, VolumeName from Win32_LogicalDisk where Name='{0}'",
        //                            logical["Name"])).First();

        //                        UsbDisk disk = new UsbDisk(logical["Name"].ToString());
        //                      //  listBox1.Items.Add("Letter=" + diskName);
        //                        //  disk.Model = drive["Model"].ToString();
        //                        // listBox1.Items.Add(disk.Model);
        //                        disk.Volume = volume["VolumeName"].ToString();
        //                        //string volum = disk.Volume.ToString();
        //                        //volum = volume["VolumeName"].ToString();

        //                        //   var volnumint = Convert.ToInt32(disk.Volume);

        //                        //   byte[] ba = System.Text.Encoding.ASCII.GetBytes(disk.Volume);
        //                        //   string ba0 = ba[0].ToString();
        //                        //   string ba1 = ba[1].ToString();
        //                        //   string ba2 = ba[2].ToString();

        //                        //  int intba0 = Convert.ToInt32(ba0) % 10;
        //                        //    int intba1 = Convert.ToInt32(ba1) % 10;
        //                        //   int intba2 = Convert.ToInt32(ba2) % 10;
        //                        //    string intstrba0 = intba0.ToString();
        //                        //    string intstrba1 = intba1.ToString();
        //                        //    string intstrba2 = intba2.ToString();
        //                        //  List<string> listba = new List<string>();
        //                        //   list.Add(intstrba0);
        //                        //  list.Add(intstrba1);
        //                        //    list.Add(intstrba2);
        //                        // Console.WriteLine(intba0 % 10);
        //                        //StringBuilder builder1 = new StringBuilder();
        //                        //foreach (string cat in list) // Loop through all strings
        //                        //{
        //                        //    builder1.Append(cat).Append(""); // Append string to StringBuilder
        //                        //}
        //                        //string result1 = builder1.ToString();

        //                        ////   listba.Add(result1.Substring(0, 3));

        //                        //listBox1.Items.Add("Volume Name=" + result1);

        //                        //// disk.FreeSpace = (ulong)volume["FreeSpace"];
        //                        ////  listBox1.Items.Add(disk.FreeSpace);

        //                        //disk.Size = (ulong)volume["Size"];
        //                        //string size = disk.Size.ToString();
        //                        //size = volume["Size"].ToString();
        //                        //list.Add(size.Substring(0, 4));
        //                        ////  string result;
        //                        //// list.Add(result.Substring(0, 3));
        //                        //listBox1.Items.Add("Size=" + disk.Size);
        //                        //    disks.Add(disk);

        //                        string serial = drive["SerialNumber"].ToString().Trim();

        //                        var numint = Convert.ToInt64(serial, 16).ToString();


        //                        list.Add(numint.Substring(2, 8));
        //                        StringBuilder builder = new StringBuilder();
        //                        foreach (string cat in list) // Loop through all strings
        //                        {
        //                            builder.Append(cat).Append(""); // Append string to StringBuilder
        //                        }
        //                        string result = builder.ToString();



        //                   //     listBox2.Items.Add(result);
        //                        serialnember = result;
        //                        //  list.Add(serial.Substring(8, 4));
        //                     //   if (serial.Length > 1) //listBox1.Items.Add("Serial number=\n" + numint);
        //                     //   else


        //                          //  listBox1.Items.Add("Serial number=\n" + parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim()));
        //                        //  }
        //                 //       listBox1.Items.Add(CR);

        //                    }

        //                }
        //                //     else
        //                //         MessageBox.Show("Licence not found!\n" +
        //                //"Now running in demo mode\n" +
        //                //"OY Hardmeier\n" + "Pärnu mnt. 102\n" + "10129 Tallinn\n" + "Tel. 6661400\n" +
        //                //"Email: info@foxsec.eu\n");
        //                //    }

        //                //listBox1.Items.Add("Модель=" + drive["Model"]);
        //                //listBox1.Items.Add("Ven=" + parseVenFromDeviceID(drive["PNPDeviceID"].ToString().Trim()));
        //                //listBox1.Items.Add("Prod=" + parseProdFromDeviceID(drive["PNPDeviceID"].ToString().Trim()));
        //                //listBox1.Items.Add("Rev=" + parseRevFromDeviceID(drive["PNPDeviceID"].ToString().Trim()));


        //                //string serial = drive["SerialNumber"].ToString().Trim();
        //                //string model = drive["Model"].ToString().Trim();

        //                //if (serial.Length > 1) listBox1.Items.Add("Serial number=" + serial);
        //                //else


        //                //    listBox1.Items.Add("Serial number=" + parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim()));


        //                //if (model.Length > 1) listBox1.Items.Add("Model=" + model);
        //                //else
        //                //    listBox1.Items.Add("Model=" + parseProdFromDeviceID(drive["PNPDeviceID"].ToString().Trim()));

        //            }
        //            //catch
        //        }
        //        catch (Exception ex)
        //        {

        //        }

        //    }
        //}
        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        [return: MarshalAs(UnmanagedType.Bool)]
        protected extern static bool GetVolumeInformation(
          string RootPathName,
          StringBuilder VolumeNameBuffer,
          int VolumeNameSize,
          out uint VolumeSerialNumber,
          out uint MaximumComponentLength,
          out uint FileSystemFlags,
          StringBuilder FileSystemNameBuffer,
          int nFileSystemNameSize);
       
        
        //internal static void GetDeviceID()
        //{
        //    StringBuilder volname = new StringBuilder(256);
        //    StringBuilder fsname = new StringBuilder(256);
        //    uint sernum, maxlen, flags;
        //    if (!GetVolumeInformation(
        //        @"c:\",
        //        volname, volname.Capacity, out sernum, out maxlen, out flags, fsname, fsname.Capacity))
        //        Marshal.ThrowExceptionForHR(Marshal.GetLastWin32Error());
        //    deviceID = sernum.ToString();

        //}

        protected static void RecognizeAppState()
        {
            try
            {
                string rTime = "";
                if (Registry.GetValue("HKEY_LOCAL_MACHINE\\System\\Inith", "1", null) == null)
                {
                    rTime = null;
                }
                else
                {
                    rTime = Registry.GetValue("HKEY_LOCAL_MACHINE\\System\\Inith", "1", null).ToString();
                }
                //Try
                //    rTime = Registry.GetValue("HKEY_LOCAL_MACHINE\System\Inith", "1", Nothing).ToString
                //Catch ex As NullReferenceException
                //    rTime = Nothing
                //End Try
                //kontrollime kas aeg on õiges formaadis(pikkus vähemalt
                //10, sisaldab ainult numbrid) ja paneme õigesse järjekorda DDHHYYMiMiMoMo -> YYMoMoDDHHMiMi
                if (!String.IsNullOrEmpty(rTime))
                {
                    if (rTime.Length > 9)
                    {
                        if (Digits(rTime))
                        {
                            if (ValidateDate(String.Concat(
                                "20" + rTime.Substring(4, 2), "-",
                                rTime.Substring(8, 2), "-",
                                rTime.Substring(0, 2))))
                            {
                                rTime =
                                   "20" + rTime.Substring(4, 2) +
                                   rTime.Substring(8, 2) +
                                   rTime.Substring(0, 2) +
                                   rTime.Substring(2, 2) +
                                   rTime.Substring(6, 2);
                                //yy = ToInt32(rTime.Substring(4, 2))
                                //mm = ToInt32(rTime.Substring(8, 2))
                                //dd = ToInt32(rTime.Substring(0, 2))
                                //mi = ToInt32(rTime.Substring(6, 2))
                                //hh = ToInt32(rTime.Substring(2, 2))
                            }
                            else
                                return;
                        }
                    }
                    else
                    {
                        return;
                    }

                    DateTime currentTime = DateTime.Now;
                    DateTime rTimeDate = DateTime.ParseExact(rTime, "yyyyMMddHHmm", null);
                    TimeSpan span = currentTime.Subtract(rTimeDate);

                    if (span.TotalHours > 2)
                        appIsLocked = false;
                    else
                        appIsLocked = true;
                }
                else
                {
                    //siia tuleb kontroll , kas registris on kirje et oli lukustatud.
                    //kui selline kirje on ja rTime=Nothing siis on võimalik et registrist kustutati rTime
                    appIsLocked = false;
                }
            }
          catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
        }
        internal static bool ValidateDate(string yyyyMMdd)
        {
            return dateRegex.IsMatch(yyyyMMdd);
        }
        internal static bool Digits(params string[] values)
        {
            bool valid = ValidateString(digitsRegex, values);
            return valid;
        }
        internal static bool ValidateString(Regex r, params string[] values)
        {
            if (r == null)
                return false;

            bool valid = true;
            foreach (string s in values)
            {
                valid &= r.IsMatch(s);
                if (!valid)
                    return false;
            }
            return valid;
        }
        //private void button1_Click(object sender, EventArgs e)
        //{
        //    string diskName = string.Empty;
        //    string testser = string.Empty;
        //    var numint = string.Empty;
        //    StringBuilder volumename = new StringBuilder(256);
        //    //listBox1.Items.Clear();
        //    //listBox2.Items.Clear();
        //    //foreach (ManagementObject drive in
        //    //new ManagementObjectSearcher(
        //    //    "select DeviceID, Model from Win32_DiskDrive where InterfaceType='USB'").Get())
        //    //{
        //    //    // associate physical disks with partitions

        //    //}
        //    try
        //    {
        //        foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
        //        {

        //            // ManagementObject partition = new ManagementObjectSearcher(String.Format("associators of {{Win32_DiskDrive.DeviceID='{0}'}} where AssocClass = Win32_DiskDriveToDiskPartition", drive["DeviceID"])).First();
        //            foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
        //            {

        //                foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
        //                {

        //                    diskName = disk["Name"].ToString().Trim();
        //                    testser = disk["VolumeSerialNumber"].ToString().Trim();
        //                    numint = Convert.ToInt64(testser, 16).ToString();

        //                    //  MessageBox.Show(testser);
        //                }

        //                licenseFilePath = Path.Combine(diskName + "//FoxSecLicense.ini");
        //                // if (File.Exists(licenseFilePath))
        //                //  {
        //                if (partition != null)
        //                {
        //                    // associate partitions with logical disks (drive letter volumes)
        //                    ManagementObject logical = new ManagementObjectSearcher(String.Format(
        //                        "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
        //                        partition["DeviceID"])).First();

        //                    if (logical != null)
        //                    {
        //                        List<string> list = new List<string>();

        //                        ManagementObject volume = new ManagementObjectSearcher(String.Format(
        //                            "select FreeSpace, Size from Win32_LogicalDisk where Name='{0}'",
        //                            logical["Name"])).First();

        //                        UsbDisk disk = new UsbDisk(logical["Name"].ToString());
        //                        listBox1.Items.Add("Letter=" + diskName);

        //                        //  disk.Volume = volume["VolumeName"].ToString();

        //                        DriveInfo[] allDrives = DriveInfo.GetDrives();
        //                        foreach (DriveInfo d in allDrives)
        //                        {
        //                            string namedisk = diskName + @"\";
        //                            string name = d.Name;
        //                            if (namedisk == name)
        //                            {
        //                                if (d.IsReady == true)
        //                                {
        //                                    d.VolumeLabel = "FoxSec";
        //                                }
        //                            }

        //                        }
        //                        //String Manufacturer = drive["Manufacturer"].ToString().Trim();
        //                        //listBox1.Items.Add("Manufacturer:" + Manufacturer);

        //                        string pnpdeviceid = parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim());
        //                        //   listBox1.Items.Add("pnpdeviceid=" + pnpdeviceid);
        //                        var conpnp = pnpdeviceid.Substring(0, 3);

        //                        var pnpdevidint = Convert.ToUInt64(conpnp, 16).ToString();

        //                        list.Add(pnpdevidint.Substring(0, 3));


        //                        listBox1.Items.Add("pnpdeviceid=" + pnpdevidint.Substring(0, 3));
        //                        //StringBuilder builder2 = new StringBuilder();
        //                        //foreach (string cat in list) // Loop through all strings
        //                        //{
        //                        //    builder2.Append(cat).Append(""); // Append string to StringBuilder
        //                        //}
        //                        //string result122 = builder2.ToString();
        //                        //string volum = disk.Volume.ToString();
        //                        //volum = volume["VolumeName"].ToString();

        //                        //   var volnumint = Convert.ToInt32(disk.Volume);

        //                        //byte[] ba = System.Text.Encoding.ASCII.GetBytes(disk.Volume);
        //                        //string ba0=ba[0].ToString();
        //                        //string ba1= ba[1].ToString();
        //                        //string ba2 = ba[2].ToString();

        //                        //int intba0 = Convert.ToInt32(ba0) % 10;
        //                        //int intba1 = Convert.ToInt32(ba1) % 10;
        //                        //int intba2 = Convert.ToInt32(ba2) % 10;
        //                        //string intstrba0 = intba0.ToString();
        //                        //string intstrba1 = intba1.ToString();
        //                        //string intstrba2 = intba2.ToString();

        //                        //list.Add(intstrba0);
        //                        //list.Add(intstrba1);
        //                        //list.Add(intstrba2);

        //                        //StringBuilder builder1 = new StringBuilder();
        //                        //foreach (string cat in list) 
        //                        //{
        //                        //    builder1.Append(cat).Append(""); 
        //                        //}
        //                        //string result1 = builder1.ToString();

        //                        //    listBox1.Items.Add("Volume Name=" + result1);


        //                        disk.Size = (ulong)volume["Size"];
        //                        string size = disk.Size.ToString();
        //                        size = volume["Size"].ToString();
        //                        list.Add(size.Substring(0, 4));

        //                        listBox1.Items.Add("Size=" + size.Substring(0, 4));

        //                        //  string serial = drive["SerialNumber"].ToString().Trim();

        //                        //    var numint = Convert.ToInt64(serial, 16).ToString();


        //                        list.Add(numint.Substring(2, 8));
        //                        StringBuilder builder = new StringBuilder();
        //                        foreach (string cat in list)
        //                        {
        //                            builder.Append(cat).Append("");
        //                        }
        //                        string result = builder.ToString();

        //                        listBox1.Items.Add("Serial number=" + numint.Substring(2, 8));
        //                        listBox2.Items.Add(result);
        //                        serialnember = result;

        //                    }

        //                    WriteSerialNumber(serialnember.ToString());
        //                }

        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());

        //    }
        //    if (listBox1.Items.Count == 0) { MessageBox.Show("Not found any flash disk drives!!!"); }
     
        //}
        //private void CheckLisenceCurrentDirectory()
        //{

        //    appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

        //    if (appPath.Substring(0, 6).Equals("file:\\"))
        //        appPath = appPath.Substring(6);
        //    string diskName = string.Empty;
        //    string testser = string.Empty;
        //    var numint = string.Empty;
        //    StringBuilder volumename = new StringBuilder(256);
        //    listBox1.Items.Clear();
        //    listBox2.Items.Clear();
        //    //foreach (ManagementObject drive in
        //    //new ManagementObjectSearcher(
        //    //    "select DeviceID, Model from Win32_DiskDrive where InterfaceType='USB'").Get())
        //    //{
        //    //    // associate physical disks with partitions

        //    //}
        //    try
        //    {
        //        foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
        //        {



        //            // ManagementObject partition = new ManagementObjectSearcher(String.Format("associators of {{Win32_DiskDrive.DeviceID='{0}'}} where AssocClass = Win32_DiskDriveToDiskPartition", drive["DeviceID"])).First();
        //            foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
        //            {

        //                foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
        //                {

        //                    diskName = disk["Name"].ToString().Trim();
        //                    testser = disk["VolumeSerialNumber"].ToString().Trim();
        //                    numint = Convert.ToInt64(testser, 16).ToString();



        //                    //  MessageBox.Show(testser);
        //                }

        //                licenseFilePath = Path.Combine(diskName + "//FoxSecLicense.ini");
        //                 if (File.Exists(licenseFilePath))
        //                  {
        //                if (partition != null)
        //                {
        //                    // associate partitions with logical disks (drive letter volumes)
        //                    ManagementObject logical = new ManagementObjectSearcher(String.Format(
        //                        "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
        //                        partition["DeviceID"])).First();

        //                    if (logical != null)
        //                    {
        //                        List<string> list = new List<string>();

        //                        ManagementObject volume = new ManagementObjectSearcher(String.Format(
        //                            "select FreeSpace, Size from Win32_LogicalDisk where Name='{0}'",
        //                            logical["Name"])).First();

        //                        UsbDisk disk = new UsbDisk(logical["Name"].ToString());
        //                        listBox1.Items.Add("Letter=" + diskName);

        //                        //  disk.Volume = volume["VolumeName"].ToString();

        //                        DriveInfo[] allDrives = DriveInfo.GetDrives();
        //                        foreach (DriveInfo d in allDrives)
        //                        {
        //                            string namedisk = diskName + @"\";
        //                            string name = d.Name;
        //                            if (namedisk == name)
        //                            {
        //                                if (d.IsReady == true)
        //                                {
        //                                    d.VolumeLabel = "FoxSec";
        //                                }
        //                            }


        //                        }
        //                        //String Manufacturer = drive["Manufacturer"].ToString().Trim();
        //                        //listBox1.Items.Add("Manufacturer:" + Manufacturer);

        //                        string pnpdeviceid = parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim());
        //                        //   listBox1.Items.Add("pnpdeviceid=" + pnpdeviceid);
        //                        var conpnp = pnpdeviceid.Substring(0, 3);

        //                        var pnpdevidint = Convert.ToUInt64(conpnp, 16).ToString();

        //                        list.Add(pnpdevidint.Substring(0, 3));


        //                        listBox1.Items.Add("pnpdeviceid=" + pnpdevidint.Substring(0, 3));
        //                        //StringBuilder builder2 = new StringBuilder();
        //                        //foreach (string cat in list) // Loop through all strings
        //                        //{
        //                        //    builder2.Append(cat).Append(""); // Append string to StringBuilder
        //                        //}
        //                        //string result122 = builder2.ToString();
        //                        //string volum = disk.Volume.ToString();
        //                        //volum = volume["VolumeName"].ToString();

        //                        //   var volnumint = Convert.ToInt32(disk.Volume);

        //                        //byte[] ba = System.Text.Encoding.ASCII.GetBytes(disk.Volume);
        //                        //string ba0=ba[0].ToString();
        //                        //string ba1= ba[1].ToString();
        //                        //string ba2 = ba[2].ToString();

        //                        //int intba0 = Convert.ToInt32(ba0) % 10;
        //                        //int intba1 = Convert.ToInt32(ba1) % 10;
        //                        //int intba2 = Convert.ToInt32(ba2) % 10;
        //                        //string intstrba0 = intba0.ToString();
        //                        //string intstrba1 = intba1.ToString();
        //                        //string intstrba2 = intba2.ToString();

        //                        //list.Add(intstrba0);
        //                        //list.Add(intstrba1);
        //                        //list.Add(intstrba2);

        //                        //StringBuilder builder1 = new StringBuilder();
        //                        //foreach (string cat in list) 
        //                        //{
        //                        //    builder1.Append(cat).Append(""); 
        //                        //}
        //                        //string result1 = builder1.ToString();



        //                        //    listBox1.Items.Add("Volume Name=" + result1);


        //                        disk.Size = (ulong)volume["Size"];
        //                        string size = disk.Size.ToString();
        //                        size = volume["Size"].ToString();
        //                        list.Add(size.Substring(0, 4));

        //                        listBox1.Items.Add("Size=" + size.Substring(0, 4));




        //                        //  string serial = drive["SerialNumber"].ToString().Trim();

        //                        //    var numint = Convert.ToInt64(serial, 16).ToString();


        //                        list.Add(numint.Substring(2, 8));
        //                        StringBuilder builder = new StringBuilder();
        //                        foreach (string cat in list) // Loop through all strings
        //                        {
        //                            builder.Append(cat).Append(""); // Append string to StringBuilder
        //                        }
        //                        string result = builder.ToString();


        //                        listBox1.Items.Add("Serial number=" + numint.Substring(2, 8));
        //                        listBox2.Items.Add(result);
        //                        serialnember = result;
        //                        //  list.Add(serial.Substring(8, 4));
        //                        //if (serial.Length > 1) listBox1.Items.Add("Serial number=\n" + numint);
        //                        //else


        //                        //    listBox1.Items.Add("Serial number=\n" + parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim()));
        //                    }
        //                    //  listBox1.Items.Add(CR);
        //                //    WriteSerialNumber(serialnember.ToString());
        //                    }
        //                }

        //            }







        //            //   }


        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());

        //    }
        //    if (listBox1.Items.Count == 0) { MessageBox.Show("Not found any flash disk drives!!!"); }
        //}
        internal const string LICENSE_SERNUM =
           "[Users]\r\nCompUniqNr={0}\r\n";
        private static object l = new object();
        internal static void WriteSerialNumber(string sernum)
        {

            string backup = licenseFilePath + ".copy";
            if (File.Exists(backup))
                File.Delete(backup);
            if (File.Exists(licenseFilePath))
                File.Move(licenseFilePath, backup);
            string data = String.Format(LICENSE_SERNUM, serialnember);
            WriteToFile(licenseFilePath, data);
        }
        internal static void WriteToFile(string path, string arg)
        {
            bool append = false;
            WriteToFile(path, arg, append);
        }
        internal static void WriteToFile(string path, string arg, bool append)
        {
            lock (l)
            {
                StreamWriter sw = new StreamWriter(path, append);
                sw.WriteLine(arg);
                sw.Close();
            }
        }
        public string displayMembers(List<String> vegetables)
        {
            foreach (String s in vegetables)
            {
                return s.ToString();
            }
            return null;
        }
        private string parseSerialFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string[] serialArray; string serial;
            int arrayLen = splitDeviceId.Length - 1;
            serialArray = splitDeviceId[arrayLen].Split('&');
            serial = serialArray[0]; return serial;
        }
        private string parseVenFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string Ven;
            //Разбиваем строку на несколько частей.    
            //Каждая чаcть отделяется по символу &  
            string[] splitVen = splitDeviceId[1].Split('&');
            Ven = splitVen[1].Replace("VEN_", "");
            Ven = Ven.Replace("_", " "); return Ven;
        }
        private string parseProdFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string Prod;
            //Разбиваем строку на несколько частей.     //Каждая чаcть отделяется по символу & 
            string[] splitProd = splitDeviceId[1].Split('&');
            Prod = splitProd[2].Replace("PROD_", ""); ;
            Prod = Prod.Replace("_", " "); return Prod;
        }
        private string parseRevFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string Rev;     //Разбиваем строку на несколько частей.     //Каждая чаcть отделяется по символу &   
            string[] splitRev = splitDeviceId[1].Split('&');
            Rev = splitRev[3].Replace("REV_", ""); ;
            Rev = Rev.Replace("_", " ");
            return Rev;
        }

      

    }
}
