﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using System.Windows.Forms;

//namespace SetupWebFoxSec1
//{
//    static class Program
//    {
//        /// <summary>
//        /// The main entry point for the application.
//        /// </summary>
//        [STAThread]
//        static void Main()
//        {
//            Application.EnableVisualStyles();
//            Application.SetCompatibleTextRenderingDefault(false);
//            Application.Run(new Welcome());
//        }    
//    }

using FoxSecWebSetup.License;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.ServiceProcess;
using System.Windows.Forms;
using SetupWebSetup;
using FoxSecWebSetup;
using FoxSecWebSetup.Conf;
using System.IO;

namespace WindowsFormsApplication1
{
    static class Program
    {
        public static bool Repair;

        // public static string Repair="";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);

            if (hasAdministrativeRight == false)
            {
                ProcessStartInfo processInfo = new ProcessStartInfo();
                processInfo.Verb = "runas"; //admin 
                processInfo.FileName = Application.ExecutablePath;
                try
                {
                    Process.Start(processInfo);
                }
                catch (Win32Exception)
                {

                }
                Application.Exit();
            }
            else //if admin, start
            {
                // To copy all the files in one directory to another directory. 
                // Get the files in the source folder. (To recursively iterate through 
                // all subfolders under the current directory, see 
                // "How to: Iterate Through a Directory Tree.")
                // Note: Check for target path was performed previously 
                //       in this code example. 

                string serviceName = "FoxSecWebService";
                ServiceController[] services = ServiceController.GetServices();
                var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
                if (service != null)
                {
                    // InstallConf6.StartIExplorer();

                    //  InstallConf6.ExecuteCommand(1000);
                    // InstallConf6.istallConf();
                    //   Conf.UpdateUsers();

                    // Check.GetWinName();
                    Repair = true;
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Change());

                    // InstallConf6.istallConf();
                    //Application.Run(new Sql());
                    // Application.Run(new InstallConf6());
                }
                else
                {

                    Repair = false;
                    // Conf.UpdateUsers();
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    // Activation form4 = new Activation();


                    Application.Run(new Welcome());
                    // Application.Run(new Sql());

                    // Application.Run(new Activation());
                }


            }
        }

        //static void Main()
        //{
        //    WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
        //    bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);

        //    if (hasAdministrativeRight == false)
        //    {
        //        ProcessStartInfo processInfo = new ProcessStartInfo();
        //        processInfo.Verb = "runas"; //admin 
        //        processInfo.FileName = Application.ExecutablePath;
        //        try
        //        {
        //            Process.Start(processInfo);
        //        }
        //        catch (Win32Exception)
        //        {

        //        }
        //        Application.Exit();
        //    }
        //    else //if admin, start
        //    {
        //        Application.Run(new Form1());

        //    }
        //}

    }
}
