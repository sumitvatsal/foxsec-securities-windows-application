﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Compression;
using Microsoft.SqlServer.Management.Smo;
using System.Data.SqlClient;
using System.IO;
using Microsoft.SqlServer.Management.Common;
using System.Reflection;
using System.Data.Sql;
using System.Diagnostics;
using Microsoft.Win32;
using System.Security.Cryptography;
using System.Xml;
using System.Web;
using System.Configuration;
using System.Web.Configuration;
using FoxSecWebSetup;
using System.Runtime.InteropServices;
using Microsoft.SqlServer.Management.Smo.Wmi;
using FoxSecWebSetup.Conf;
using System.Data.OleDb;
using WindowsFormsApplication1;
using System.Threading;
using System.Text.RegularExpressions;
using FoxSecWebLicense;
using System.Xml.Linq;
using System.Management;
using System.ServiceProcess;
using FoxSecWebSetup.License;
using System.Security.Principal;
using System.Management.Automation;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Web.Administration;
using System.Net;
using System.Net.NetworkInformation;

namespace SetupWebSetup
{
    public partial class Sql : Form
    {
        public DateTime saveNow = DateTime.Now;
        public string msg = "";
        public string licenseFilePath = string.Empty;
        private static string HTTPSInstallation = string.Empty;
        private static string CertName = string.Empty;

        public Sql()
        {
            InitializeComponent();
            checkcertexist();// this method is used to check that makecert.exe which is used to create self signed certificate exist or not 
            //try
            //{
            //    string appPath = AppDomain.CurrentDomain.BaseDirectory;
            //    string zipPath1 = System.IO.Path.Combine(appPath + "FoxSecDb.zip");

            //    //string extractPath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA";
            //    //string deletePath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FoxSecDb";
            //    string extractPath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin");
            //    //       string deletePath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin\FoxSecDb");
            //    //  Directory.Delete(deletePath1, true); //true - если директория не пуста (удалит и файлы и папки)
            //    //  Directory.CreateDirectory(deletePath1);

            //    System.IO.Compression.ZipFile.ExtractToDirectory(zipPath1, extractPath1);

            //}
            //catch (Exception ex)
            //{
            //   // string msg = ex.ToString();
            //   //MessageBox.Show(ex.ToString());
            //    //   Finish.lgcheck1 = msg;
            //   FileWriter.UpdateEventsLogFile(saveNow + msg + ex.Message);
            //}


            // createSqlLogin();
            string icon = "icon1.ico";
            this.Icon = new Icon(icon, 60, 60);
            this.Text = "FoxSecWeb Installer";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            //   if (Change.Replace == "Yes")
            //{

            //    var currentpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\");
            //    var connectionweb = DBConnectionSettings.ReadWebXMLConnectionString(currentpath, "FoxSecDBEntities");
            //    if (connectionweb.Length > 30)
            //    {
            //        this.textBox8.Visible = true;
            //        textBox8.Text = connectionweb;
            //    }


            this.panel2.Enabled = false;
            this.button1.Enabled = false;
            Sqlin();

            //  comboBoxSQL.Items.Add("New installation. Sql Express 2014.");
            //   LaunchCommandLineApp();
        }


        static void LaunchCommandLineApp()
        {
            // For the example
            //  string extractPath = Path.Combine(Install.projectsPath + @"\Web\bin");
            //string ex1 = Path.Combine(Install.projectsPath + @"\Web\bin\");
            //const string ex2 = "C:\Dir";

            //// Use ProcessStartInfo class
            //ProcessStartInfo startInfo = new ProcessStartInfo();
            //startInfo.CreateNoWindow = false;
            //startInfo.UseShellExecute = false;
            //startInfo.FileName = "SQLManagementStudio_x64_ENU.exe";
            //startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            //startInfo.Arguments = ex1;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(Path.Combine(Install.projectsPath + @"\Web\bin\") + "SQLEXPRADV_x64_ENU.exe"))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch
            {
                // Log error.
            }
        }
        void Sqlin()
        {
            try
            {

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                startInfo.Arguments = "/c sqllocaldb create FoxSecMSSQLExpress";
                process.StartInfo = startInfo;
                process.Start();

                using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                {
                    RegistryKey instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", true);

                    if (instanceKey != null)
                    {
                        foreach (var instanceName in instanceKey.GetValueNames())
                        {

                            //if (instanceName == "MSSQLSERVER") { 
                            //else
                            //   comboBoxSQL.Items.Add(System.Environment.MachineName + @"\" + instanceName);
                        }


                    }
                    hklm.Close();
                    if (instanceKey != null)
                    {
                        instanceKey.Close();
                    }
                }
                try
                {
                    comboBox1.SelectedIndex = 0;

                    comboBoxSQL.Items.Add(@"(LocalDB)\FoxSecMSSQLExpress");
                    comboBoxSQL.SelectedIndex = 0;

                    DataTable dataTable = SmoApplication.EnumAvailableSqlServers(true);
                    foreach (DataRow db in dataTable.Rows)
                    {
                        comboBoxSQL.Items.Add(db["Name"].ToString());
                    }

                    //SqlDataSourceEnumerator SqlEnumerator;
                    //SqlEnumerator = SqlDataSourceEnumerator.Instance;
                    //DataTable dTable = SqlEnumerator.GetDataSources();
                    //foreach (DataRow db in dTable.Rows)
                    //{
                    //    comboBoxSQL.Items.Add(db["ServerName"].ToString() + "\\" + db["InstanceName"].ToString());
                    //}

                    comboBoxSQL.Items.Add("New installation. Sql Express 2014.");

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString() + "error sql enum");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error InstanceName");
            }

        }

        public void IIS()
        {
            //string name = "FoxSecWeb";
            //string physicalPath = Path.Combine(Install.projectsPath + @"\Web");
            //string pool = "FoxSecWebApplicationPool";
            //using (ServerManager mgr = new ServerManager())
            //{
            //    if (mgr.Sites[name] != null)
            //        mgr.Sites.Remove(mgr.Sites[name]);

            //    // Create site
            //    Site site = mgr.Sites.Add(name, "http", "*:80:", physicalPath);

            //    // Set app pool
            //    foreach (var app in site.Applications)
            //        app.ApplicationPoolName = pool;

            //    // Add extra bindings
            //    foreach (string hostname in extraBindings)
            //        site.Bindings.Add("*:80:" + hostname, "http");

            //    mgr.CommitChanges();
            //}

            try
            {
                string projectspath = Path.Combine(Install.projectsPath + @"\Web");
                string name = "FoxSecWeb";
                string directory = Path.Combine(Install.projectsPath + @"\Web");
                string pfxpath = directory + @"\" + CertName + ".pfx";
                string password = "foxsecsslcert@123";
                using (ServerManager serverManager = new ServerManager())
                {
                    try
                    {
                        Site s1 = serverManager.Sites["FoxSecWeb"];
                        serverManager.Sites.Remove(s1);
                        serverManager.CommitChanges();
                    }
                    catch
                    {

                    }
                    Site mySite = serverManager.Sites.Add(name, projectspath, 9191);
                    mySite.ServerAutoStart = true;

                    serverManager.CommitChanges();
                    Site site = serverManager.Sites[name];
                    site.Name = name;

                    site.Applications[0].VirtualDirectories[0].PhysicalPath = projectspath;

                    if (serverManager.ApplicationPools["FoxSecWebApplicationPool"] != null)
                        serverManager.ApplicationPools.Remove(serverManager.ApplicationPools["FoxSecWebApplicationPool"]);

                    serverManager.ApplicationPools.Add("FoxSecWebApplicationPool");
                    serverManager.Sites[name].Applications[0].ApplicationPoolName = "FoxSecWebApplicationPool";
                    ApplicationPool apppool = serverManager.ApplicationPools["FoxSecWebApplicationPool"];
                    apppool.ManagedRuntimeVersion = "v4.0";
                    apppool.ManagedPipelineMode = ManagedPipelineMode.Integrated;

                    if (HTTPSInstallation == "1")
                    {
                        try
                        {
                            // Create a collection object and populate it using the PFX file
                            X509Certificate2Collection collection = new X509Certificate2Collection();
                            collection.Import(pfxpath, password, X509KeyStorageFlags.MachineKeySet);

                            foreach (X509Certificate2 cert in collection)
                            {
                                if (cert.Subject.Equals("CN=TestServer, O=Test"))
                                {
                                    X509Store store1 = new X509Store(StoreName.Root, StoreLocation.LocalMachine);
                                    store1.Open(OpenFlags.ReadWrite);
                                    store1.Add(cert);
                                    store1.Close();
                                }

                                if (cert.Subject.Equals("CN=TestClient, OU=Applications, O=Test"))
                                {
                                    X509Store store1 = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                                    store1.Open(OpenFlags.ReadWrite);
                                    store1.Add(cert);
                                    store1.Close();
                                }
                            }
                            string hostName = Dns.GetHostName();
                            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
                            int port = CheckAvailableServerPort();
                            var site1 = serverManager.Sites[name];
                            site1.Bindings.Add("" + myIP + ":" + port + ":", collection[0].GetCertHash(), "MY");
                            serverManager.CommitChanges();

                            var site2 = serverManager.Sites[name];
                            for (int i = 0; i < site2.Bindings.Count; i++)
                            {
                                if (site2.Bindings[i].Protocol == "http")
                                {
                                    site2.Bindings.RemoveAt(i);
                                    break;
                                }
                            }
                            site2.ServerAutoStart = true;
                            serverManager.CommitChanges();
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    // FileWriter.UpdateEventsLogFile(msg);
                }

                string msg = "IIS installed";
                // FileWriter.UpdateEventsLogFile(msg);
                Finish.lgiis = msg;
                SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " FoxSec web hosted on IIS successfully...");
            }
            catch (Exception ex)
            {

            }
        }

        private int CheckAvailableServerPort()
        {
            // Evaluate current system tcp connections. This is the same information provided
            // by the netstat command line application, just in .Net strongly-typed object
            // form.  We will look through the list, and if our port we would like to use
            // in our TcpClient is occupied, we will set isAvailable to false.

            int port = 443;
            for (int i = 0; i < 20; i++)
            {
                bool isAvailable = true;
                IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
                IPEndPoint[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpListeners();

                foreach (IPEndPoint endpoint in tcpConnInfoArray)
                {
                    if (endpoint.Port == port)
                    {
                        isAvailable = false;
                        break;
                    }
                }

                if (isAvailable == false)
                {
                    port = port + 1;
                }
                else
                {
                    break;
                }
            }

            return (port);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            msg = "Config file successfully written";
            Finish.lgconf = msg;
            // replaceconfig();
            //createRoleLogin();
            if (chkhttps.Checked == true)
            {
                HTTPSInstallation = "1";
                createcertificate();
            }
            StartInstall();
            IIS();
            //  IIS();
            this.Hide();
            //  Check form5 = new Check();
            //  form5.Show();
            //InstallConf6.istallConf(); //commented on 8 apr 2019
        }

        public void createcertificate()
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            if (hasAdministrativeRight)
            {
                Random generator = new Random();
                var shell = PowerShell.Create();
                shell.AddCommand("Get-Process");
                shell.Invoke();
                string savepath = Path.Combine(Install.projectsPath + @"\Web");
                string certname = "MyFoxSecCert" + generator.Next(1, 10000).ToString("D4");
                string cerpath = savepath + @"\" + certname + ".cer";
                string pfxpath = savepath + @"\" + certname + ".pfx";
                CertName = certname;
                string strscript = (@"$certificate = New-SelfSignedCertificate `
                -Subject localhost `
                -DnsName localhost `
                -KeyAlgorithm RSA `
                -KeyLength 2048 `
                -NotBefore (Get-Date) `
                -NotAfter (Get-Date).AddYears(20) `
                -CertStoreLocation ‘cert:CurrentUser\My’ `
                -FriendlyName ‘FoxSecSSLCertName’ `
                -HashAlgorithm SHA256 `
                -KeyUsage DigitalSignature, KeyEncipherment, DataEncipherment `
                -TextExtension @(‘2.5.29.37={text}1.3.6.1.5.5.7.3.1’) 
                $certificatePath = 'Cert:\CurrentUser\My\' + ($certificate.ThumbPrint)  

                # create temporary certificate path
                $tmpPath = ‘certsavepath’
                If(!(test-path $tmpPath))
                {
                New-Item -ItemType Directory -Force -Path $tmpPath
                }
                
                # set certificate password here
                $pfxPassword = ConvertTo-SecureString -String ‘foxsecsslcert@123’ -Force -AsPlainText
                $pfxFilePath = ‘pfxfilepath’
                $cerFilePath = ‘certfilepath’
                
                # create pfx certificate
                Export-PfxCertificate -Cert $certificatePath -FilePath $pfxFilePath -Password $pfxPassword
                Export-Certificate -Cert $certificatePath -FilePath $cerFilePath
                
                # import the pfx certificate
                Import-PfxCertificate -FilePath $pfxFilePath Cert:\LocalMachine\My -Password $pfxPassword -Exportable
                
                # trust the certificate by importing the pfx certificate into your trusted root
                Import-Certificate -FilePath $cerFilePath -CertStoreLocation Cert:\CurrentUser\Root
                
                # optionally delete the physical certificates (don’t delete the pfx file as you need to copy this to your app directory)
                # Remove-Item $pfxFilePath
                Remove-Item $cerFilePath").Replace("certsavepath", savepath).Replace("pfxfilepath", pfxpath).Replace("certfilepath", cerpath).Replace("FoxSecSSLCertName", certname);
                // Add the script to the PowerShell object
                shell.Commands.AddScript(strscript);

                // Execute the script
                var results = shell.Invoke();

                // display results, with BaseObject converted to string
                // Note : use |out-string for console-like output
                if (results.Count > 0)
                {
                    // We use a string builder ton create our result text
                    var builder = new StringBuilder();

                    foreach (var psObject in results)
                    {
                        // Convert the Base Object to a string and append it to the string builder.
                        // Add \r\n for line breaks
                        builder.Append(psObject.BaseObject.ToString() + "\r\n");
                    }

                    // Encode the string in HTML (prevent security issue with 'dangerous' caracters like < >
                    string directory = Path.Combine(Install.projectsPath + @"\Web");
                    string pass = "foxsecsslcert@123";

                    String xMachineName = Environment.MachineName;
                    String xSSLCertificate = pfxpath;
                    String xSSLCertificatePassword = pass;
                    using (Microsoft.Web.Administration.ServerManager iisManager = new Microsoft.Web.Administration.ServerManager())
                    {
                        X509Store store = new X509Store("WebHosting", StoreLocation.LocalMachine);
                        store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
                        X509Certificate2 certificate = new X509Certificate2(xSSLCertificate, xSSLCertificatePassword);
                        store.Add(certificate);
                        store.Close();
                        iisManager.CommitChanges();
                    }
                }
                SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " HTTPS certificate created successfully....");
            }
            else
            {
                if (MessageBox.Show("This application requires admin privilages.\nClick Ok to elevate or Cancel to exit.", "Elevate?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    ProcessStartInfo processInfo = new ProcessStartInfo();
                    processInfo.Verb = "runas";
                    processInfo.FileName = System.Windows.Forms.Application.ExecutablePath;
                    try
                    {
                        Process.Start(processInfo);
                    }
                    catch (Win32Exception ex)
                    {
                        System.Windows.Forms.Application.Exit();
                    }
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    System.Windows.Forms.Application.Exit();
                }
            }
        }


        public void comboBoxSQL_SelectedIndexChanged(object sender, EventArgs e)
        {

            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;

            if (comboBoxSQL.Text == "New installation. Sql Express 2014.")
            {
                try
                {
                    //using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                    //{
                    //    RegistryKey instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", false);

                    //    if (instanceKey != null)
                    //    {
                    //        foreach (var instanceName in instanceKey.GetValueNames())
                    //        {
                    //            if (instanceName == "SQLEXPRESS") { MessageBox.Show("SQLEXPRESS already istalled"); }
                    //        }

                    //    }
                    //    if (instanceKey == null)
                    //    {
                    //        Version vers = Check.GetNETVersion();
                    //        try
                    //        {
                    //            string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    //            string zipPath = Path.Combine(appPath + "SQLEXPRADV_x64_ENU.zip");
                    //            string extractPath = Path.Combine(Install.projectsPath + @"\Web\bin");

                    //            ZipFile.ExtractToDirectory(zipPath, extractPath);
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            // MessageBox.Show(ex.ToString() + "Error extract");

                    //            FileWriter.UpdateEventsLogFile(saveNow + "Error extract SQLEXPRADV_x64_ENU.exe" + ex.Message);
                    //        }

                    //        try
                    //        {
                    //            // Start the process with the info we specified.
                    //            // Call WaitForExit and then the using statement will close.
                    //            string extractPath1 = Path.Combine(Install.projectsPath + @"\Web\bin\");
                    //            // string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    //            // MessageBox.Show(extractPath1.ToString());
                    //            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(Path.Combine(extractPath1 + "SQLEXPRADV_x64_ENU.exe"));
                    //            psi.WorkingDirectory = extractPath1;
                    //            psi.RedirectStandardOutput = true;
                    //            psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    //            psi.UseShellExecute = false;
                    //            System.Diagnostics.Process listFiles;
                    //            listFiles = System.Diagnostics.Process.Start(psi);
                    //            System.IO.StreamReader myOutput = listFiles.StandardOutput;
                    //            listFiles.WaitForExit();
                    //            if (listFiles.HasExited)
                    //            {
                    //                string output = myOutput.ReadToEnd();
                    //            }
                    //            //using (Process exeProcess = Process.Start(Path.Combine(Install.projectsPath + @"\Web\bin\") + "SQLManagementStudio_x64_ENU.exe"))
                    //            //{
                    //            //    exeProcess.WaitForExit();
                    //            //}
                    //        }
                    //        catch (Exception ex)
                    //        {
                    //            MessageBox.Show(ex.ToString());
                    //            // Log error.
                    //        }

                    //    }
                    //    hklm.Close();
                    //    if (instanceKey != null)
                    //    {
                    //        instanceKey.Close();
                    //    }
                    //}

                    string servicename = "MSSQL";
                    string servicename2 = "SQLAgent";
                    string servicename3 = "SQL Server";
                    string servicename4 = "msftesql";

                    string serviceoutput = string.Empty;

                    ServiceController[] services = ServiceController.GetServices();

                    foreach (ServiceController service in services)
                    {

                        if (service == null)

                            continue;

                        if (service.ServiceName.Contains(servicename) || service.ServiceName.Contains(servicename2) || service.ServiceName.Contains(servicename3) || service.ServiceName.Contains(servicename4))
                        {

                            serviceoutput = serviceoutput + System.Environment.NewLine + "Service Name = " + service.ServiceName + System.Environment.NewLine + "Display Name = " + service.DisplayName + System.Environment.NewLine + "Status = " + service.Status + System.Environment.NewLine;

                        }

                    }

                    if (serviceoutput == "")
                    {
                        try
                        {
                            string appPath = AppDomain.CurrentDomain.BaseDirectory;
                            string zipPath = Path.Combine(appPath + "SQLEXPRADV_x64_ENU.zip");
                            string extractPath = Path.Combine(Install.projectsPath + @"\Web\bin");

                            ZipFile.ExtractToDirectory(zipPath, extractPath);
                        }
                        catch (Exception ex)
                        {
                            // MessageBox.Show(ex.ToString() + "Error extract");

                            FileWriter.UpdateEventsLogFile(saveNow + " Error extract SQLEXPRADV_x64_ENU.exe " + ex.Message);
                        }

                        try
                        {
                            // Start the process with the info we specified.
                            // Call WaitForExit and then the using statement will close.
                            string extractPath1 = Path.Combine(Install.projectsPath + @"\Web\bin\");
                            // string appPath = AppDomain.CurrentDomain.BaseDirectory;
                            // MessageBox.Show(extractPath1.ToString());
                            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(Path.Combine(extractPath1 + "SQLEXPRADV_x64_ENU.exe"));
                            psi.WorkingDirectory = extractPath1;
                            psi.RedirectStandardOutput = true;
                            psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            psi.UseShellExecute = false;
                            System.Diagnostics.Process listFiles;
                            listFiles = System.Diagnostics.Process.Start(psi);
                            System.IO.StreamReader myOutput = listFiles.StandardOutput;
                            listFiles.WaitForExit();
                            if (listFiles.HasExited)
                            {
                                string output = myOutput.ReadToEnd();
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            // Log error.
                        }

                    }
                    else
                    {
                        MessageBox.Show("SQLEXPRESS already installed in your local machine!!");
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString() + "Error not registry sql express");
                }
            }
        }

        public static bool isExpressInstalled()
        {
            const string edition = "Express Edition";

            const string instance = "MSSQL$SQLEXPRESS";

            const int spLevel = 1;


            bool fCheckEdition = false;

            bool fCheckSpLevel = false;


            try

            {

                // Run a WQL query to return information about SKUNAME and SPLEVEL about installed instances

                // of the SQL Engine.

                ManagementObjectSearcher getSqlExpress =

                    new ManagementObjectSearcher("root\\Microsoft\\SqlServer\\ComputerManagement",

                    "select * from SqlServiceAdvancedProperty where SQLServiceType = 1 and ServiceName = '"

                    + instance + "' and (PropertyName = 'SKUNAME' or PropertyName = 'SPLEVEL')");


                // If nothing is returned, SQL Express isn't installed.

                if (getSqlExpress.Get().Count == 0)

                {

                    return false;

                }


                // If something is returned, verify it is the correct edition and SP level.

                foreach (ManagementObject sqlEngine in getSqlExpress.Get())

                {

                    if (sqlEngine["ServiceName"].ToString().Equals(instance))

                    {

                        switch (sqlEngine["PropertyName"].ToString())

                        {

                            case "SKUNAME":

                                // Check if this is Express Edition or Express Edition with Advanced Services

                                fCheckEdition = sqlEngine["PropertyStrValue"].ToString().Contains(edition);

                                break;


                            case "SPLEVEL":

                                // Check if the instance matches the specified level

                                fCheckSpLevel = int.Parse(sqlEngine["PropertyNumValue"].ToString()) >= spLevel;

                                //fCheckSpLevel = sqlEngine["PropertyNumValue"].ToString().Contains(spLevel);

                                break;

                        }

                    }

                }


                if (fCheckEdition & fCheckSpLevel)

                {


                    return true;

                }

                return false;

            }

            catch (ManagementException e)

            {

                Console.WriteLine("Error: " + e.ErrorCode + ", " + e.Message);

                return false;

            }

        }
        SqlConnection conn = new SqlConnection();
        private void button4_Click(object sender, EventArgs e)
        {
            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;
            selectedItem = comboBoxSQL.Text;
            string name = textBox2.Text;
            string pass = textBox3.Text;

            if (comboBox1.SelectedIndex == 0)
            {
                //conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=SSPI;Initial Catalog=;", selectedItem, name, pass);
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Initial Catalog=;", selectedItem, name, pass);
            }
            else
            {
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=false;Connect Timeout=30;Initial Catalog=;", selectedItem, name, pass);
            }
            try
            {
                conn.Open();
                //   MessageBox.Show("Test connection succeeded.");
                msg = "Test connection succeeded.";
                SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " Connection Test successfully....");
                Finish.lgsql = msg;
                if (Change.Replace == "Yes")
                {
                    this.panel2.Enabled = true;
                    this.button1.Enabled = true;
                    string msg1 = "Login already exists";
                    //  MessageBox.Show(msg);
                    Finish.lglogin = msg1;
                }
                else
                {
                    this.panel2.Enabled = true;
                    this.button1.Enabled = false;
                }
                conn.Close();
                //RestoreDatabase();
                DialogResult dialogResult = MessageBox.Show("Database will be copied to the server. It will take some time. If you want to continue, click 'Yes'.", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    restoreDB();
                }
                else if (dialogResult == DialogResult.No)
                {

                    Finish.lgdb1 = msg;
                }
                //  restoreDB();
                // SqlRestore();
            }
            catch (SqlException ex)
            {
                msg = "Test connection not succeeded.";
                MessageBox.Show("Test connection not succeeded.");
                //MessageBox.Show(ex.Message);

                Finish.lgsql1 = msg;
                if (ex.Number == 15025)
                    FileWriter.UpdateEventsLogFile(saveNow + " Test connection not succeeded.");
                // Console.WriteLine("Login already exists.");
                else
                    FileWriter.UpdateEventsLogFile(saveNow + " Test connection not succeeded.");
                //  Console.WriteLine("{0}: {1}", ex.Number, ex.Message);
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }
        }

        void CheckSql()
        {
            try

            {
                using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                {
                    RegistryKey instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", false);

                    if (instanceKey != null)
                    {
                        foreach (var instanceName in instanceKey.GetValueNames())
                        {
                            if (instanceName == "SQLEXPRESS") { MessageBox.Show("SQLEXPRESS already istalled"); }
                        }

                    }
                    hklm.Close();
                    if (instanceKey != null)
                    {
                        instanceKey.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString() + "Error not registry sql express");
            }

        }
        void restoreDB()
        {
            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;
            selectedItem = comboBoxSQL.Text;

            textBox2.Text = textBox2.Text;
            textBox3.Text = textBox3.Text;
            string name = textBox2.Text;
            string pass = textBox3.Text;

            if (comboBox1.SelectedIndex == 0)
            {
                //conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=SSPI;Initial Catalog=;", selectedItem, name, pass);
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Initial Catalog=;", selectedItem, name, pass);
                //MessageBox.Show(conn.ConnectionString.ToString());
            }
            else
            {
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=false;Connect Timeout=30;Initial Catalog=;", selectedItem, name, pass);
                //MessageBox.Show(conn.ConnectionString.ToString());
            }


            string scriptDirectory = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin\");
            conn.Open();

            string cmdText = "Select * from sys.databases Where name = 'FoxSecDB'";

            SqlCommand sqlCmd = new SqlCommand(cmdText, conn);

            Boolean bRet;
            try
            {
                // conn.Open();
                System.Data.SqlClient.SqlDataReader reader = sqlCmd.ExecuteReader();
                bRet = reader.HasRows;
                conn.Close();
            }
            catch
            {

                MessageBox.Show("Cannot check existing FoxSecDB database.");
                bRet = false;
                conn.Close();
                //   conn.Close();

                //return false;
            } //End Try Catch Block


            if (bRet == true)
            {
                MessageBox.Show("Database FoxSecDB already exists");
                if (Change.Replace == "Yes")
                {
                    this.panel2.Enabled = true;
                    this.button1.Enabled = true;
                    string msg1 = "Login already exists";
                    //  MessageBox.Show(msg);
                    Finish.lglogin = msg1;
                    replaceconfig();
                }
                else
                {
                    this.panel2.Enabled = true;
                    this.button1.Enabled = false;
                }
                conn.Close();
                msg = "Database FoxSecDB already exists";
                Finish.lgdb1 = msg;
                Insertlicence();
            }
            else
            {

                try
                {
                    conn.Open();
                    //    DirectoryInfo di = new DirectoryInfo(scriptDirectory);
                    //   FileInfo[] rgFiles = di.GetFiles("FoxSecDb.sql");
                    //     foreach (FileInfo fi in rgFiles)
                    //   {
                    //   FileInfo fileInfo = new FileInfo(fi.FullName);
                    // string script = fileInfo.OpenText().ReadToEnd();
                    string strCmd = string.Format(@" 
USE [master]

IF NOT EXISTS (SELECT [name] FROM sys.databases WHERE name = N'FoxSecDB')
BEGIN
CREATE DATABASE [FoxSecDB] COLLATE SQL_Latin1_General_CP1_CI_AS
END

EXEC dbo.sp_dbcmptlevel @dbname=N'FoxSecDB', @new_cmptlevel=100

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
BEGIN
	EXEC [FoxSecDB].[dbo].[sp_fulltext_database] @action = 'enable'
END
");

                    string strCmd1 = string.Format(@"
          ALTER DATABASE [FoxSecDB] SET ANSI_NULL_DEFAULT OFF

ALTER DATABASE [FoxSecDB] SET ANSI_NULLS OFF

ALTER DATABASE [FoxSecDB] SET ANSI_PADDING OFF

ALTER DATABASE [FoxSecDB] SET ANSI_WARNINGS OFF

ALTER DATABASE [FoxSecDB] SET ARITHABORT OFF

ALTER DATABASE [FoxSecDB] SET AUTO_CLOSE OFF

ALTER DATABASE [FoxSecDB] SET AUTO_CREATE_STATISTICS ON

ALTER DATABASE [FoxSecDB] SET AUTO_SHRINK OFF

ALTER DATABASE [FoxSecDB] SET AUTO_UPDATE_STATISTICS ON

ALTER DATABASE [FoxSecDB] SET CURSOR_CLOSE_ON_COMMIT OFF

ALTER DATABASE [FoxSecDB] SET CURSOR_DEFAULT GLOBAL

ALTER DATABASE [FoxSecDB] SET CONCAT_NULL_YIELDS_NULL OFF

ALTER DATABASE [FoxSecDB] SET NUMERIC_ROUNDABORT OFF

ALTER DATABASE [FoxSecDB] SET QUOTED_IDENTIFIER OFF

ALTER DATABASE [FoxSecDB] SET RECURSIVE_TRIGGERS OFF

ALTER DATABASE [FoxSecDB] SET DISABLE_BROKER

ALTER DATABASE [FoxSecDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF

ALTER DATABASE [FoxSecDB] SET DATE_CORRELATION_OPTIMIZATION OFF

ALTER DATABASE [FoxSecDB] SET TRUSTWORTHY OFF

ALTER DATABASE [FoxSecDB] SET TRUSTWORTHY OFF

ALTER DATABASE [FoxSecDB] SET ALLOW_SNAPSHOT_ISOLATION OFF

ALTER DATABASE [FoxSecDB] SET PARAMETERIZATION SIMPLE

ALTER DATABASE [FoxSecDB] SET READ_WRITE

ALTER DATABASE [FoxSecDB] SET RECOVERY FULL

ALTER DATABASE [FoxSecDB] SET MULTI_USER

ALTER DATABASE [FoxSecDB] SET PAGE_VERIFY CHECKSUM

ALTER DATABASE [FoxSecDB] SET DB_CHAINING OFF


USE [FoxSecDB]


--Create Roles
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'public' AND type = 'R')
	CREATE ROLE [public] AUTHORIZATION [dbo]


--Database Schemas

USE [FoxSecDB]

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'dbo')
EXEC sys.sp_executesql N'CREATE SCHEMA [dbo] AUTHORIZATION [dbo]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_datareader')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_datareader] AUTHORIZATION [dbo]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_datawriter')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_datawriter] AUTHORIZATION [dbo]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_denydatareader')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_denydatareader] AUTHORIZATION [dbo]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_denydatawriter')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_denydatawriter] AUTHORIZATION [dbo]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'guest')
EXEC sys.sp_executesql N'CREATE SCHEMA [guest] AUTHORIZATION [guest]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'INFORMATION_SCHEMA')
EXEC sys.sp_executesql N'CREATE SCHEMA [INFORMATION_SCHEMA] AUTHORIZATION [INFORMATION_SCHEMA]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'sys')
EXEC sys.sp_executesql N'CREATE SCHEMA [sys] AUTHORIZATION [sys]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_owner')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_owner] AUTHORIZATION [db_owner]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_accessadmin')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_accessadmin] AUTHORIZATION [db_accessadmin]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_securityadmin')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_securityadmin] AUTHORIZATION [db_securityadmin]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_ddladmin')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_ddladmin] AUTHORIZATION [db_ddladmin]'

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'db_backupoperator')
EXEC sys.sp_executesql N'CREATE SCHEMA [db_backupoperator] AUTHORIZATION [db_backupoperator]'

USE [FoxSecDB]
CREATE TABLE [dbo].[__RefactorLog](
	[OperationKey] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK____Refact__D3AEFFDB66603565] PRIMARY KEY CLUSTERED 
(
	[OperationKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[AuditLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EventTypeId] [int] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[UserName] [nvarchar](500) NOT NULL,
	[OldValue] [nvarchar](max) NOT NULL,
	[NewValue] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_AuditLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[BuildingObjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeId] [int] NOT NULL,
	[BuildingId] [int] NOT NULL,
	[ObjectNr] [int] NULL,
	[ParentObjectId] [int] NULL,
	[FloorNr] [int] NULL,
	[Timestamp] [timestamp] NOT NULL,
	[Description] [nvarchar](250) NULL,
	[StatusIconId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[FSControllerNodeId] [int] NULL,
	[FSControllerObjectNr] [int] NULL,
	[FSTypeId] [int] NULL,
	[BOOrder] [int] NULL,
	[GlobalBuilding] [int] NULL,
	[ParentArea] [int] NULL,
 CONSTRAINT [PK_BuildingObjects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE TABLE [dbo].[BuildingObjectTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Description] [nvarchar](250) NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ModifiedBy] [nvarchar](50) NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_BuildingTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Buildings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AdressStreet] [nvarchar](75) NULL,
	[AdressHouse] [nvarchar](15) NULL,
	[AdressIndex] [nvarchar](50) NULL,
	[LocationId] [int] NOT NULL,
	[Name] [nvarchar](75) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Floors] [int] NOT NULL,
	[TimediffGMTMinutes] [int] NULL,
	[TimezoneId] [varchar](250) NULL,
 CONSTRAINT [PK_Buildings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[BuldingObjectsStatusOrAlarms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsStatus] [bit] NOT NULL,
	[BuildingObjectFSTypeId] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Value] [bit] NOT NULL,
	[BitValue] [int] NOT NULL,
	[Priority] [int] NOT NULL,
	[AlarmFoundTime] [datetime] NULL,
 CONSTRAINT [PK_BuldingObjectsStatusOrAlarms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[CameraPermissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CameraID] [int] NULL,
	[CameraName] [nvarchar](50) NULL,
	[CompanyID] [int] NULL,
	[Access] [bit] NULL,
 CONSTRAINT [PK_CameraPermissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Classificators](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](500) NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_Classificators] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[ClassificatorValues](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassificatorId] [int] NOT NULL,
	[Value] [nvarchar](500) NULL,
	[Comments] [nvarchar](500) NULL,
	[SortOrder] [int] NULL,
	[DisplayValue] [nvarchar](200) NULL,
	[Legal] [int] NULL,
	[LegalHash] [nvarchar](250) NULL,
	[Remaining] [int] NULL,
	[RemainingHash] [nvarchar](250) NULL,
	[ValidTo] [datetime] NULL,
	[ValidToHash] [nvarchar](250) NULL,
 CONSTRAINT [PK_ClassificatorValues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Companies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](125) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ModifiedBy] [nvarchar](100) NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[Comment] [nvarchar](250) NULL,
	[Active] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[ParentId] [int] NULL,
	[IsCanUseOwnCards] [bit] NOT NULL,
	[ClassificatorValueId] [int] NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[CompanyBuildingObjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[BuildingObjectId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_CompanyBuildings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[CompanyManagers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CompanyId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_CompanyManagers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[ControllerUpdate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ControllerId] [int] NULL,
	[EntityId] [int] NOT NULL,
	[ParameterId] [int] NOT NULL,
	[Value] [varchar](255) NOT NULL,
	[MemoryStartAddress] [nchar](10) NULL,
	[Status] [int] NOT NULL,
	[DateAdded] [datetime] NOT NULL,
	[DateLastChanged] [datetime] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_ControllerUpdate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Countries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](75) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ISONumber] [smallint] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Departments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](100) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Number] [nvarchar](max) NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FSBuildingObjectCameras](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BuildingObjectId] [int] NOT NULL,
	[CameraId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_FSBuildingObjectCameras] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSBuildingObjectMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BuildingObjectId] [int] NOT NULL,
	[MessageId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_FSBuildingObjectMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSCameras](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ServerNr] [int] NULL,
	[CameraNr] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Port] [int] NULL,
	[ResX] [int] NULL,
	[ResY] [int] NULL,
	[Skip] [int] NULL,
	[Delay] [int] NULL,
	[EnableLiveControls] [bit] NULL,
	[QuickPreviewSeconds] [int] NULL,
	[UsedOnThePlan] [bit] NULL,
	[Deleted] [bit] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[URL] [nvarchar](255) NULL,
	[Playback] [nvarchar](255) NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK_FSCameras] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FsControllerNodeTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[active] [bit] NULL,
	[timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FsControllerNodeTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSControllersNodes](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DataLineId] [int] NOT NULL,
	[ControllerAddress] [int] NOT NULL,
	[PositionInDataLine1_31] [int] NULL,
	[ControllerTypeId] [int] NULL,
	[Name] [varchar](50) NULL,
	[ReaderType] [int] NULL,
	[StatusIconId] [int] NULL,
	[SaveStatus] [int] NULL,
	[LastModified] [date] NULL,
	[MaxBuildingObjects] [int] NULL,
	[MaxUsers] [int] NULL,
	[Online] [bit] NULL,
	[SoftVers] [nvarchar](10) NULL,
	[IsDeleted] [bit] NULL,
	[Ttimestamp] [timestamp] NOT NULL,
	[FSCntOrder] [int] NULL,
 CONSTRAINT [PK_FSControllersNodes] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSControllersTZsMemoryPosSave](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ControllerId] [int] NOT NULL,
	[TimeZoneId] [int] NOT NULL,
	[MemoryPos] [int] NOT NULL,
	[SaveStatus] [int] NULL,
	[Active] [bit] NULL,
	[LastChanged] [datetime] NULL,
	[Isdeleted] [bit] NOT NULL,
	[priority] [int] NOT NULL,
	[DoubleIndex] [nvarchar](17) NOT NULL,
	[ControllerUpdateId] [int] NULL,
	[IsOriginal] [bit] NOT NULL,
	[UserTimeZoneId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FSControllersTZsMemoryPosSave] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FsControllersUsersSaveStatus](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ControllerId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Mem1] [bit] NULL,
	[Mem2] [bit] NULL,
	[Mem3] [bit] NULL,
	[mem4] [bit] NULL,
	[mem5] [bit] NULL,
	[mem6] [bit] NULL,
	[mem7] [bit] NULL,
	[memPos] [int] NULL,
	[Status] [int] NULL,
	[LastChanged] [datetime] NULL,
	[Priority] [int] NOT NULL,
	[DoubleIndex] [nvarchar](17) NOT NULL,
	[FsProjectsUsersMemoryPosID] [int] NULL,
	[ControllerUpdateId] [int] NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FsControllersUsersSaveStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSDatalines](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PanelId] [int] NOT NULL,
	[PanelTypeId] [int] NOT NULL,
	[ServerID] [int] NOT NULL,
	[ProjectID] [int] NULL,
	[Name] [varchar](50) NULL,
	[IPaddress] [varchar](15) NULL,
	[ArmingIPPort] [int] NULL,
	[DoorIPPort] [int] NULL,
	[EncryptionKeys] [varchar](128) NULL,
	[Online] [bit] NOT NULL,
	[Active] [bit] NULL,
	[Opened] [date] NULL,
	[LastSave] [date] NULL,
	[ServisUserId] [int] NULL,
	[RWUserId] [int] NULL,
	[ServisPW] [nvarchar](10) NULL,
	[LocalServerTimeDifHours] [int] NULL,
	[Timestamp] [timestamp] NOT NULL,
	[TimezoneId] [nvarchar](250) NULL,
 CONSTRAINT [PK_FSDatalines] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSHR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FoxSecFieldName] [nvarchar](50) NOT NULL,
	[HRFieldname] [nvarchar](50) NOT NULL,
	[FoxSecTableId] [int] NOT NULL,
	[IsIndex] [bit] NOT NULL,
	[AutoUpdate] [bit] NOT NULL,
	[FieldType] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IndexFilename] [nvarchar](50) NULL,
	[FoxsecTableName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FSHR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[FSINISettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](225) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[SoftType] [int] NOT NULL,
	[SoftId] [int] NULL,
	[Value] [nvarchar](max) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_FSINISettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FSMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectID] [int] NULL,
	[MessageNr] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[Message] [nvarchar](500) NULL,
	[Timestamp] [timestamp] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_FSMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FsPanelTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PanelTypeNr] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[active] [bit] NOT NULL,
	[MaxSecurityUsers] [int] NULL,
	[MaxDoorUsers] [int] NULL,
	[MaxRoom] [int] NULL,
	[MaxDoor] [int] NULL,
	[MaxElevator] [int] NULL,
	[Maxkeypad] [int] NOT NULL,
	[MaxNode] [int] NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FsPanelTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSProjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[active] [bit] NOT NULL,
	[LastUsersFullUpdate] [date] NULL,
	[UnigueSring] [nvarchar](50) NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FSProjects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FsProjectsUsersMemoryPos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[DoorFloorMemoryPos] [int] NOT NULL,
	[RoomMemoryPos] [int] NOT NULL,
	[IsRoomPermission] [bit] NULL,
	[KeyPairPrjUsr] [nvarchar](20) NOT NULL,
	[UsersAccessUnitId] [int] NULL,
	[IsDeleted] [bit] NULL,
	[DeletedDate] [date] NULL,
	[IsDoorLiftPermission] [bit] NULL,
	[DateDelRoomPermission] [date] NULL,
	[DateDelDoorLiftPermission] [date] NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FsProjectsUsersMemoryPos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSServers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ProjectID] [int] NULL,
	[active] [bit] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FSServers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSSublocationObjects](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SublocationId] [int] NOT NULL,
	[BuildingObjectId] [int] NOT NULL,
	[LogTypeId] [int] NOT NULL,
	[IsDeleted] [bit] NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_FSSublocationObjects] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[FSSublocations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Sublocation] [nvarchar](100) NULL,
	[IsDeleted] [bit] NULL,
	[Timestamp] [timestamp] NOT NULL,
	[SglType] [int] NULL,
	[SqlValue] [nvarchar](max) NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK_FSSublocations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[FSVideoServers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProjectId] [int] NULL,
	[Name] [nvarchar](50) NULL,
	[IP] [nvarchar](128) NULL,
	[UID] [nvarchar](50) NULL,
	[PWD] [nvarchar](50) NULL,
	[Deleted] [bit] NULL,
	[Timestamp] [timestamp] NOT NULL,
	[Type] [smallint] NULL,
 CONSTRAINT [PK_FSVideoServers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[HolidayBuildings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HoliDayId] [int] NOT NULL,
	[BuildingId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_HolidayBuildings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Holidays](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[EventStart] [datetime] NOT NULL,
	[EventEnd] [datetime] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ModifiedBy] [nvarchar](100) NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[MovingHoliday] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Holidays] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Locations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](75) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[CountryId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Log](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Action] [nvarchar](max) NOT NULL,
	[UserId] [int] NULL,
	[EventTime] [datetime] NOT NULL,
	[CompanyId] [int] NULL,
	[BuildingObjectId] [int] NULL,
	[LogTypeId] [int] NOT NULL,
	[Building] [nvarchar](50) NULL,
	[Node] [nvarchar](50) NULL,
	[EventKey] [nvarchar](20) NULL,
	[TAReportLabelId] [int] NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[LogFilters](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[CompanyId] [int] NULL,
	[Building] [nvarchar](50) NULL,
	[Node] [nvarchar](50) NULL,
	[FromDate] [datetime] NULL,
	[ToDate] [datetime] NULL,
	[Name] [nvarchar](50) NULL,
	[UserId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Activity] [nvarchar](50) NULL,
	[IsShowDefaultLog] [bit] NULL,
 CONSTRAINT [PK_LogFilter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[LogTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NumberOfError] [int] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Color] [nvarchar](6) NULL,
	[Timestamp] [timestamp] NOT NULL,
	[IsDefault] [bit] NOT NULL,
 CONSTRAINT [PK_LogTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[RoleBuildings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[BuildingId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_RoleBuildings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Permissions] [binary](400) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](100) NOT NULL,
	[Description] [nchar](250) NOT NULL,
	[Active] [bit] NOT NULL,
	[Menues] [binary](400) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[StaticId] [int] NOT NULL,
	[Priority] [int] NOT NULL,
	[RoleTypeId] [int] NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[RoleTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](3) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Menues] [binary](50) NOT NULL,
 CONSTRAINT [PK_RoleTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[TABuildingNames](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BuildingId] [int] NOT NULL,
	[Name] [nvarchar](75) NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[Address] [nvarchar](75) NOT NULL,
	[BuildingLicense] [nvarchar](75) NOT NULL,
	[CadastralNr] [nvarchar](75) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Customer] [nvarchar](75) NULL,
	[Contractor] [nvarchar](75) NULL,
	[Contract] [nvarchar](75) NULL,
	[Sum] [nvarchar](75) NULL,
 CONSTRAINT [PK_TABuildingNames] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[TAMoves](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[DepartmentId] [int] NULL,
	[Label] [nvarchar](8) NOT NULL,
	[Remark] [nvarchar](20) NOT NULL,
	[Started] [datetime] NOT NULL,
	[Finished] [datetime] NULL,
	[Hours] [real] NOT NULL,
	[Hours_Min] [nvarchar](8) NOT NULL,
	[Schedule] [int] NOT NULL,
	[Status] [tinyint] NULL,
	[JobNotMove] [bit] NOT NULL,
	[Completed] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[ModifiedBy] [nvarchar](20) NOT NULL,
	[StartedBoId] [int] NULL,
	[FinishedBoId] [int] NULL,
 CONSTRAINT [PK_Moves] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[TAReportLabels](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Label] [nvarchar](8) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[PIN] [smallint] NULL,
	[RegistratorKey] [smallint] NULL,
	[RegistratorMenuNr] [smallint] NULL,
	[CompanyId] [int] NULL,
	[JobNotMove] [bit] NULL,
	[Fixed] [bit] NOT NULL,
	[ValidFrom] [date] NULL,
	[ValidTo] [date] NULL,
	[EnteredEvent] [bit] NOT NULL,
	[At_work] [bit] NOT NULL,
	[InBuilding] [bit] NOT NULL,
	[Allow_Jobs] [bit] NOT NULL,
	[DaysNotHours] [bit] NOT NULL,
	[AskStartStopCount] [int] NOT NULL,
	[DefaultStart] [nvarchar](20) NULL,
	[DefaultStop] [nvarchar](20) NULL,
	[FinishPrevious] [bit] NOT NULL,
	[Admin_only] [bit] NOT NULL,
	[RegistratorId] [int] NULL,
	[ShiftId] [int] NULL,
	[NotFixNextTASeconds] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[Report] [bit] NOT NULL,
	[SaveStatus] [smallint] NULL,
	[Active] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[MenuImageSelectedPng] [image] NULL,
	[MenuImageNotSelectedPng] [image] NULL,
 CONSTRAINT [PK_ReportLabels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[TAReports](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[DepartmentId] [int] NULL,
	[Name] [nvarchar](8) NOT NULL,
	[ReportDate] [datetime] NOT NULL,
	[Day] [smallint] NOT NULL,
	[Hours] [real] NOT NULL,
	[Hours_Min] [nvarchar](8) NOT NULL,
	[Shift] [int] NOT NULL,
	[Status] [tinyint] NULL,
	[Completed] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[ModifiedId] [int] NOT NULL,
	[BuildingId] [int] NULL,
 CONSTRAINT [PK_TAReports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[TAShifts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](75) NOT NULL,
	[StartFrom] [smalldatetime] NULL,
	[FinishAt] [smalldatetime] NULL,
	[WorkBeforeBreak] [int] NULL,
	[Breaks] [int] NULL,
	[WorkBeforeLunch] [int] NULL,
	[Lunch] [int] NULL,
	[OvertimeMin] [int] NULL,
	[CompanyId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[Changed] [bit] NOT NULL,
 CONSTRAINT [PK_TAShifts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[TAUsersShifts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[UeserId] [int] NOT NULL,
	[TaShiftId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_TAUsersShifts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Terminal](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShowScreensaver] [bit] NOT NULL,
	[ScreenSaverShowAfter] [time](7) NOT NULL,
	[MaxUserId] [int] NOT NULL,
	[CompanyId] [varchar](50) NULL,
	[TerminalId] [nvarchar](max) NULL,
	[ApprovedDevice] [bit] NULL,
	[Name] [nvarchar](150) NULL,
	[IsDeleted] [bit] NULL,
	[InfoKioskMode] [bit] NULL,
	[SoundAlarms] [nvarchar](250) NULL,
	[ShowMaxAlarmsFistPage] [int] NULL,
	[LastLogin] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[Titles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](150) NOT NULL,
	[Description] [nchar](200) NULL,
	[Timestamp] [timestamp] NOT NULL,
	[CompanyId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Titles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserAccessUnitType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](125) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[Description] [nvarchar](250) NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsCardCode] [bit] NOT NULL,
	[IsSerDK] [bit] NOT NULL,
 CONSTRAINT [PK_CardTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserBuildings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[BuildingId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[BuildingObjectId] [int] NULL,
 CONSTRAINT [PK_UserBuildings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserDepartments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[DepartmentId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CurrentDep] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsDepartmentManager] [bit] NOT NULL,
 CONSTRAINT [PK_UsersDepartments2] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserLastMoves](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[NotFinishedMoveTaReportLabelId] [int] NULL,
	[NotFinishedMoveStartTime] [datetime] NOT NULL,
	[NotFinishedJobTaReportLabelId] [int] NULL,
	[NotFinishedJobStartTime] [datetime] NOT NULL,
	[MoveAllowNewWork] [bit] NOT NULL,
	[FirstComeToWork] [datetime] NOT NULL,
	[DepartureFromWork] [datetime] NOT NULL,
	[LastRecordedTimeAtWork] [datetime] NOT NULL,
	[NextMoveBlockedTo] [datetime] NOT NULL,
	[LocationAtWork] [nvarchar](10) NOT NULL,
	[LastMoveTime] [datetime] NOT NULL,
	[LastMoveBOId] [int] NULL,
	[LastEnteredExited] [bit] NOT NULL,
	[EnteredBuilding] [bit] NOT NULL,
	[Savestatus] [int] NOT NULL,
 CONSTRAINT [PK_UserLastMoves] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserPermissionGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[DefaultUserTimeZoneId] [int] NOT NULL,
	[ParentUserPermissionGroupId] [int] NULL,
	[Name] [nvarchar](255) NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[IsOriginal] [bit] NOT NULL,
	[PermissionIsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UserPermissionGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserPermissionGroupTimeZones](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserPermissionGroupId] [int] NOT NULL,
	[UserTimeZoneId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[BuildingObjectId] [int] NOT NULL,
	[IsArming] [bit] NOT NULL,
	[IsDefaultArming] [bit] NOT NULL,
	[IsDisarming] [bit] NOT NULL,
	[IsDefaultDisarming] [bit] NOT NULL,
	[Active] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UserPermissionGroupTimeZones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ValidFrom] [datetime] NOT NULL,
	[ValidTo] [datetime] NOT NULL,
	[CompanyId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_UserRoles_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PersonalId] [nvarchar](50) NULL,
	[FirstName] [nvarchar](20) NOT NULL,
	[LastName] [nvarchar](20) NOT NULL,
	[LoginName] [nvarchar](40) NOT NULL,
	[Password] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL,
	[Comment] [nvarchar](200) NULL,
	[ModifiedBy] [nvarchar](40) NOT NULL,
	[ModifiedLast] [datetime] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
	[OccupationName] [nvarchar](100) NULL,
	[PhoneNumber] [nvarchar](100) NULL,
	[WorkHours] [smallint] NULL,
	[GroupId] [int] NULL,
	[Birthday] [datetime] NULL,
	[Birthplace] [nvarchar](100) NULL,
	[FamilyState] [nvarchar](75) NULL,
	[Citizenship] [nvarchar](75) NULL,
	[Residence] [nvarchar](75) NULL,
	[Nation] [nvarchar](75) NULL,
	[TitleId] [int] NULL,
	[CompanyId] [int] NULL,
	[ContractNum] [nvarchar](50) NULL,
	[ContractStartDate] [datetime] NULL,
	[ContractEndDate] [datetime] NULL,
	[PermitOfWork] [datetime] NULL,
	[PermissionCallGuests] [bit] NULL,
	[MillitaryAssignment] [bit] NULL,
	[Image] [varbinary](max) NULL,
	[PersonalCode] [nvarchar](20) NULL,
	[ExternalPersonalCode] [nvarchar](20) NULL,
	[LanguageId] [int] NULL,
	[RegistredStartDate] [datetime] NOT NULL,
	[RegistredEndDate] [datetime] NOT NULL,
	[TableNumber] [int] NULL,
	[WorkTime] [bit] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [nvarchar](50) NOT NULL,
	[PIN1] [nvarchar](50) NULL,
	[PIN2] [nvarchar](50) NULL,
	[EServiceAllowed] [bit] NULL,
	[ClassificatorValueId] [int] NULL,
	[IsVisitor] [bit] NULL,
	[CardAlarm] [bit] NULL,
	[IsShortTermVisitor] [bit] NULL,
	[ApproveTerminals] [bit] NULL,
	[ApproveVisitor] [bit] NULL,
	[ApproveVisitors] [bit] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [dbo].[UsersAccessUnit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[TypeId] [int] NULL,
	[CompanyId] [int] NULL,
	[Serial] [nvarchar](3) NULL,
	[Code] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL,
	[Free] [bit] NOT NULL,
	[Opened] [datetime] NULL,
	[Closed] [datetime] NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
	[Timestamp] [timestamp] NOT NULL,
	[Dk] [nvarchar](5) NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[ClassificatorValueId] [int] NULL,
	[BuildingId] [int] NOT NULL,
	[Classificator_dt] [datetime] NULL,
	[Comment] [nchar](150) NULL,
 CONSTRAINT [PK_UsersAccessUnit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserTimeZoneProperties](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserTimeZoneId] [int] NOT NULL,
	[TimeZoneId] [int] NULL,
	[OrderInGroup] [int] NOT NULL,
	[ValidFrom] [datetime] NULL,
	[ValidTo] [datetime] NULL,
	[IsMonday] [bit] NOT NULL,
	[IsTuesday] [bit] NOT NULL,
	[IsWednesday] [bit] NOT NULL,
	[IsThursday] [bit] NOT NULL,
	[IsFriday] [bit] NOT NULL,
	[IsSaturday] [bit] NOT NULL,
	[IsSunday] [bit] NOT NULL,
	[Timestamp] [timestamp] NOT NULL,
 CONSTRAINT [PK_UserTimeZoneProperties] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[UserTimeZones](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[TimeZoneId] [int] NULL,
	[Timestamp] [timestamp] NOT NULL,
	[ParentUserTimeZoneId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Uid] [uniqueidentifier] NOT NULL,
	[IsOriginal] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsCompanySpecific] [bit] NOT NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK_UserTimeZones] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[Visitors](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CarNr] [nvarchar](50) NULL,
	[UserId] [int] NULL,
	[FirstName] [nvarchar](50) NULL,
	[CarType] [nvarchar](50) NULL,
	[StartDateTime] [datetime] NULL,
	[StopDateTime] [datetime] NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsUpdated] [bit] NOT NULL,
	[UpdateDatetime] [datetime] NULL,
	[LastChange] [datetime] NOT NULL,
	[CompanyId] [int] NULL,
	[Timestamp] [timestamp] NOT NULL,
	[Accept] [bit] NOT NULL,
	[AcceptUserId] [int] NULL,
	[AcceptDateTime] [datetime] NULL,
	[LastName] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL,
	[Company] [nvarchar](50) NULL,
	[ParentVisitorsId] [int] NULL,
	[Comment] [nvarchar](200) NULL,
	[ReturnDate] [datetime] NULL,
	[Email] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](20) NULL,
	[IsCarNrAccessUnit] [bit] NOT NULL,
	[IsPhoneNrAccessUnit] [bit] NOT NULL,
	[ResponsibleUserId] [int] NULL,
	[CardNeedReturn] [bit] NOT NULL,
 CONSTRAINT [PK_Visitors] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET IDENTITY_INSERT [dbo].[Buildings] ON 

INSERT [dbo].[Buildings] ([Id], [AdressStreet], [AdressHouse], [AdressIndex], [LocationId], [Name], [IsDeleted], [Floors], [TimediffGMTMinutes], [TimezoneId]) VALUES (1, N'virtual', N'6', N'223344', 1, N'Pärnu mnt. 102', 0, 4, 120, N'FLE Standard Time')
SET IDENTITY_INSERT [dbo].[Buildings] OFF
SET IDENTITY_INSERT [dbo].[Classificators] ON 

INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (1, N'User deactivation reasons', NULL)
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (2, N'Company deactivation reasons', NULL)
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (3, N'Company activation reasons', NULL)
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (4, N'User activation reasons', NULL)
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (5, N'Card deactivation reasons', NULL)
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (6, N'Card activation reasons', NULL)
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (8, N'test', N'test')
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (9, N'name', N'comment')
INSERT [dbo].[Classificators] ([Id], [Description], [Comments]) VALUES (10, N'License', N'View licensis')
SET IDENTITY_INSERT [dbo].[Classificators] OFF
SET IDENTITY_INSERT [dbo].[ClassificatorValues] ON 

INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (1, 1, N'maternity leave', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (2, 1, N'Sickness', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (5, 2, N'Unpaid bills', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (6, 2, N'Other', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (7, 2, N'Company has left the building', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (11, 4, N'transfer from other company', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (19, 5, N'Broken card', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (20, 5, N'stolen card', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (21, 6, N'card is found', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (22, 5, N'Owner quit his job', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (30, 4, N'Back from maternity leave', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (32, 3, N'All bills are paid', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (33, 3, N'Back from other building', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (42, 1, N'quit job', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (43, 1, N'change company', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (46, 6, N'3', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (47, 4, N'is kito pastato', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (48, 9, N'Value1', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (49, 9, N'value2', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (50, 10, N'Users', N'Maximum active users in FoxSec Database', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (51, 10, N'Companies', N'Maximum active different Companie managers in FoxSec Database', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (52, 10, N'Doors', N'Maximum doors and elevator floors in FoxSec Database', 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (53, 10, N'Zones', N'Maximum active users in FoxSec Database', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (55, 10, N'Visitors', N'Is visitors registration TAB avalable', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (56, 10, N'Video', N'How many video cameras avalable', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (57, 10, N'Terminals', N'How many mobile devices or web browsers can connected', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (61, 10, N'Time&attendense', N'Are time and attendense reports TAB available', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ClassificatorValues] ([Id], [ClassificatorId], [Value], [Comments], [SortOrder], [DisplayValue], [Legal], [LegalHash], [Remaining], [RemainingHash], [ValidTo], [ValidToHash]) VALUES (62, 10, N'Licence Path', NULL, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ClassificatorValues] OFF
SET IDENTITY_INSERT [dbo].[Countries] ON 

INSERT [dbo].[Countries] ([Id], [Name], [ISONumber], [IsDeleted]) VALUES (1, N'ESTONIA', 233, 0)
SET IDENTITY_INSERT [dbo].[Countries] OFF
SET IDENTITY_INSERT [dbo].[FSINISettings] ON 

INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (1, N'MailSubject', 2, 1, N'FoxSec Message', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (2, N'MailFrom', 2, 1, N'parnumnt102@foxsec.eu', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (3, N'SmtpServer', 2, 1, N'195.222.0.3', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (4, N'SmtpPort', 2, 1, N'25', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (5, N'SmtpUser', 2, 1, N'test', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (6, N'SmtpPsw', 2, 1, N'test', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (8, N'AccessSyncPort-1', 1, 1, N'10000', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (1009, N'HR', 6, 1, N'C:\csvDatabase\RealDatabaseOnServer\FS_EMY.csv', 1)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (1010, N'Backup', 7, 1, N'e:\FSBackup\', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (1011, N'HR', 6, 1, N'C:\csvDatabase\RealDatabaseOnServer\piletid.xml', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (1012, N'DeleteOlderLogDays', 8, 1, N'730', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (1013, N'VisitorMailSubject', 2, 1, N'FoxSec Visitor Card', 0)
INSERT [dbo].[FSINISettings] ([Id], [Name], [SoftType], [SoftId], [Value], [IsDeleted]) VALUES (1014, N'VisitorMailText', 2, 1, N'Hi!;;; 1 Visitor card is Attached! ;Best regards ,;Foxsec', 0)
SET IDENTITY_INSERT [dbo].[FSINISettings] OFF
SET IDENTITY_INSERT [dbo].[Locations] ON 

INSERT [dbo].[Locations] ([Id], [Name], [CountryId], [IsDeleted]) VALUES (1, N'Tallinn', 1, 0)
SET IDENTITY_INSERT [dbo].[Locations] OFF
SET IDENTITY_INSERT [dbo].[LogTypes] ON 

INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (1, 1, N'Errors', N'FF4F4F', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (2, 2, N'Protocol', N'77AACC', 0)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (4, 3, N'AlarmEvent', N'FF0400', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (5, 4, N'WcfCommand', N'CCAA77', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (6, 5, N'NormalLog', N'D9D9D9', 0)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (7, 6, N'Arming', N'665EFF', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (8, 7, N'ArmingInfo', N'92CDDC', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (9, 8, N'DoorAlarm', N'D08100', 0)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (10, 9, N'DoorInfo', N'95DFB8', 0)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (11, 10, N'CardEvent', N'95DFB8', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (12, 11, N'NewCard', N'FB7171', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (13, 12, N'CreditInfo', N'0FFF8D', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (14, 13, N'WebDatabaseChanges', N'00DDAA', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (15, 14, N'WebAuditLog', N'BBAACC', 0)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (16, 15, N'Weberror', N'DDBBAA', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (17, 16, N'WebWarning', N'FFFF66', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (18, 17, N'WebInfo', N'C5D9F1', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (19, 18, N'Warning', N'FFFF66', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (20, 19, N'TechnicalFault', N'FFFF66', 0)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (21, 20, N'CreditInfoAlarm', N'FFA3A3', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (22, 21, N'CardAlarm', N'8899AA', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (23, 22, N'DownloadInfo', N'948A54', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (24, 24, N'ControllerAlarm', N'FB7171', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (27, 25, N'ControllerOk', N'95DFB8', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (28, 26, N'ControllerWarning', N'FFFF66', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (29, 5000, N'ObjectStatus', N'51C5FF', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (30, 500, N'CardEntryOk', N'92D050', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (31, 1500, N'CardExitOk', N'92D050', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (32, 501, N'CardEntryNok', N'FAC540', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (33, 1501, N'CardExitNok', N'FAC540', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (34, 502, N'CardEventEntryNormalNok', N'FFE2AB', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (35, 1502, N'CardEventExitNormalNok', N'FFE2AB', 1)
INSERT [dbo].[LogTypes] ([Id], [NumberOfError], [Name], [Color], [IsDefault]) VALUES (36, 36, N'RecalculateT&A', N'C5D9F1', 0)
SET IDENTITY_INSERT [dbo].[LogTypes] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name], [Permissions], [ModifiedLast], [ModifiedBy], [Description], [Active], [Menues], [IsDeleted], [StaticId], [Priority], [RoleTypeId], [UserId]) VALUES (5, N'Super Admin', 0x00010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, CAST(0x00009ED400E126A1 AS DateTime), N'admin_test', N'Can do everything                                                                                                                                                                                                                                         ', 1, 0x00010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000, 0, 5, 0, 1, NULL)
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[RoleTypes] ON 

INSERT [dbo].[RoleTypes] ([Id], [Name], [IsDeleted], [Menues]) VALUES (1, N'SA', 0, 0x0001010101010101010101010101010101010101010101010101010101010101010101010101010101010000000000000000)
INSERT [dbo].[RoleTypes] ([Id], [Name], [IsDeleted], [Menues]) VALUES (2, N'BA', 0, 0x0001010101010101010101010101010101010101010101010101010101010101010101010101010101010000000000000000)
INSERT [dbo].[RoleTypes] ([Id], [Name], [IsDeleted], [Menues]) VALUES (3, N'CM', 0, 0x0001010101010101010101010101010101010101010101010101010101010101010101010101010101010000000000000000)
INSERT [dbo].[RoleTypes] ([Id], [Name], [IsDeleted], [Menues]) VALUES (4, N'DM', 0, 0x0001010101010101010101010101010101010101010101010101010101010101010101010101010101010000000000000000)
INSERT [dbo].[RoleTypes] ([Id], [Name], [IsDeleted], [Menues]) VALUES (5, N'U', 0, 0x0001010101010101010101010101010101010101010101010101010101010101010101010101010101010000000000000000)
SET IDENTITY_INSERT [dbo].[RoleTypes] OFF
SET IDENTITY_INSERT [dbo].[UserAccessUnitType] ON 

INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (7, N'Proxy card', N'simple card', 0, 1, 1)
INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (8, N'Magnetic card ', NULL, 0, 1, 0)
INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (11, N'Fingerprint', NULL, 0, 1, 0)
INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (13, N'Licence plate ', N'vehicle registretion number', 0, 1, 0)
INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (14, N'PIN', N'pin code', 0, 0, 0)
INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (15, N'Barcode', NULL, 0, 0, 0)
INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (16, N'Mobile ID', N'barrier/gates', 0, 0, 0)
INSERT [dbo].[UserAccessUnitType] ([Id], [Name], [Description], [IsDeleted], [IsCardCode], [IsSerDK]) VALUES (17, N'Iris recognition', NULL, 0, 0, 1)
SET IDENTITY_INSERT [dbo].[UserAccessUnitType] OFF
SET IDENTITY_INSERT [dbo].[UserBuildings] ON 

INSERT [dbo].[UserBuildings] ([Id], [UserId], [BuildingId], [IsDeleted], [BuildingObjectId]) VALUES (1032, 1, 1, 0, NULL)
SET IDENTITY_INSERT [dbo].[UserBuildings] OFF
SET IDENTITY_INSERT [dbo].[UserPermissionGroups] ON 

INSERT [dbo].[UserPermissionGroups] ([Id], [UserId], [DefaultUserTimeZoneId], [ParentUserPermissionGroupId], [Name], [IsOriginal], [PermissionIsActive], [IsDeleted]) VALUES (1, 1, 2, NULL, N' Pärnu mnt. 102-1.Grupp 1', 1, 0, 1)
INSERT [dbo].[UserPermissionGroups] ([Id], [UserId], [DefaultUserTimeZoneId], [ParentUserPermissionGroupId], [Name], [IsOriginal], [PermissionIsActive], [IsDeleted]) VALUES (2, 1, 1, NULL, N' Pärnu mnt. 102-2.Grupp 5xx', 1, 0, 0)
INSERT [dbo].[UserPermissionGroups] ([Id], [UserId], [DefaultUserTimeZoneId], [ParentUserPermissionGroupId], [Name], [IsOriginal], [PermissionIsActive], [IsDeleted]) VALUES (3, 1, 2, NULL, N' Pärnu mnt. 102-3.Belyi', 1, 0, 0)
INSERT [dbo].[UserPermissionGroups] ([Id], [UserId], [DefaultUserTimeZoneId], [ParentUserPermissionGroupId], [Name], [IsOriginal], [PermissionIsActive], [IsDeleted]) VALUES (4, 1, 5, NULL, N' Pärnu mnt. 102-4.ainult tootmine', 1, 0, 0)
INSERT [dbo].[UserPermissionGroups] ([Id], [UserId], [DefaultUserTimeZoneId], [ParentUserPermissionGroupId], [Name], [IsOriginal], [PermissionIsActive], [IsDeleted]) VALUES (5, 1, 5, NULL, N' Pärnu mnt. 102-5.Grupp 5 All Doors', 1, 0, 0)
INSERT [dbo].[UserPermissionGroups] ([Id], [UserId], [DefaultUserTimeZoneId], [ParentUserPermissionGroupId], [Name], [IsOriginal], [PermissionIsActive], [IsDeleted]) VALUES (6, 1, 2, NULL, N' Pärnu mnt. 102-6.Grupp 6', 1, 0, 0)
INSERT [dbo].[UserPermissionGroups] ([Id], [UserId], [DefaultUserTimeZoneId], [ParentUserPermissionGroupId], [Name], [IsOriginal], [PermissionIsActive], [IsDeleted]) VALUES (7, 1, 5, NULL, N' Pärnu mnt. 102-7.AinultPeauks', 1, 0, 1)
SET IDENTITY_INSERT [dbo].[UserPermissionGroups] OFF
SET IDENTITY_INSERT [dbo].[UserRoles] ON 

INSERT [dbo].[UserRoles] ([Id], [UserId], [RoleId], [ValidFrom], [ValidTo], [CompanyId], [IsDeleted]) VALUES (323, 1, 5, CAST(0x0000901A00000000 AS DateTime), CAST(0x0000DD2500000000 AS DateTime), NULL, 0)
SET IDENTITY_INSERT [dbo].[UserRoles] OFF
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [PersonalId], [FirstName], [LastName], [LoginName], [Password], [Email], [Active], [Comment], [ModifiedBy], [ModifiedLast], [OccupationName], [PhoneNumber], [WorkHours], [GroupId], [Birthday], [Birthplace], [FamilyState], [Citizenship], [Residence], [Nation], [TitleId], [CompanyId], [ContractNum], [ContractStartDate], [ContractEndDate], [PermitOfWork], [PermissionCallGuests], [MillitaryAssignment], [Image], [PersonalCode], [ExternalPersonalCode], [LanguageId], [RegistredStartDate], [RegistredEndDate], [TableNumber], [WorkTime], [IsDeleted], [CreatedBy], [PIN1], [PIN2], [EServiceAllowed], [ClassificatorValueId], [IsVisitor], [CardAlarm], [IsShortTermVisitor], [ApproveTerminals], [ApproveVisitor], [ApproveVisitors]) VALUES (1, NULL, N'BuildIn', N'SuperAdmin', N'kasutaja', N'3C74683AADC4A087B42A8141332B256D', N'test@mail.ru', 1, N'asdsad', N'kasutaja', CAST(0x0000A8450141DD4E AS DateTime), NULL, NULL, NULL, NULL, CAST(0x00009D7400000000 AS DateTime), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0x89504E470D0A1A0A0000000D49484452000000740000001E0806000000FDF53A1B000000017352474200AECE1CE90000000467414D410000B18F0BFC6105000000097048597300000EC300000EC301C76FA8640000076F494441546843ED58094F5547147E7FCA34A9B69A6AADC456166DD50A415A445C402D8D0A7651A1A55A8251E3566ADDAA82686D836BEB926A04312E2088A2208A08D6B816A3B13199BEEFF69DFBCE9D77EEDCE5619A104EF2E572CFF9E6CCBCF9E6CCCC2592B7E9BC1AC2E0C190A0830C43820E320C093AC83024E820C390A0830C49095ABCEBB26AEFFDDB1535F5DD623B134AF7B6A9632D7F59ED9FBF7CA5B8DD79F05CD5B73F50EB8F76886D095EE392803E315EB4957202938AD6AAB19372EDF7093397AAF77316A9AC6F7FB5629C9B0C468D4BB7F286C99994A09800933DE87F29B6934022F835E4AEA86B1773C19F8C611C58587A5E08F8E6F0B7D5C74B77A94FD79EB6FE1E3D619A9A5E71D4F2E1094E7AC14A8B8F27DE753F84C2FBE492CD0E1E16069E10149CB7DE1967BD631141601A87094909DAD4F5383605EE264D8C0E70F46AF46B5B4F7625E44B565018C6A38F1D939B32ADD002261C130F4121264D3CFC78C2F7C6B061966810064FF8211EDE11E77CC4B140F08EFC1017F9C1838FC4F7426841E76FB914FBE966C35626B527A032C38A49A657EA40080AC3B8F8160CD1482012830B0A41F83810C313C2E2493C541B72208EBFE1233E71787B02FA430C4FEEE7082D282AC38F796DBB41B65937D3FB1828416138B3292F2613138E0AA2EAE122F08AA318DA814B150A01D11E71F8F14E154A95CB05851FDB3BBD6371E842738416D4CF764BE6B6ED62F59B0C7DD49DEFB5E0D51FBF2879094A1721008BC164A852CA8BC9C67988ED90FE86806E672855129D8DFA194A717E86522EEA0F7EF8F0FEDA04F5BBDD92B96DBBA62AC764EB7CFDAC8518C81D74CBE55CC0EB72A7E7FFBFF0DA04453504317C6E487950796E06E1A4CF13F820A2E9B2155450C06424282A0A9349138AAD101584AD12130D601BA5CA3381AA99F2F901CE68F441B76B026DE340284171AEE8862D11C2B999F47D6712940CC2A25A21222ADA242421A8A05E5B3F090A0148388809F1E85D072657EF870001A436610151697B0E2528DFF6C8B06D990492FEC9E035F16E86FEB180A44F163F79E95C06B038A5DFC38DF2724121189EA84EF831A1108AAA08A0339383DA01E0A20D9DA9108662007C1CB413509F04BE230416D46DBBC52A47F5B899DBB6EB7529F132E4C599CE73865D2892E9B75C3EE1A850BA811270B1E1711E83C0A6B600171CFDF118098E27F773041654DA6EF9678369B54BDB6ED0F358327DB10C94A0F82D7CCC5C5054972408C045E17E7E06D2CD5502AF44EE7F2D824A15C56FB192E064D2B60BF8FDA63519DF7E07425088A95FCAB8A0A63392F3C887EF493F6D017EC6D2270CE04BD0FC1F2E28BF28DB276FA90FA3225FEFEDB780BFDD0C9524E50596EC6E510D86C5E065E89B7255D65D8F79C3197E037E2B1F1F903A73993DD153A302E97102E7493E089AB96CB72B3EC8596C73330A57DA39B8A0E4D311995D7541F9C5896825266B45DB9AC4DC04C437FDDE69F5D5F3D0FDD62C19E5587520B8A010B1F9D613B5FDCF5B8EF170A4E5C745C98A4EBCC401388F7C63529D171EBF402E3D079EE4D31199BBF9A2F20B53F5F9B51DA76E25E4FDAAA655AD3E745D1DBAD8A79A6F3FB1DE79FCBBFD57D5BEB33D9EB751E22397C978EE20489F15172ABBB45AE4009C47BEB08222979E034FF2E98814466F887EB0E2B76BB1E948CE2E470543BE9DA76FAB1B7DFD31AFD31A6F3C4CE81FA83A7E33C64834884DBC35876FC4BCB2F19C4190317BB93DD1D3CBAA450EC079E47B372DD3575B1328079E521C882CD8DAA4FC60FFB9BBB1E948DE4A76B578E66BE97EA2D61DE9B0FB479B234DF762D144039FB8686732E205C52426D427DFD4881C80F3C897965BECABAD09636382E229C58148D1F626E507771F259E672FFE79A53AEEF58B40CCCDAACF74AB2FAA5B8C9CA0869C3456AF4F21FEBB82E0C33971A172CB6B440EC079E4CB2C5E6FFBD267143BF83AE655D55BF9F1E47E2E28F7734416EE68565E288F9E61929DEF7C24F201C4DCEC4AF7538BF347B37BC505B1CE7BCF1C7D6F8C5EAA4CC6B96E985D79C002F77D34B7D41625AF7C8F23C6C179E42B5C77DCF68D1C9DA23EFBB1C1D18663FC943C9BCBC7F05EFA7F82E2C9F91C91453B2F2B2FD45D90FFA5871BA1C407F634DC89B1645B5A7BC5E29984F763BD8F5ED8B9089B8E9905E55C0913F34AEC09C5E4927F72415CA8992B6A1D6D38388FFB53B3E7DB7EFC5DF4D359471CC85EB2C1E640381E234101A92D10C1D9E485DEC72F6253E1B4E57BDB443E8098C97E3E75DBE69EB916EEFBB3EDCE53710C55C7DC2F4F309DAF83268D50B0FAA0E59F525066FB66ADAC4D6847D0798419A5DBD4F01123ED182A35EBF34A3B96363D2E3878D42F015C8A8F4BCF72E45EB8A5D1E244709699E0F6CF84AEFBCF443E479FCB42805DEC7AECE06E3E71D3F2F9B1B69EA7169FB7E740CC64521B0E3EE9C0BC35072DFFD4C2B85073BEAF4D6847E0BC301815159AFAE458BCAD518D193F516C43E3897C1DFDE633A1FC97AB6ACBC9AE04AC3AD02EF239C091DA02A6F688EF6FEC51275BEFDB387CA9CFF24B7C1D6E630636442F4C521B8EDC2F37DA1335397F89ED9F362F2E5441C55E471B0ECE0B82948C2C95BDB052956C3F27E60510CBC859A046688B6EC1DA43D178ABFA170D01BCD07F2FED250000000049454E44AE426082, N'0', N'0', NULL, CAST(0xFFFFF82F00000000 AS DateTime), CAST(0x00003F8500000000 AS DateTime), NULL, 0, 0, N'test', N'LRvbyBf+J4Q=', N'LRvbyBf+J4Q=', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Users] OFF
SET IDENTITY_INSERT [dbo].[UsersAccessUnit] ON 

INSERT [dbo].[UsersAccessUnit] ([Id], [UserId], [TypeId], [CompanyId], [Serial], [Code], [Active], [Free], [Opened], [Closed], [ValidFrom], [ValidTo], [Dk], [IsDeleted], [CreatedBy], [ClassificatorValueId], [BuildingId], [Classificator_dt], [Comment]) VALUES (1, 1, 7, NULL, N'255', NULL, 1, 0, NULL, NULL, CAST(0x0000A36700000000 AS DateTime), CAST(0x000620BB00000000 AS DateTime), N'65534', 0, 1, NULL, 1, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UsersAccessUnit] OFF
SET IDENTITY_INSERT [dbo].[UserTimeZones] ON 

INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (1, 1, 1, NULL, N'Default Time Zone', N'1b88a6b1-eeac-43e3-9422-d8296434d340', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (2, 1, 2, NULL, N'Morning Shift', N'1d3af014-ca2a-4150-8de6-4bd814c295de', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (3, 1, 3, NULL, N'Evening Shift', N'e8c686d7-f6d3-4942-89fc-0598eac43ac9', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (4, 1, 4, NULL, N'Night Shift', N'aa116612-2797-4d23-a3e2-e27e645f979c', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (5, 1, 5, NULL, N'24/7 H', N'5f5d7439-f0cf-4fdf-a94c-c1ba5226b803', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (6, 1, 6, NULL, N'07:50-20:00 MTWTFSS', N'5c603e1c-4d99-484e-85b7-63fbf44eec72', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (7, 1, 7, NULL, N'07:50-22:00 MTWTFSS', N'2eb8b96f-1dbc-4c81-ae00-866dae9127f6', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (8, 1, 8, NULL, N'07:50-22:00 MTWTF', N'9f9e5990-5939-424b-b633-ac0104550a76', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (9, 1, 9, NULL, N'Short Frayday', N'a574c47b-2568-4d77-a283-4a116713d2fd', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (10, 1, 10, NULL, N'No lunch', N'73a5b295-9912-4d90-a6f4-1bdbad07f572', 1, 0, 0, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (11, 1, 11, NULL, N'roster', N'4481ab30-c01c-4176-b57c-0a9ebb85abbe', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (12, 1, 12, NULL, N'HEAD TIME ZONE', N'd8fd9c11-2107-44dd-a45f-f70016a68d2f', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (13, 1, 13, NULL, N'mytimezone', N'6a614f1d-fb5f-4564-83e2-fa1a63449ba2', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (14, 1, 14, NULL, N'Defoult Time Zone', N'c17dc489-2b71-4f18-9970-29f9f8ebb0bb', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (15, 1, 15, NULL, N'Miksi', N'ee962c76-799a-4af2-bcd3-ea3ac4ff26cb', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (16, 1, 16, NULL, N'test new', N'd6de6b0b-6b41-4ebb-95d6-be54283ec2de', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (17, 1, 17, NULL, N'test', N'f1e6e3e9-d73f-482d-a2f6-8b1350a38f14', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (18, 1, 18, NULL, N'Tootmine', N'9ccac859-128d-488e-a99f-a46aa870f681', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (19, 1, 19, NULL, N'Test', N'70f59dea-a159-426c-acf8-efd9de122d6d', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (20, 1, 20, NULL, N'A', N'365ceb43-20c6-4918-8ba7-c927ae2e875c', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (21, 1, 20, NULL, N'A', N'078cfe85-07f2-467f-ac03-95388859725c', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (22, 1, 21, NULL, N'B', N'168323e5-11a4-4d0f-ada2-42ba970091b3', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (23, 1, 22, NULL, N'B', N'093d32b8-28d5-43ed-a5f0-7ed5432ecf98', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (24, 1, 23, NULL, N'B', N'f12de27b-6213-48dd-becd-fea06dd8117f', 1, 0, 1, NULL)
INSERT [dbo].[UserTimeZones] ([Id], [UserId], [TimeZoneId], [ParentUserTimeZoneId], [Name], [Uid], [IsOriginal], [IsDeleted], [IsCompanySpecific], [CompanyId]) VALUES (25, 1, 24, NULL, N'test tz', N'd2d74ac3-4016-4366-a856-1c0b4a4317a7', 1, 0, 1, NULL)
SET IDENTITY_INSERT [dbo].[UserTimeZones] OFF
/****** Object:  Index [BuildingObjects_BuildingObjectTypes]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [BuildingObjects_BuildingObjectTypes] ON [dbo].[BuildingObjects]
(
	[TypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [BuildingObjects_Buildings]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [BuildingObjects_Buildings] ON [dbo].[BuildingObjects]
(
	[BuildingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [FK_BuildingObjects_FSControllersNodes]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [FK_BuildingObjects_FSControllersNodes] ON [dbo].[BuildingObjects]
(
	[FSControllerNodeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [Buildings_Locations]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [Buildings_Locations] ON [dbo].[Buildings]
(
	[LocationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [CompanyBuildingObjects_BuildingObjects]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [CompanyBuildingObjects_BuildingObjects] ON [dbo].[CompanyBuildingObjects]
(
	[BuildingObjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [CompanyBuildings_Companies]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [CompanyBuildings_Companies] ON [dbo].[CompanyBuildingObjects]
(
	[CompanyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [Locations_Countries]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [Locations_Countries] ON [dbo].[Locations]
(
	[CountryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [ix_logtypeid]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [ix_logtypeid] ON [dbo].[Log]
(
	[LogTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

SET ANSI_PADDING ON


/****** Object:  Index [IX_Roles]    Script Date: 3/6/2019 1:55:58 AM ******/
ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [IX_Roles] UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserBuildings_BuildingObjects]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserBuildings_BuildingObjects] ON [dbo].[UserBuildings]
(
	[BuildingObjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserBuildings_Buildings]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserBuildings_Buildings] ON [dbo].[UserBuildings]
(
	[BuildingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserBuildings_Users]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserBuildings_Users] ON [dbo].[UserBuildings]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserPermissionGroups_Users]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserPermissionGroups_Users] ON [dbo].[UserPermissionGroups]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserPermissionGroups_UserTimeZones]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserPermissionGroups_UserTimeZones] ON [dbo].[UserPermissionGroups]
(
	[DefaultUserTimeZoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserPermissionGroupTimeZones_BuildingObjects]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserPermissionGroupTimeZones_BuildingObjects] ON [dbo].[UserPermissionGroupTimeZones]
(
	[BuildingObjectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserPermissionGroupTimeZones_UserPermissionGroups]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserPermissionGroupTimeZones_UserPermissionGroups] ON [dbo].[UserPermissionGroupTimeZones]
(
	[UserPermissionGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserPermissionGroupTimeZones_UserTimeZones]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserPermissionGroupTimeZones_UserTimeZones] ON [dbo].[UserPermissionGroupTimeZones]
(
	[UserTimeZoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [IX_UserRoles]    Script Date: 3/6/2019 1:55:58 AM ******/
ALTER TABLE [dbo].[UserRoles] ADD  CONSTRAINT [IX_UserRoles] UNIQUE NONCLUSTERED 
(
	[RoleId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserTimeZoneProperties_TimeZones]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserTimeZoneProperties_TimeZones] ON [dbo].[UserTimeZoneProperties]
(
	[TimeZoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserTimeZoneProperties_UserTimeZones]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserTimeZoneProperties_UserTimeZones] ON [dbo].[UserTimeZoneProperties]
(
	[UserTimeZoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserTimeZones_TimeZones]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserTimeZones_TimeZones] ON [dbo].[UserTimeZones]
(
	[TimeZoneId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

/****** Object:  Index [UserTimeZones_Users]    Script Date: 3/6/2019 1:55:58 AM ******/
CREATE NONCLUSTERED INDEX [UserTimeZones_Users] ON [dbo].[UserTimeZones]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

ALTER TABLE [dbo].[Buildings] ADD  CONSTRAINT [DF_Buildings_Floors]  DEFAULT ((5)) FOR [Floors]

ALTER TABLE [dbo].[BuldingObjectsStatusOrAlarms] ADD  CONSTRAINT [DF_BuldingObjectsStatusOrAlarms_IsStatus]  DEFAULT ((0)) FOR [IsStatus]

ALTER TABLE [dbo].[Companies] ADD  CONSTRAINT [DF_Companies_IsCanUseOwnCards]  DEFAULT ((0)) FOR [IsCanUseOwnCards]

ALTER TABLE [dbo].[Roles] ADD  CONSTRAINT [DF_Roles_Priority]  DEFAULT ((100)) FOR [Priority]

ALTER TABLE [dbo].[RoleTypes] ADD  CONSTRAINT [DF_RoleTypes_Menues]  DEFAULT (0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000) FOR [Menues]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_UserId]  DEFAULT ((1)) FOR [UserId]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Dep_Id]  DEFAULT ((90)) FOR [DepartmentId]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Label]  DEFAULT ('tulp') FOR [Label]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Name]  DEFAULT ('nimetus') FOR [Remark]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Started]  DEFAULT ('2014-04-04 09:00') FOR [Started]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Finished]  DEFAULT ('2014-04-04 17:00') FOR [Finished]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Hours]  DEFAULT ((8)) FOR [Hours]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Hours_Min]  DEFAULT ('08:00') FOR [Hours_Min]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Schedule]  DEFAULT ((1)) FOR [Schedule]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Job_Move]  DEFAULT ('True') FOR [JobNotMove]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_Completed]  DEFAULT ('False') FOR [Completed]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_IsDeleted]  DEFAULT ('False') FOR [IsDeleted]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF__TAMoves__Modifie__61A66D40]  DEFAULT (sysdatetime()) FOR [ModifiedLast]

ALTER TABLE [dbo].[TAMoves] ADD  CONSTRAINT [DF_Moves_ModifiedBy]  DEFAULT ('service') FOR [ModifiedBy]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_ReportLabels_Label]  DEFAULT ('Labelx') FOR [Label]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_RegistratorKey]  DEFAULT (NULL) FOR [RegistratorKey]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_RegistratorMenuNr]  DEFAULT (NULL) FOR [RegistratorMenuNr]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_JobNotMove]  DEFAULT ('False') FOR [JobNotMove]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_ReportLabels_Permanent]  DEFAULT ('True') FOR [Fixed]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_Entered]  DEFAULT ('True') FOR [EnteredEvent]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_ReportLabels_At_work]  DEFAULT ('True') FOR [At_work]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_ReportLabels_Allow_Jobs]  DEFAULT ('True') FOR [Allow_Jobs]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_ReportLabels_DaysNotHours]  DEFAULT ('False') FOR [DaysNotHours]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_ReportLabels_Admin_only]  DEFAULT ('False') FOR [Admin_only]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_RegistratorId]  DEFAULT (NULL) FOR [RegistratorId]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_ShiftId]  DEFAULT (NULL) FOR [ShiftId]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF__TASReport__Modif__480696CE]  DEFAULT (sysdatetime()) FOR [ModifiedLast]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_SaveStatus]  DEFAULT ((0)) FOR [SaveStatus]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_TASReportLabels_Active]  DEFAULT ('True') FOR [Active]

ALTER TABLE [dbo].[TAReportLabels] ADD  CONSTRAINT [DF_ReportLabels_IsDeleted]  DEFAULT ('False') FOR [IsDeleted]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_UserId]  DEFAULT ((1)) FOR [UserId]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_Dep_Id]  DEFAULT ((90)) FOR [DepartmentId]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_Name]  DEFAULT ('header') FOR [Name]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_ReportDate]  DEFAULT ((201401)) FOR [ReportDate]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_Day]  DEFAULT ((1)) FOR [Day]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_Hours]  DEFAULT ((8)) FOR [Hours]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_Hours_Min]  DEFAULT ('08:00') FOR [Hours_Min]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_Shift]  DEFAULT ((0)) FOR [Shift]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_Completed]  DEFAULT ('False') FOR [Completed]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_IsDeleted]  DEFAULT ('False') FOR [IsDeleted]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF__TAReports__Modif__71DCD509]  DEFAULT (sysdatetime()) FOR [ModifiedLast]

ALTER TABLE [dbo].[TAReports] ADD  CONSTRAINT [DF_TAReports_ModifiedId]  DEFAULT ((1)) FOR [ModifiedId]

ALTER TABLE [dbo].[UsersAccessUnit] ADD  CONSTRAINT [DF_UsersAccessUnit_BuildingId]  DEFAULT ((1)) FOR [BuildingId]

ALTER TABLE [dbo].[UserTimeZones] ADD  CONSTRAINT [DF_UserTimeZones_IsCompanySpecific]  DEFAULT ((0)) FOR [IsCompanySpecific]

ALTER TABLE [dbo].[BuildingObjects]  WITH CHECK ADD  CONSTRAINT [FK_BuildingObjects_BuildingObjectTypes] FOREIGN KEY([TypeId])
REFERENCES [dbo].[BuildingObjectTypes] ([Id])

ALTER TABLE [dbo].[BuildingObjects] CHECK CONSTRAINT [FK_BuildingObjects_BuildingObjectTypes]

ALTER TABLE [dbo].[BuildingObjects]  WITH CHECK ADD  CONSTRAINT [FK_BuildingObjects_Buildings] FOREIGN KEY([BuildingId])
REFERENCES [dbo].[Buildings] ([Id])

ALTER TABLE [dbo].[BuildingObjects] CHECK CONSTRAINT [FK_BuildingObjects_Buildings]

ALTER TABLE [dbo].[BuildingObjects]  WITH CHECK ADD  CONSTRAINT [FK_BuildingObjects_FSControllersNodes] FOREIGN KEY([FSControllerNodeId])
REFERENCES [dbo].[FSControllersNodes] ([ID])

ALTER TABLE [dbo].[BuildingObjects] CHECK CONSTRAINT [FK_BuildingObjects_FSControllersNodes]

ALTER TABLE [dbo].[Buildings]  WITH CHECK ADD  CONSTRAINT [FK_Buildings_Locations] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Locations] ([Id])

ALTER TABLE [dbo].[Buildings] CHECK CONSTRAINT [FK_Buildings_Locations]

ALTER TABLE [dbo].[ClassificatorValues]  WITH CHECK ADD  CONSTRAINT [FK_ClassificatorValues_Classificators] FOREIGN KEY([ClassificatorId])
REFERENCES [dbo].[Classificators] ([Id])

ALTER TABLE [dbo].[ClassificatorValues] CHECK CONSTRAINT [FK_ClassificatorValues_Classificators]

ALTER TABLE [dbo].[Companies]  WITH CHECK ADD  CONSTRAINT [FK_Companies_ClassificatorValues] FOREIGN KEY([ClassificatorValueId])
REFERENCES [dbo].[ClassificatorValues] ([Id])

ALTER TABLE [dbo].[Companies] CHECK CONSTRAINT [FK_Companies_ClassificatorValues]

ALTER TABLE [dbo].[CompanyBuildingObjects]  WITH NOCHECK ADD  CONSTRAINT [FK_CompanyBuildingObjects_BuildingObjects] FOREIGN KEY([BuildingObjectId])
REFERENCES [dbo].[BuildingObjects] ([Id])

ALTER TABLE [dbo].[CompanyBuildingObjects] NOCHECK CONSTRAINT [FK_CompanyBuildingObjects_BuildingObjects]

ALTER TABLE [dbo].[CompanyBuildingObjects]  WITH CHECK ADD  CONSTRAINT [FK_CompanyBuildings_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[CompanyBuildingObjects] CHECK CONSTRAINT [FK_CompanyBuildings_Companies]

ALTER TABLE [dbo].[CompanyManagers]  WITH CHECK ADD  CONSTRAINT [FK_CompanyManagers_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[CompanyManagers] CHECK CONSTRAINT [FK_CompanyManagers_Companies]

ALTER TABLE [dbo].[CompanyManagers]  WITH CHECK ADD  CONSTRAINT [FK_CompanyManagers_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[CompanyManagers] CHECK CONSTRAINT [FK_CompanyManagers_Users]

ALTER TABLE [dbo].[Departments]  WITH CHECK ADD  CONSTRAINT [FK_Departments_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[Departments] CHECK CONSTRAINT [FK_Departments_Companies]

ALTER TABLE [dbo].[FSBuildingObjectCameras]  WITH CHECK ADD  CONSTRAINT [FK_FSBuildingObjectCameras_BuildingObjects] FOREIGN KEY([BuildingObjectId])
REFERENCES [dbo].[BuildingObjects] ([Id])

ALTER TABLE [dbo].[FSBuildingObjectCameras] CHECK CONSTRAINT [FK_FSBuildingObjectCameras_BuildingObjects]

ALTER TABLE [dbo].[FSBuildingObjectMessages]  WITH CHECK ADD  CONSTRAINT [FK_FSBuildingObjectMessages_BuildingObjects] FOREIGN KEY([BuildingObjectId])
REFERENCES [dbo].[BuildingObjects] ([Id])

ALTER TABLE [dbo].[FSBuildingObjectMessages] CHECK CONSTRAINT [FK_FSBuildingObjectMessages_BuildingObjects]

ALTER TABLE [dbo].[FSBuildingObjectMessages]  WITH CHECK ADD  CONSTRAINT [FK_FSBuildingObjectMessages_FSMessages] FOREIGN KEY([MessageId])
REFERENCES [dbo].[FSMessages] ([Id])

ALTER TABLE [dbo].[FSBuildingObjectMessages] CHECK CONSTRAINT [FK_FSBuildingObjectMessages_FSMessages]

ALTER TABLE [dbo].[FSCameras]  WITH CHECK ADD  CONSTRAINT [FK_FSCameras_FSVideoServers] FOREIGN KEY([ServerNr])
REFERENCES [dbo].[FSVideoServers] ([Id])

ALTER TABLE [dbo].[FSCameras] CHECK CONSTRAINT [FK_FSCameras_FSVideoServers]

ALTER TABLE [dbo].[FSControllersNodes]  WITH CHECK ADD  CONSTRAINT [FK_FSControllersNodes_FsControllerNodeTypes] FOREIGN KEY([ControllerTypeId])
REFERENCES [dbo].[FsControllerNodeTypes] ([Id])

ALTER TABLE [dbo].[FSControllersNodes] CHECK CONSTRAINT [FK_FSControllersNodes_FsControllerNodeTypes]

ALTER TABLE [dbo].[FSControllersNodes]  WITH CHECK ADD  CONSTRAINT [FK_FSControllersNodes_FSDatalines] FOREIGN KEY([DataLineId])
REFERENCES [dbo].[FSDatalines] ([ID])

ALTER TABLE [dbo].[FSControllersNodes] CHECK CONSTRAINT [FK_FSControllersNodes_FSDatalines]

ALTER TABLE [dbo].[FSControllersTZsMemoryPosSave]  WITH CHECK ADD  CONSTRAINT [FK_FSControllersTZsMemoryPosSave_FSControllersNodes] FOREIGN KEY([ControllerId])
REFERENCES [dbo].[FSControllersNodes] ([ID])

ALTER TABLE [dbo].[FSControllersTZsMemoryPosSave] CHECK CONSTRAINT [FK_FSControllersTZsMemoryPosSave_FSControllersNodes]

ALTER TABLE [dbo].[FsControllersUsersSaveStatus]  WITH CHECK ADD  CONSTRAINT [FK_FsControllersUsersSaveStatus_FSControllersNodes] FOREIGN KEY([ControllerId])
REFERENCES [dbo].[FSControllersNodes] ([ID])

ALTER TABLE [dbo].[FsControllersUsersSaveStatus] CHECK CONSTRAINT [FK_FsControllersUsersSaveStatus_FSControllersNodes]

ALTER TABLE [dbo].[FsControllersUsersSaveStatus]  WITH CHECK ADD  CONSTRAINT [FK_FsControllersUsersSaveStatus_FsProjectsUsersMemoryPos] FOREIGN KEY([FsProjectsUsersMemoryPosID])
REFERENCES [dbo].[FsProjectsUsersMemoryPos] ([Id])

ALTER TABLE [dbo].[FsControllersUsersSaveStatus] CHECK CONSTRAINT [FK_FsControllersUsersSaveStatus_FsProjectsUsersMemoryPos]

ALTER TABLE [dbo].[FsControllersUsersSaveStatus]  WITH CHECK ADD  CONSTRAINT [FK_FsControllersUsersSaveStatus_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[FsControllersUsersSaveStatus] CHECK CONSTRAINT [FK_FsControllersUsersSaveStatus_Users]

ALTER TABLE [dbo].[FSDatalines]  WITH CHECK ADD  CONSTRAINT [FK_FSDatalines_FsPanelTypes] FOREIGN KEY([PanelTypeId])
REFERENCES [dbo].[FsPanelTypes] ([Id])

ALTER TABLE [dbo].[FSDatalines] CHECK CONSTRAINT [FK_FSDatalines_FsPanelTypes]

ALTER TABLE [dbo].[FSDatalines]  WITH CHECK ADD  CONSTRAINT [FK_FSDatalines_FSProjects] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[FSProjects] ([Id])

ALTER TABLE [dbo].[FSDatalines] CHECK CONSTRAINT [FK_FSDatalines_FSProjects]

ALTER TABLE [dbo].[FSDatalines]  WITH CHECK ADD  CONSTRAINT [FK_FSDatalines_FSServers] FOREIGN KEY([ServerID])
REFERENCES [dbo].[FSServers] ([Id])

ALTER TABLE [dbo].[FSDatalines] CHECK CONSTRAINT [FK_FSDatalines_FSServers]

ALTER TABLE [dbo].[FSMessages]  WITH CHECK ADD  CONSTRAINT [FK_FSMessages_FSProjects] FOREIGN KEY([ProjectID])
REFERENCES [dbo].[FSProjects] ([Id])

ALTER TABLE [dbo].[FSMessages] CHECK CONSTRAINT [FK_FSMessages_FSProjects]

ALTER TABLE [dbo].[FsProjectsUsersMemoryPos]  WITH CHECK ADD  CONSTRAINT [FK_FsProjectsUsersMemoryPos_FSProjects] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[FSProjects] ([Id])

ALTER TABLE [dbo].[FsProjectsUsersMemoryPos] CHECK CONSTRAINT [FK_FsProjectsUsersMemoryPos_FSProjects]

ALTER TABLE [dbo].[FSVideoServers]  WITH CHECK ADD  CONSTRAINT [FK_FSVideoServers_FSProjects] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[FSProjects] ([Id])

ALTER TABLE [dbo].[FSVideoServers] CHECK CONSTRAINT [FK_FSVideoServers_FSProjects]

ALTER TABLE [dbo].[Locations]  WITH CHECK ADD  CONSTRAINT [FK_Locations_Countries] FOREIGN KEY([CountryId])
REFERENCES [dbo].[Countries] ([Id])

ALTER TABLE [dbo].[Locations] CHECK CONSTRAINT [FK_Locations_Countries]

ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Companies]

ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_LogTypes] FOREIGN KEY([LogTypeId])
REFERENCES [dbo].[LogTypes] ([Id])

ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_LogTypes]

ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_TAReportLabel] FOREIGN KEY([TAReportLabelId])
REFERENCES [dbo].[TAReportLabels] ([Id])

ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_TAReportLabel]

ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Users]

ALTER TABLE [dbo].[LogFilters]  WITH CHECK ADD  CONSTRAINT [FK_LogFilter_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[LogFilters] CHECK CONSTRAINT [FK_LogFilter_Companies]

ALTER TABLE [dbo].[LogFilters]  WITH CHECK ADD  CONSTRAINT [FK_LogFilter_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[LogFilters] CHECK CONSTRAINT [FK_LogFilter_Users]

ALTER TABLE [dbo].[RoleBuildings]  WITH CHECK ADD  CONSTRAINT [FK_RoleBuildings_Buildings] FOREIGN KEY([BuildingId])
REFERENCES [dbo].[Buildings] ([Id])

ALTER TABLE [dbo].[RoleBuildings] CHECK CONSTRAINT [FK_RoleBuildings_Buildings]

ALTER TABLE [dbo].[RoleBuildings]  WITH CHECK ADD  CONSTRAINT [FK_RoleBuildings_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])

ALTER TABLE [dbo].[RoleBuildings] CHECK CONSTRAINT [FK_RoleBuildings_Roles]

ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [FK_Roles_RoleTypes] FOREIGN KEY([RoleTypeId])
REFERENCES [dbo].[RoleTypes] ([Id])

ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [FK_Roles_RoleTypes]

ALTER TABLE [dbo].[Roles]  WITH CHECK ADD  CONSTRAINT [FK_Roles_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[Roles] CHECK CONSTRAINT [FK_Roles_Users]

ALTER TABLE [dbo].[TAMoves]  WITH CHECK ADD  CONSTRAINT [FK_Moves_Departments] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([Id])

ALTER TABLE [dbo].[TAMoves] CHECK CONSTRAINT [FK_Moves_Departments]

ALTER TABLE [dbo].[TAMoves]  WITH CHECK ADD  CONSTRAINT [FK_Moves_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[TAMoves] CHECK CONSTRAINT [FK_Moves_Users]

ALTER TABLE [dbo].[TAReportLabels]  WITH CHECK ADD  CONSTRAINT [FK_ReportLabels_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[TAReportLabels] CHECK CONSTRAINT [FK_ReportLabels_Companies]

ALTER TABLE [dbo].[TAReports]  WITH CHECK ADD  CONSTRAINT [FK_TAReports_Buildings] FOREIGN KEY([BuildingId])
REFERENCES [dbo].[Buildings] ([Id])

ALTER TABLE [dbo].[TAReports] CHECK CONSTRAINT [FK_TAReports_Buildings]

ALTER TABLE [dbo].[TAReports]  WITH CHECK ADD  CONSTRAINT [FK_TAReports_Departments] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([Id])

ALTER TABLE [dbo].[TAReports] CHECK CONSTRAINT [FK_TAReports_Departments]

ALTER TABLE [dbo].[TAReports]  WITH CHECK ADD  CONSTRAINT [FK_TAReports_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[TAReports] CHECK CONSTRAINT [FK_TAReports_Users]

ALTER TABLE [dbo].[TAShifts]  WITH CHECK ADD  CONSTRAINT [FK_TAShifts_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[TAShifts] CHECK CONSTRAINT [FK_TAShifts_Companies]

ALTER TABLE [dbo].[TAUsersShifts]  WITH CHECK ADD  CONSTRAINT [FK_TAUsersShifts_TAShifts] FOREIGN KEY([TaShiftId])
REFERENCES [dbo].[TAShifts] ([Id])

ALTER TABLE [dbo].[TAUsersShifts] CHECK CONSTRAINT [FK_TAUsersShifts_TAShifts]

ALTER TABLE [dbo].[TAUsersShifts]  WITH CHECK ADD  CONSTRAINT [FK_TAUsersShifts_Users] FOREIGN KEY([UeserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[TAUsersShifts] CHECK CONSTRAINT [FK_TAUsersShifts_Users]

ALTER TABLE [dbo].[Titles]  WITH CHECK ADD  CONSTRAINT [FK_Titles_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[Titles] CHECK CONSTRAINT [FK_Titles_Companies]

ALTER TABLE [dbo].[UserBuildings]  WITH CHECK ADD  CONSTRAINT [FK_UserBuildings_BuildingObjects] FOREIGN KEY([BuildingObjectId])
REFERENCES [dbo].[BuildingObjects] ([Id])

ALTER TABLE [dbo].[UserBuildings] CHECK CONSTRAINT [FK_UserBuildings_BuildingObjects]

ALTER TABLE [dbo].[UserBuildings]  WITH CHECK ADD  CONSTRAINT [FK_UserBuildings_Buildings] FOREIGN KEY([BuildingId])
REFERENCES [dbo].[Buildings] ([Id])

ALTER TABLE [dbo].[UserBuildings] CHECK CONSTRAINT [FK_UserBuildings_Buildings]

ALTER TABLE [dbo].[UserBuildings]  WITH CHECK ADD  CONSTRAINT [FK_UserBuildings_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[UserBuildings] CHECK CONSTRAINT [FK_UserBuildings_Users]

ALTER TABLE [dbo].[UserDepartments]  WITH CHECK ADD  CONSTRAINT [FK_UserDepartments_Departments] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Departments] ([Id])

ALTER TABLE [dbo].[UserDepartments] CHECK CONSTRAINT [FK_UserDepartments_Departments]

ALTER TABLE [dbo].[UserDepartments]  WITH CHECK ADD  CONSTRAINT [FK_UserDepartments_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[UserDepartments] CHECK CONSTRAINT [FK_UserDepartments_Users]

ALTER TABLE [dbo].[UserLastMoves]  WITH CHECK ADD  CONSTRAINT [FK_UserLastMoves_TAReportLabels] FOREIGN KEY([NotFinishedMoveTaReportLabelId])
REFERENCES [dbo].[TAReportLabels] ([Id])

ALTER TABLE [dbo].[UserLastMoves] CHECK CONSTRAINT [FK_UserLastMoves_TAReportLabels]

ALTER TABLE [dbo].[UserLastMoves]  WITH CHECK ADD  CONSTRAINT [FK_UserLastMoves_TAReportLabels1] FOREIGN KEY([NotFinishedJobTaReportLabelId])
REFERENCES [dbo].[TAReportLabels] ([Id])

ALTER TABLE [dbo].[UserLastMoves] CHECK CONSTRAINT [FK_UserLastMoves_TAReportLabels1]

ALTER TABLE [dbo].[UserLastMoves]  WITH CHECK ADD  CONSTRAINT [FK_UserLastMoves_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[UserLastMoves] CHECK CONSTRAINT [FK_UserLastMoves_Users]

ALTER TABLE [dbo].[UserPermissionGroups]  WITH CHECK ADD  CONSTRAINT [FK_UserPermissionGroups_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[UserPermissionGroups] CHECK CONSTRAINT [FK_UserPermissionGroups_Users]

ALTER TABLE [dbo].[UserPermissionGroups]  WITH CHECK ADD  CONSTRAINT [FK_UserPermissionGroups_UserTimeZones] FOREIGN KEY([DefaultUserTimeZoneId])
REFERENCES [dbo].[UserTimeZones] ([Id])

ALTER TABLE [dbo].[UserPermissionGroups] CHECK CONSTRAINT [FK_UserPermissionGroups_UserTimeZones]

ALTER TABLE [dbo].[UserPermissionGroupTimeZones]  WITH CHECK ADD  CONSTRAINT [FK_UserPermissionGroupTimeZones_BuildingObjects] FOREIGN KEY([BuildingObjectId])
REFERENCES [dbo].[BuildingObjects] ([Id])

ALTER TABLE [dbo].[UserPermissionGroupTimeZones] CHECK CONSTRAINT [FK_UserPermissionGroupTimeZones_BuildingObjects]

ALTER TABLE [dbo].[UserPermissionGroupTimeZones]  WITH CHECK ADD  CONSTRAINT [FK_UserPermissionGroupTimeZones_UserPermissionGroups] FOREIGN KEY([UserPermissionGroupId])
REFERENCES [dbo].[UserPermissionGroups] ([Id])

ALTER TABLE [dbo].[UserPermissionGroupTimeZones] CHECK CONSTRAINT [FK_UserPermissionGroupTimeZones_UserPermissionGroups]

ALTER TABLE [dbo].[UserPermissionGroupTimeZones]  WITH CHECK ADD  CONSTRAINT [FK_UserPermissionGroupTimeZones_UserTimeZones] FOREIGN KEY([UserTimeZoneId])
REFERENCES [dbo].[UserTimeZones] ([Id])

ALTER TABLE [dbo].[UserPermissionGroupTimeZones] CHECK CONSTRAINT [FK_UserPermissionGroupTimeZones_UserTimeZones]

ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Companies]

ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Roles] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Roles] ([Id])

ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Roles]

ALTER TABLE [dbo].[UserRoles]  WITH CHECK ADD  CONSTRAINT [FK_UserRoles_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[UserRoles] CHECK CONSTRAINT [FK_UserRoles_Users]

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_ClassificatorValues] FOREIGN KEY([ClassificatorValueId])
REFERENCES [dbo].[ClassificatorValues] ([Id])

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_ClassificatorValues]

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Companies]

ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Titles] FOREIGN KEY([TitleId])
REFERENCES [dbo].[Titles] ([Id])

ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Titles]

ALTER TABLE [dbo].[UsersAccessUnit]  WITH CHECK ADD  CONSTRAINT [FK_UsersAccessUnit_Buildings] FOREIGN KEY([BuildingId])
REFERENCES [dbo].[Buildings] ([Id])

ALTER TABLE [dbo].[UsersAccessUnit] CHECK CONSTRAINT [FK_UsersAccessUnit_Buildings]

ALTER TABLE [dbo].[UsersAccessUnit]  WITH CHECK ADD  CONSTRAINT [FK_UsersAccessUnit_ClassificatorValues] FOREIGN KEY([ClassificatorValueId])
REFERENCES [dbo].[ClassificatorValues] ([Id])

ALTER TABLE [dbo].[UsersAccessUnit] CHECK CONSTRAINT [FK_UsersAccessUnit_ClassificatorValues]

ALTER TABLE [dbo].[UsersAccessUnit]  WITH CHECK ADD  CONSTRAINT [FK_UsersAccessUnit_Companies] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Companies] ([Id])

ALTER TABLE [dbo].[UsersAccessUnit] CHECK CONSTRAINT [FK_UsersAccessUnit_Companies]

ALTER TABLE [dbo].[UsersAccessUnit]  WITH CHECK ADD  CONSTRAINT [FK_UsersAccessUnit_UserAccessUnitType] FOREIGN KEY([TypeId])
REFERENCES [dbo].[UserAccessUnitType] ([Id])

ALTER TABLE [dbo].[UsersAccessUnit] CHECK CONSTRAINT [FK_UsersAccessUnit_UserAccessUnitType]

ALTER TABLE [dbo].[UsersAccessUnit]  WITH CHECK ADD  CONSTRAINT [FK_UsersAccessUnit_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[UsersAccessUnit] CHECK CONSTRAINT [FK_UsersAccessUnit_Users]

ALTER TABLE [dbo].[UserTimeZoneProperties]  WITH CHECK ADD  CONSTRAINT [FK_UserTimeZoneProperties_UserTimeZones] FOREIGN KEY([UserTimeZoneId])
REFERENCES [dbo].[UserTimeZones] ([Id])

ALTER TABLE [dbo].[UserTimeZoneProperties] CHECK CONSTRAINT [FK_UserTimeZoneProperties_UserTimeZones]

ALTER TABLE [dbo].[UserTimeZones]  WITH CHECK ADD  CONSTRAINT [FK_UserTimeZones_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[UserTimeZones] CHECK CONSTRAINT [FK_UserTimeZones_Users]

ALTER TABLE [dbo].[Visitors]  WITH CHECK ADD  CONSTRAINT [FK_Visitors_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])

ALTER TABLE [dbo].[Visitors] CHECK CONSTRAINT [FK_Visitors_Users]

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Order number, to show in quick access menu. 2 and 3 are reserved for ''Job operation'' and ''Going to'' items' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAReportLabels', @level2type=N'COLUMN',@level2name=N'RegistratorMenuNr'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True - this is job label, 
False - this is move label' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAReportLabels', @level2type=N'COLUMN',@level2name=N'JobNotMove'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True - always available(event if isDeleted, not Active or validFrom/validTo not for current date' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAReportLabels', @level2type=N'COLUMN',@level2name=N'Fixed'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True - enter office,
False - exit office' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAReportLabels', @level2type=N'COLUMN',@level2name=N'EnteredEvent'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True - job/move time caclulated based on start and finish dates,
False - job/move time calculated from start date and time to end date and time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAReportLabels', @level2type=N'COLUMN',@level2name=N'DaysNotHours'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0 - Do not ask start and stop time or count during move or job declaration,
1 - Can fill start and stop time during move or job declaration,
2 - Have to fill count during move or job declaration' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAReportLabels', @level2type=N'COLUMN',@level2name=N'AskStartStopCount'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Reference to new Device/Registrator table' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAReportLabels', @level2type=N'COLUMN',@level2name=N'RegistratorId'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'only time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAShifts', @level2type=N'COLUMN',@level2name=N'StartFrom'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'only time' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAShifts', @level2type=N'COLUMN',@level2name=N'FinishAt'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TAShifts'

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'True - have access to Time and attention' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Users', @level2type=N'COLUMN',@level2name=N'WorkTime'

");

                    string strCmd2 = string.Format(@"IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'Proc_camera') AND type in (N'P', N'PC'))
  DROP PROCEDURE [dbo].[Proc_camera]

  IF EXISTS(select * FROM sys.views where name = 'CameraIP')
   DROP View [dbo].[CameraIP]
   
  IF EXISTS(select * FROM sys.views where name = 'VideoAccess')
   DROP View [dbo].[VideoAccess]
");

                    string strcmd3 = string.Format(@"CREATE PROCEDURE [dbo].[Proc_camera]
 @BuildingObjectId int 
AS
BEGIN
select  CameraId from FSBuildingObjectCameras where BuildingObjectId =@BuildingObjectId and IsDeleted=0
end");

                    string strcmd4 = string.Format(@"CREATE view [dbo].[CameraIP]
as
select FSC.ServerNr as ServerId ,FSC.Id AS CameraId,  FSVS.IP , FSVS.UID, FSVS.PWD from FSCameras as FSC
inner join FSVideoServers FSVS on 
FSC.ServerNr= FSVS.Id");

                    string strcmd5 = string.Format(@"CREATE view [dbo].[VideoAccess]
as
select FSC.Name as CameraName , FSC.Id,cmpbo.CompanyId from CompanyBuildingObjects as cmpbo
inner join FSBuildingObjectCameras fsboc on 
cmpbo.BuildingObjectId= fsboc.BuildingObjectId
inner join FSCameras FSC ON FSBOC.CameraId= FSC.Id");


                    try
                    {
                        SqlCommand cmd = new SqlCommand(strCmd, conn);
                        SqlCommand cmd1 = new SqlCommand(strCmd1, conn);
                        SqlCommand cmd2 = new SqlCommand(strCmd2, conn);

                        SqlCommand cmd3 = new SqlCommand(strcmd3, conn);
                        SqlCommand cmd4 = new SqlCommand(strcmd4, conn);
                        SqlCommand cmd5 = new SqlCommand(strcmd5, conn);
                        //  conn.Open();

                        cmd.ExecuteNonQuery();
                        cmd1.ExecuteNonQuery();
                        cmd2.ExecuteNonQuery();
                        cmd3.ExecuteNonQuery();
                        cmd4.ExecuteNonQuery();
                        cmd5.ExecuteNonQuery();

                        //Server server = new Server(new ServerConnection(conn));
                        //server.ConnectionContext.ExecuteNonQuery(strCmd2);
                        //  Server server = new Server(new ServerConnection(conn));
                        //  server.ConnectionContext.ExecuteNonQuery(strCmd);
                        //  server.ConnectionContext.ExecuteNonQuery(strCmd1);
                        msg = "RESTORE DATABASE successfully processed";
                        MessageBox.Show("RESTORE DATABASE successfully processed");
                        Finish.lgdb = msg;
                        //here add code to save licence details in table
                        conn.Close();
                        SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " Database copied successfully....");
                        Insertlicence();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("CREATE DATABASE permission denied in database 'master'. Your login does not have the sysadmin role.");
                        //throw new Exception("Error : \r\n" + ex.ToString());
                        FileWriter.UpdateEventsLogFile(saveNow + " " + ex.ToString());
                        this.panel2.Enabled = false;
                        this.button1.Enabled = false;
                    }
                }

                catch (SqlException ex)
                {
                    FileWriter.UpdateEventsLogFile(saveNow + " " + ex.ToString());
                    MessageBox.Show(ex.ToString());
                    msg = "RESTORE DATABASE not complete successfully";
                    Finish.lgdb1 = msg;
                    this.panel2.Enabled = false;
                    this.button1.Enabled = false;
                }
                finally
                {
                    conn.Dispose();
                    conn.Close();
                }
            }

            conn.Dispose();
            conn.Close();

        }
        private void btnCreateLogin_Click(object sender, EventArgs e)
        {

            string name = textBox2.Text;
            string pass = textBox3.Text;

            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;
            selectedItem = comboBoxSQL.Text;

            SqlConnection conn = new SqlConnection();

            if (comboBox1.SelectedIndex == 0)
            {
                //conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=SSPI;Initial Catalog=;", selectedItem, name, pass);
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Initial Catalog=;", selectedItem, name, pass);
            }
            else
            {
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=false;Connect Timeout=30;Initial Catalog=;", selectedItem, name, pass);
            }
            textBox1.Text = textBox1.Text;
            textBox4.Text = textBox4.Text;
            string namelogin = textBox1.Text;
            string passlogin = textBox4.Text;

            //  int selectedIndex = comboBoxSQL.SelectedIndex;
            // Object selectedItem = comboBoxSQL.SelectedItem;

            //  SqlConnection conn = new SqlConnection(string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=SSPI;Initial Catalog=;", selectedItem, name, pass));

            //SqlConnection conn = new SqlConnection(@"Data Source=.\SQLEXPRESS; Initial Catalog =; Integrated Security=SSPI;");


            // Creating a login specific to SQL Server.
            string strCmd = string.Format(@"CREATE LOGIN {0} WITH PASSWORD = '{1}';", namelogin, passlogin);
            string strCmd1 = string.Format(@"ALTER LOGIN [{0}] WITH DEFAULT_DATABASE=[FoxSecDB], DEFAULT_LANGUAGE=[us_english], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;", namelogin);

            string strCmd2 = string.Format(@"USE [FoxSecDB] ; CREATE USER [{0}] FOR LOGIN [{1}];", namelogin, namelogin);

            //string strCmd3 = string.Format(@"USE [DemoHMVideo] ; ALTER ROLE [db_datareader] ADD MEMBER [{0}];", namelogin );
            //string strCmd4 = string.Format(@"USE [DemoHMVideo] ; ALTER ROLE [db_datawriter] ADD MEMBER [{0}];", namelogin);
            //string strCmd5 = string.Format(@"USE [DemoHMVideo] ; ALTER ROLE [db_owner] ADD MEMBER [{0}];", namelogin);
            string strCmd3 = string.Format(@"USE [FoxSecDB] ; EXEC sp_addrolemember [db_datareader], [{0}];", namelogin);
            string strCmd4 = string.Format(@"USE [FoxSecDB] ; EXEC sp_addrolemember [db_datawriter], [{0}];", namelogin);
            string strCmd5 = string.Format(@"USE [FoxSecDB] ; EXEC sp_addrolemember [db_owner],[{0}];", namelogin);


            //string strCmd2 = "USE [DemoHMVideo] ; CREATE USER [FOXSECWEB_USER] FOR LOGIN [FOXSECWEB_USER];";
            //string strCmd3 = "USE [DemoHMVideo] ; ALTER ROLE [db_datareader] ADD MEMBER [FOXSECWEB_USER];";
            //string strCmd4 = "USE [DemoHMVideo] ; ALTER ROLE [db_datawriter] ADD MEMBER [FOXSECWEB_USER;";
            //string strCmd5 = "USE [DemoHMVideo] ; ALTER ROLE [db_owner] ADD MEMBER [FOXSECWEB_USER];";
            SqlCommand cmd = new SqlCommand(strCmd, conn);
            SqlCommand cmd1 = new SqlCommand(strCmd1, conn);
            SqlCommand cmd2 = new SqlCommand(strCmd2, conn);
            SqlCommand cmd3 = new SqlCommand(strCmd3, conn);
            SqlCommand cmd4 = new SqlCommand(strCmd4, conn);
            SqlCommand cmd5 = new SqlCommand(strCmd5, conn);
            try
            {
                conn.Open();
                cmd.ExecuteNonQuery();
                cmd1.ExecuteNonQuery();
                cmd2.ExecuteNonQuery();
                cmd3.ExecuteNonQuery();
                cmd4.ExecuteNonQuery();
                cmd5.ExecuteNonQuery();

                this.panel2.Enabled = true;
                this.button1.Enabled = true;
                string msg = "Login created successfully";
                MessageBox.Show(msg);
                Finish.lglogin = msg;
                replaceconfig();

            }
            catch (SqlException ex)
            {
                replaceconfig();
                //StartInstall();
                //IIS();
                this.panel2.Enabled = true;
                this.button1.Enabled = true;
                //  msg = "Login already exist";
                msg = "Login already exist. Use another username";
                MessageBox.Show(msg);
                Finish.lglogin1 = msg;
                if (ex.Number == 15025)
                {
                    FileWriter.UpdateEventsLogFile(saveNow + " " + msg);

                }
                else
                    FileWriter.UpdateEventsLogFile(saveNow + " Not Create Login");
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }



        }
        public void replaceconfig()
        {
            string name = textBox2.Text;
            string pass = textBox3.Text;

            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;
            selectedItem = comboBoxSQL.Text;

            SqlConnection conn = new SqlConnection();

            if (comboBox1.SelectedIndex == 0)
            {
                //conn.ConnectionString = string.Format(@"Data Source={0};Integrated Security=SSPI;Initial Catalog=;", selectedItem);
                conn.ConnectionString = string.Format(@"Data Source={0};Initial Catalog=;", selectedItem);

            }
            else
            {
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=false;Connect Timeout=30;Initial Catalog=;", selectedItem, name, pass);

            }
            textBox1.Text = textBox1.Text;
            textBox4.Text = textBox4.Text;
            string namelogin = textBox1.Text;
            string passlogin = textBox4.Text;
            try
            {
                // string connnectionstring;
                var currentpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\");
                var connectionweb = DBConnectionSettings.ReadWebXMLConnectionString(currentpath, "FoxSecDBEntities");
                if (connectionweb.Length > 30)
                {
                    string connnectionstring = "";

                    if (comboBox1.SelectedIndex == 0)
                    {
                        connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={0};Initial Catalog=FoxSecDB;Integrated Security=SSPI;Persist Security Info=True;MultipleActiveResultSets=True\"", selectedItem.ToString());
                    }
                    else
                    {
                        connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={0};Initial Catalog=FoxSecDB;Integrated Security=false;Persist Security Info=True;User ID={1};Password={2};MultipleActiveResultSets=True\"", selectedItem.ToString(), namelogin.ToString(), passlogin.ToString());
                    }

                    //string connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={0};Initial Catalog=FoxSecDB;Persist Security Info=True;MultipleActiveResultSets=True\"", selectedItem.ToString(), namelogin.ToString(), passlogin.ToString());
                    //string connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source='" + selectedItem.ToString() + "';Initial Catalog=FoxSecDB;Persist Security Info=True;User ID='" + namelogin.ToString() + "';Password='" + passlogin.ToString() + "';MultipleActiveResultSets=True\"");
                    // connectionweb = CONNECTIONSTRING_TEMPLATE.Replace();
                    //connectionweb = connectionweb.Replace(@".\sqlexpress", selectedItem.ToString());
                    //connectionweb = connectionweb.Replace("FoxSecWeb", namelogin.ToString());
                    //connectionweb = connectionweb.Replace("FsWeb123", passlogin.ToString());
                    //   connnectionstring = connectionweb;
                    var connectionwebreplace = DBConnectionSettings.WriteWebXMLConnectionString(currentpath, "FoxSecDBEntities", connnectionstring);
                }
                var connectionweb1 = DBConnectionSettings.ReadWebXMLConnectionString(currentpath, "FoxSecDBContext");
                if (connectionweb1.Length > 30)
                {
                    string connnectionstring = "";
                    if (comboBox1.SelectedIndex == 0)
                    {
                        connnectionstring = string.Format("Data Source={0};Initial Catalog=FoxSecDB;Integrated Security=SSPI;Persist Security Info=True;MultipleActiveResultSets=True", selectedItem.ToString());
                    }
                    else
                    {
                        connnectionstring = string.Format("Data Source={0};Initial Catalog=FoxSecDB;User ID={1};Password={2};Integrated Security=false;Persist Security Info=True;MultipleActiveResultSets=True", selectedItem.ToString(), namelogin.ToString(), passlogin.ToString());
                    }


                    //string connnectionstring = string.Format("Data Source={0};Initial Catalog=FoxSecDB;Persist Security Info=True;MultipleActiveResultSets=True", selectedItem.ToString(), namelogin.ToString(), passlogin.ToString());
                    var connectionwebreplace = DBConnectionSettings.WriteWebXMLConnectionString(currentpath, "FoxSecDBContext", connnectionstring);
                }

                var currentpath1 = System.IO.Path.Combine(Install.projectsPath + @"\Portaal\");
                var connectionweb2 = DBConnectionSettings.ReadWebXMLConnectionString(currentpath1, "FoxSecDBEntities1");
                if (connectionweb2.Length > 30)
                {
                    string connnectionstring = "";

                    if (comboBox1.SelectedIndex == 0)
                    {
                        connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={0};Initial Catalog=FoxSecDB;Integrated Security=SSPI;Persist Security Info=True;MultipleActiveResultSets=True\"", selectedItem.ToString());
                    }
                    else
                    {
                        connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={0};Initial Catalog=FoxSecDB;Integrated Security=false;Persist Security Info=True;User ID={1};Password={2};MultipleActiveResultSets=True\"", selectedItem.ToString(), namelogin.ToString(), passlogin.ToString());
                    }

                    //string connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source={0};Initial Catalog=FoxSecDB;Persist Security Info=True;MultipleActiveResultSets=True\"", selectedItem.ToString(), namelogin.ToString(), passlogin.ToString());
                    //string connnectionstring = string.Format("metadata=res://*/FoxSecModel.csdl|res://*/FoxSecModel.ssdl|res://*/FoxSecModel.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source='" + selectedItem.ToString() + "';Initial Catalog=FoxSecDB;Persist Security Info=True;User ID='" + namelogin.ToString() + "';Password='" + passlogin.ToString() + "';MultipleActiveResultSets=True\"");
                    // connectionweb = CONNECTIONSTRING_TEMPLATE.Replace();
                    //connectionweb = connectionweb.Replace(@".\sqlexpress", selectedItem.ToString());
                    //connectionweb = connectionweb.Replace("FoxSecWeb", namelogin.ToString());
                    //connectionweb = connectionweb.Replace("FsWeb123", passlogin.ToString());
                    //   connnectionstring = connectionweb;
                    var connectionwebreplace = DBConnectionSettings.WriteWebXMLConnectionString(currentpath1, "FoxSecDBEntities1", connnectionstring);
                }
                msg = "Config file successfully written";
                Finish.lgconf = msg;

            }
            catch (Exception ex)
            {
                msg = "Config file is not recorded";
                Finish.lgconf1 = msg;
                FileWriter.UpdateEventsLogFile(saveNow + " " + msg + " " + ex.ToString());
            }
            //  InstallConf6.istallConf();
            try
            {
                //   Confexe();
                //    string extractPath1 = Path.Combine(Install.projectsPath + @"\FSConf6\");
                //    Conf ini = new Conf(extractPath1+"SecConf.ini");
                //    string lastproject = ini.IniReadValue("Project", "Last project");



                //    OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + lastproject + "';Jet OLEDB:Database Password=5");
                //    string dbname = "FoxSecDB";
                //    string sqlname= selectedItem.ToString();
                //    string login = namelogin.ToString();
                //    string passlog = passlogin.ToString();
                //    string ip = "127.0.0.1";
                //  //  string sevdat = true;
                //    //string query = "SELECT JrkNr";
                //    //OleDbCommand com = new OleDbCommand(query, con);
                //    //con.Open();
                //    //com.ExecuteNonQuery();

                //    string queryString = "UPDATE ProjectInfo SET DBName=?, Login=?,[Password]=?, PCIP=?, Directory=?, SeveralDatabases=?";// (DBName, Login, [Password]) VALUES ( '" + dbname + "', '" + login + "','" + passlog + "');";

                //    OleDbCommand command = new OleDbCommand(
                //queryString, con);
                //    command.Parameters.Add("@DBName", OleDbType.VarChar).Value = dbname;
                //    command.Parameters.Add("@Login", OleDbType.VarChar).Value = login;
                //    command.Parameters.Add("@[Password]", OleDbType.VarChar).Value = passlog;
                //    command.Parameters.Add("@PCIP", OleDbType.VarChar).Value = ip;
                //    command.Parameters.Add("@Directory", OleDbType.VarChar).Value = sqlname;
                //    command.Parameters.Add("@SeveralDatabases", OleDbType.Boolean).Value = false;
                //    con.Open();
                //  //  command.ExecuteNonQuery();
                //    command.ExecuteNonQuery();
            }
            catch
            {
                //msg = "Config file is not recorded";
                //Finish.lgconf1 = msg;
                //FileWriter.UpdateEventsLogFile(saveNow + msg + ex.ToString());
            }
            //finally
            //{
            //    conn.Dispose();
            //    conn.Close();
            //}

        }
        private void btnGenerate_Click(object sender, EventArgs e)
        {
            string allowedChars = "";
            allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";
            allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";
            allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";
            char[] sep = { ',' };
            string[] arr = allowedChars.Split(sep);
            string passwordString = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < Convert.ToInt32("8"); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                passwordString += temp;
            }
            textBox4.Text = passwordString;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Install form4 = new Install();
            form4.Show();
        }
        public static void StartInstall()
        {
            try
            {
                string bat = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin");

                string battext = @"@ echo off
                                %SystemRoot%\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe " + bat + @"\FoxSecWebService.exe " + Environment.NewLine +
                                    @"REM	-------------------------------------------------------
                                REM	! Run this file as Administrator	
                                REM	! FoxSecWebService.exe.config  <add key='RunAsService' value='Yes' />
                                REM	-------------------------------------------------------" + Environment.NewLine + "net start FoxSecWebService" + Environment.NewLine + "sc config FoxSecWebService start= delayed-auto";
                // System.IO.File.AppendAllText(System.IO.Path.Combine(bat +@"\test.bat"), battext);
                System.IO.File.AppendAllText(System.IO.Path.Combine(bat + @"\FoxSecWebServiceInstall.bat"), battext);
                System.Diagnostics.ProcessStartInfo p = new
                System.Diagnostics.ProcessStartInfo(System.IO.Path.Combine(bat + @"\FoxSecWebServiceInstall.bat"));
                p.UseShellExecute = false;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = p;
                proc.Start();
                proc.WaitForExit();

                System.IO.File.Delete(System.IO.Path.Combine(bat + @"\FoxSecWebServiceInstall.bat"));
                string msg = "FoxSecWebService install and run";
                Finish.lgserv = msg;
                FileWriter.UpdateEventsLogFile(DateTime.Now + " Each process successfully done.");
                SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " FoxSecWebServiceInstall.bat installed successfully....");
            }
            catch (Exception ex)
            {
                DateTime saveNow = DateTime.Now;
                string msg = "FoxSecWebService not installed";
                Finish.lgserv1 = msg;
                FileWriter.UpdateEventsLogFile(saveNow + " " + msg + " " + ex.Message);
                FileWriter.UpdateEventsLogFile(saveNow + " Error: Log file started but not finished successfully!");
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not yet complete. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                System.Windows.Forms.Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                panel3.Enabled = false;
            }
            if (comboBox1.SelectedIndex == 1)
            {
                panel3.Enabled = true;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        //void Confexe()
        //{
        //    try
        //    {
        //        string appPath = AppDomain.CurrentDomain.BaseDirectory;
        //        string zipPath1 = System.IO.Path.Combine(appPath + "SqlSettingfromConfToWeb.zip");

        //        //string extractPath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA";
        //        //string deletePath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FoxSecDb";
        //        string extractPath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin");
        //        //       string deletePath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin\FoxSecDb");
        //        //  Directory.Delete(deletePath1, true); //true - если директория не пуста (удалит и файлы и папки)
        //        //  Directory.CreateDirectory(deletePath1);

        //        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath1, extractPath1);
        //      //  startConfexe();

        //    }
        //    catch (Exception ex)
        //    {
        //       // startConfexe();
        //        // string msg = ex.ToString();
        //        //  MessageBox.Show(msg);
        //        //   Finish.lgcheck1 = msg;
        //        //   FileWriter.UpdateEventsLogFile(saveNow + msg + ex.Message);
        //    }
        //}
        //public static void extractConfexe()
        //{
        //  try
        //    {
        //        string appPath = AppDomain.CurrentDomain.BaseDirectory;
        //        string zipPath1 = System.IO.Path.Combine(appPath + "SqlSettingfromConfToWeb.zip");

        //        //string extractPath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA";
        //        //string deletePath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FoxSecDb";
        //        string extractPath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin");
        //        //       string deletePath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin\FoxSecDb");
        //        //  Directory.Delete(deletePath1, true); //true - если директория не пуста (удалит и файлы и папки)
        //        //  Directory.CreateDirectory(deletePath1);

        //        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath1, extractPath1);

        //    }
        //    catch (Exception ex)
        //    {
        //       // string msg = ex.ToString();
        //      //  MessageBox.Show(msg);
        //        //   Finish.lgcheck1 = msg;
        //     //   FileWriter.UpdateEventsLogFile(saveNow + msg + ex.Message);
        //    }
        //}

        public static void startConfexe()
        {
            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                string extractPath1 = Path.Combine(Install.projectsPath + @"\Web\bin\");
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(Path.Combine(extractPath1 + "SqlSettingfromConfToWeb.exe"));
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;
                System.Diagnostics.Process listFiles;
                listFiles = System.Diagnostics.Process.Start(psi);
                System.IO.StreamReader myOutput = listFiles.StandardOutput;
                listFiles.WaitForExit();
                if (listFiles.HasExited)
                {
                    string output = myOutput.ReadToEnd();
                }

                //using (Process exeProcess = Process.Start(Path.Combine(Install.projectsPath + @"\Web\bin\") + "SQLManagementStudio_x64_ENU.exe"))
                //{
                //    exeProcess.WaitForExit();
                //}

            }
            catch
            {
                // Log error.
            }
        }

        private void Sql_Load(object sender, EventArgs e)
        {

        }

        public string Readlicense(string parameter, string encrypkey)
        {
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string[] lines;
            string data = string.Empty;
            string datacomnr = string.Empty;
            string datauser = string.Empty;
            string datadoor = string.Empty;
            string datazones = string.Empty;
            string datacompanies = string.Empty;
            string dataworktime = string.Empty;
            string dataportal = string.Empty;
            string datavideo = string.Empty;
            string datavisitor = string.Empty;

            string datauserhash = string.Empty;
            string datadoorhash = string.Empty;
            string datazoneshash = string.Empty;
            string datacompanieshash = string.Empty;
            string dataworktimehash = string.Empty;
            string dataportalhash = string.Empty;
            string datavideohash = string.Empty;
            string datavisitorhash = string.Empty;

            string datacompnnr = string.Empty;
            string datacompnrhash = string.Empty;

            if (Program.Repair == true)
            {
                lines = FileReader.ReadLines(Change.licenseFilePath);
                licenseFilePath = Change.licenseFilePath;
            }
            else
            {
                lines = FileReader.ReadLines(Welcome.licenseFilePath);
                licenseFilePath = Welcome.licenseFilePath;
            }

            string remaininghashcode = "";
            lines = FileReader.ReadLines(licenseFilePath);
            try
            {
                for (i = 0; i < lines.Length; i++)
                {

                    line = lines[i];
                    if (line.Contains("[CountRequest]"))
                    {
                        break;
                    }
                    else
                    {
                        if (line.Length > 15)
                        {
                            if (line.Contains("Users"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "Users=");
                                datauser = Decrypt(linedata, true, encrypkey);
                                datauserhash = linedata;
                            }
                            else
                        if (line.Contains("Doors"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "Doors=");

                                datadoor = Decrypt(linedata, true, encrypkey);
                                datadoorhash = linedata;
                            }
                            else
                        if (line.Contains("Zones"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "Zones=");

                                datazones = Decrypt(linedata, true, encrypkey);
                                datazoneshash = linedata;
                            }
                            else
                        if (line.Contains("Companies"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "Companies=");

                                datacompanies = Decrypt(linedata, true, encrypkey);
                                datacompanieshash = linedata;
                            }
                            else
                        if (line.Contains("TimeAndAttendense"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "TimeAndAttendense=");

                                dataworktime = Decrypt(linedata, true, encrypkey);
                                dataworktimehash = linedata;
                            }
                            else
                        if (line.Contains("Terminals"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "Terminals=");

                                dataportal = Decrypt(linedata, true, encrypkey);
                                dataportalhash = linedata;
                            }
                            else
                        if (line.Contains("Video"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "Video=");

                                datavideo = Decrypt(linedata, true, encrypkey);
                                datavideohash = linedata;
                            }
                            else
                        if (line.Contains("Visitors"))
                            {
                                // intEncryptLicNrIdx = i;
                                //strEncryptLicNr = line.Substring(17, line.Length - 17);
                                string linedata = Searcher.TrimEnd(line, "Visitors=");

                                datavisitor = Decrypt(linedata, true, encrypkey);
                                datavisitorhash = linedata;
                            }
                            else
                        if (line.Contains("ValidTo"))
                            {
                                string returndata = Searcher.TrimEnd(line, "ValidTo=");
                            }
                        }
                        if (intEncryptLicNrIdx > 0 & intUniqNrIdx > 0)
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error occurred while reading license: " + ex.Message);
            }

            int Readlicensevale = -1;

            string counter = "0";

            switch (parameter)
            {
                case ("CompUniqNr"):
                    if (datacomnr == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        if (string.Compare(datacomnr.Substring(0, 5), "CompUniqNr", true) == 0)
                        {
                            counter = "1";
                            remaininghashcode = datacompnrhash;
                        }
                    }
                    //  Console.Write(datacomnr);

                    break;
                case "Users":
                    if (datauser == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {

                        if (string.Compare(datauser.Substring(0, 5), "users", true) == 0)
                        {


                            //Encryption.Decrypt(datauser, true);
                            counter = Searcher.TrimEnd(datauser, "users");
                            remaininghashcode = datauserhash;

                        }
                        else
                        {

                            counter = datauser;
                        }

                    }
                    //   Console.Write(comparuser);
                    break;
                case "Doors":
                    if (datadoor == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        if (string.Compare(datadoor.Substring(0, 5), "doors", true) == 0)
                        {


                            //Encryption.Decrypt(datauser, true);
                            counter = Searcher.TrimEnd(datadoor, "doors");
                            remaininghashcode = datadoorhash;

                        }
                    }
                    //  Console.Write(datadoor);
                    break;
                case "Zones":
                    if (datazones == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        if (string.Compare(datazones.Substring(0, 5), "zones", true) == 0)
                        {


                            //Encryption.Decrypt(datauser, true);
                            counter = Searcher.TrimEnd(datazones, "zones");
                            remaininghashcode = datazoneshash;

                        }
                    }
                    //    Console.Write(datazones);
                    break;
                case "Companies":
                    if (datacompanies == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        counter = datacompanies;
                        remaininghashcode = datacompanieshash;
                    }
                    break;
                case "TimeAndAttendense":
                    if (dataworktime == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        if (string.Compare(dataworktime.Substring(0, 17), "timeandattendense", true) == 0)
                        {

                            counter = Searcher.TrimEnd(dataworktime, "timeandattendense");
                            remaininghashcode = dataworktimehash;

                        }
                    }

                    break;
                case "Terminals":
                    if (dataportal == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        if (string.Compare(dataportal.Substring(0, 9), "terminals", true) == 0)
                        {

                            counter = Searcher.TrimEnd(dataportal, "terminals");
                            remaininghashcode = dataportalhash;
                        }
                    }

                    break;
                case "Video":
                    if (datavideo == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        if (string.Compare(datavideo.Substring(0, 5), "video", true) == 0)
                        {

                            counter = Searcher.TrimEnd(datavideo, "video");
                            remaininghashcode = datavideohash;

                        }
                    }

                    break;


                case "Visitors":
                    if (datavisitor == "")
                    {
                        counter = "";
                        remaininghashcode = "";
                    }
                    else
                    {
                        if (string.Compare(datavisitor.Substring(0, 8), "visitors", true) == 0)
                        {

                            counter = Searcher.TrimEnd(datavisitor, "visitors");
                            remaininghashcode = datavisitorhash;

                        }
                    }

                    break;
            }

            if (counter != "0")
            {
                if (Regex.IsMatch(counter, @"^\d+$"))
                {

                    int result = Convert.ToInt32(counter);


                    return result + "_" + remaininghashcode;
                }
                else
                    return Convert.ToString(Readlicensevale);
            }
            else

                return Convert.ToString(Readlicensevale);

        }

        public string ReadlicenseValidTo(string parameter, string encrypkey)
        {
            Int32 i = default(Int32);
            string line = string.Empty;
            string[] lines;

            if (Program.Repair == true)
            {
                lines = FileReader.ReadLines(Change.licenseFilePath);
                licenseFilePath = Change.licenseFilePath;
            }
            else
            {
                lines = FileReader.ReadLines(Welcome.licenseFilePath);
                licenseFilePath = Welcome.licenseFilePath;
            }

            string datavalidto = null;
            string linedata = "";
            lines = FileReader.ReadLines(licenseFilePath);
            int flg = 0;
            for (i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (line.Contains("[CountRequest]"))
                {
                    break;
                }
                else
                {
                    //   TAtest.Readlicense();
                    if (line.Length > 15)
                    {
                        if (line.Contains("ValidTo"))
                        {
                            linedata = Searcher.TrimEnd(line, "ValidTo=");
                            datavalidto = Decrypt(linedata, true, encrypkey);
                            flg = flg + 1;
                        }
                    }
                }

            }
            if (flg == 0)
            {
                return "";
            }
            else
            {
                return datavalidto + "_" + linedata;
            }
        }

        public const string ENCRYPTION_KEY = "A456E4DA104F960563A66DDC";

        public object XMLLogLiterals { get; private set; }
        public object XMLLogMessageHelper { get; private set; }

        public static string Decrypt(string cipherString, bool useHashing, string encrypkey)
        {
            byte[] keyArray;
            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            //Get your key from config file to open the lock!
            //  string key = (string)settingsReader.GetValue(ENCRYPTION_KEY, typeof(String));

            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(encrypkey));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(encrypkey);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

            tdes.Clear();
            return UTF8Encoding.UTF8.GetString(resultArray);
        }

        public void Insertlicence()
        {
            string user = "Users";
            string door = "Doors";
            string zones = "Zones";
            string companies = "Companies";
            string worktime = "TimeAndAttendense";
            string portal = "Terminals";
            string video = "Video";
            string visitor = "Visitors";
            string validTo = "ValidTo";

            int tc = 0;
            string compuniqnr = Spec.ReadCompNr();
            string encrypkey = ENCRYPTION_KEY + compuniqnr;
            string ValidTo = ReadlicenseValidTo(validTo, encrypkey);
            string Usersrec = Readlicense(user, encrypkey);
            if (Usersrec != "-1")
            {
                int remaining = gettotalcount(user, Convert.ToInt32(Usersrec.Split('_')[0]));
                InsertNewLicense(user, Convert.ToInt32(Usersrec.Split('_')[0]), Usersrec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            string Doorsrec = Readlicense(door, encrypkey);
            if (Doorsrec != "-1")
            {
                int remaining = gettotalcount(door, Convert.ToInt32(Doorsrec.Split('_')[0]));
                InsertNewLicense(door, Convert.ToInt32(Doorsrec.Split('_')[0]), Doorsrec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            string Zonesrec = Readlicense(zones, encrypkey);
            if (Zonesrec != "-1")
            {
                int remaining = gettotalcount(zones, Convert.ToInt32(Zonesrec.Split('_')[0]));
                InsertNewLicense(zones, Convert.ToInt32(Zonesrec.Split('_')[0]), Zonesrec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            string Companiesrec = Readlicense(companies, encrypkey);
            if (Companiesrec != "-1")
            {
                int remaining = gettotalcount(companies, Convert.ToInt32(Companiesrec.Split('_')[0]));
                InsertNewLicense(companies, Convert.ToInt32(Companiesrec.Split('_')[0]), Companiesrec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            string TimeAndAttendenserec = Readlicense(worktime, encrypkey);
            if (TimeAndAttendenserec != "-1")
            {
                int remaining = gettotalcount(worktime, Convert.ToInt32(TimeAndAttendenserec.Split('_')[0]));
                InsertNewLicense("Time&attendense", Convert.ToInt32(TimeAndAttendenserec.Split('_')[0]), TimeAndAttendenserec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            string Terminalsrec = Readlicense(portal, encrypkey);
            if (Terminalsrec != "-1")
            {
                int remaining = gettotalcount(portal, Convert.ToInt32(Terminalsrec.Split('_')[0]));
                InsertNewLicense(portal, Convert.ToInt32(Terminalsrec.Split('_')[0]), Terminalsrec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            string Videorec = Readlicense(video, encrypkey);
            if (Videorec != "-1")
            {
                int remaining = gettotalcount(video, Convert.ToInt32(Videorec.Split('_')[0]));
                InsertNewLicense(video, Convert.ToInt32(Videorec.Split('_')[0]), Videorec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            string Visitorsrec = Readlicense(visitor, encrypkey);
            if (Visitorsrec != "-1")
            {
                int remaining = gettotalcount(visitor, Convert.ToInt32(Visitorsrec.Split('_')[0]));
                InsertNewLicense(visitor, Convert.ToInt32(Visitorsrec.Split('_')[0]), Visitorsrec.Split('_')[1], ValidTo, remaining, encrypkey);
                tc = tc + 1;
            }
            if (tc == 0 && !String.IsNullOrEmpty(ValidTo))
            {
                UpdateLicenseValidTo(ValidTo);
            }
            InsertNewLicense("Licence Path", 0, "", "", 0, encrypkey);
            SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " License details saved successfully....");
        }

        public int gettotalcount(string type, int legal)
        {

            int tc = 0;
            int tc1 = 0;
            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;
            selectedItem = comboBoxSQL.Text;
            string name = textBox2.Text;
            string pass = textBox3.Text;

            if (comboBox1.SelectedIndex == 0)
            {
                //conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=SSPI;Initial Catalog=FoxSecDB;", selectedItem, name, pass);
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Initial Catalog=FoxSecDB;", selectedItem, name, pass);
            }
            else
            {
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=false;Connect Timeout=30;Initial Catalog=FoxSecDB;", selectedItem, name, pass);
            }
            conn.Open();
            if (type == "Users")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from users where IsDeleted=0 and Active=1", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            if (type == "Doors")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from BuildingObjects where IsDeleted=0 and (TypeId=8 or TypeId=9)", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            if (type == "Visitors")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from visitors where IsDeleted=0 and StopDateTime>'" + DateTime.Now.ToString("yyyy-MM-dd hh:mm") + "'", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            if (type == "Video")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from FSCameras where Deleted=0 and Port>0", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            if (type == "Terminals")
            {
                SqlCommand cmd = new SqlCommand("Select count(*) from Terminal where IsDeleted=0 and ApprovedDevice=1", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            if (type == "Zones")
            {
                SqlCommand cmd = new SqlCommand("Select count(*) from BuildingObjects where IsDeleted=0 and TypeId=2", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            if (type == "TimeAndAttendense")
            {
                SqlCommand cmd = new SqlCommand("select count(*) from buildings where IsDeleted=0", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            if (type == "Companies")
            {
                SqlCommand cmd = new SqlCommand("select count(distinct CompanyId) from users where IsDeleted=0 and CompanyId is not null and id in (select UserId from uSERROLES where IsDeleted=0 and RoleId in (select id from Roles where IsDeleted=0 and RoleTypeId=3))", conn);
                tc1 = Convert.ToInt32(cmd.ExecuteScalar());
            }
            conn.Close();
            tc = legal - tc1;
            tc = tc < 0 ? 0 : tc;
            return tc;
        }

        public void InsertNewLicense(string type, int value, string remainhashcode, string validto, int remaining, string encrypkey)
        {
            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;
            selectedItem = comboBoxSQL.Text;
            string name = textBox2.Text;
            string pass = textBox3.Text;
            if (comboBox1.SelectedIndex == 0)
            {
                //conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=SSPI;Initial Catalog=FoxSecDB;", selectedItem, name, pass);
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Initial Catalog=FoxSecDB;", selectedItem, name, pass);
            }
            else
            {
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=false;Connect Timeout=30;Initial Catalog=FoxSecDB;", selectedItem, name, pass);
            }
            conn.Open();
            SqlCommand cmd = new SqlCommand("select id from Classificators where Description='Licence' or Description='License'", conn);
            int id = Convert.ToInt32(cmd.ExecuteScalar());

            string str = "";
            if (type == "Licence Path" || type == "License Path")
            {
                str = "update Classificatorvalues set Comments='" + licenseFilePath + "' where ClassificatorId='" + id + "' and Value='" + type + "'";
                SqlCommand cmd1 = new SqlCommand(str, conn);
                cmd1.ExecuteNonQuery();
            }
            else
            {
                int legal = 0;
                int cid = 0;
                DateTime? vldto = null;
                string strvto = "";
                SqlDataAdapter da = new SqlDataAdapter("select Id,legal from Classificatorvalues where ClassificatorId='" + id + "' and Value='" + type + "'", conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    if (Convert.ToString(dr["legal"]) == null || Convert.ToString(dr["legal"]) == "")
                    {
                        legal = 0;
                    }
                    else
                    {
                        legal = Convert.ToInt32(dr["legal"]);
                    }

                    if (Convert.ToString(dr["Id"]) == null || Convert.ToString(dr["Id"]) == "")
                    {
                        cid = 0;
                    }
                    else
                    {
                        cid = Convert.ToInt32(dr["Id"]);
                    }
                }
                if (legal <= value)
                {
                    if (validto != "")
                    {
                        string strvalto = "";
                        char[] characters = (validto.Split('_')[0]).ToCharArray();

                        for (int i = 0; i < characters.Length; i++)
                        {
                            strvalto = (i == 3 || i == 5) ? strvalto = strvalto + characters[i] + "/" : strvalto = strvalto + characters[i];
                        }
                        vldto = Convert.ToDateTime(strvalto);
                        if (vldto == null)
                        {
                            strvto = "";
                        }
                        else
                        {
                            strvto = Convert.ToDateTime(vldto).ToString("yyyy-MM-dd hh:mm");
                        }

                    }
                    string ValidToHash = Encrypttxt(type.ToLower() + validto.Split('_')[0], true, encrypkey);
                    string RemainingHash = Encrypttxt(Convert.ToString(remaining), true, encrypkey);
                    str = "update Classificatorvalues set Legal='" + value + "',LegalHash='" + remainhashcode + "', Remaining='" + remaining + "',RemainingHash='" + RemainingHash + "',ValidTo='" + strvto + "',ValidToHash='" + ValidToHash + "' where ClassificatorId='" + id + "' and Value='" + type + "'";
                    SqlCommand cmd1 = new SqlCommand(str, conn);
                    cmd1.ExecuteNonQuery();
                }
            }
            conn.Close();
        }

        public string Encrypttxt(string toEncrypt, bool useHashing, string encrypkey)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            //   string key = (string)settingsReader.GetValue(ENCRYPTION_KEY, typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(encrypkey));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(encrypkey);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public void UpdateLicenseValidTo(string validto)
        {
            int selectedIndex = comboBoxSQL.SelectedIndex;
            Object selectedItem = comboBoxSQL.SelectedItem;
            selectedItem = comboBoxSQL.Text;
            string name = textBox2.Text;
            string pass = textBox3.Text;
            if (comboBox1.SelectedIndex == 0)
            {
                //conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=SSPI;Initial Catalog=FoxSecDB;", selectedItem, name, pass);
                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Initial Catalog=FoxSecDB;", selectedItem, name, pass);
            }
            else
            {

                conn.ConnectionString = string.Format(@"Data Source={0}; user id={1}; password={2};Integrated Security=false;Connect Timeout=30;Initial Catalog=FoxSecDB;", selectedItem, name, pass);

            }
            conn.Open();
            SqlCommand cmd = new SqlCommand("select id from Classificators where Description='Licence' or Description='License'");
            int id = Convert.ToInt32(cmd.ExecuteScalar());

            {
                string strvalto = "";
                char[] characters = (validto.Split('_')[0]).ToCharArray();

                for (int i = 0; i < characters.Length; i++)
                {
                    strvalto = (i == 3 || i == 5) ? strvalto = strvalto + characters[i] + "/" : strvalto = strvalto + characters[i];
                }
                DateTime vldto = Convert.ToDateTime(strvalto);

                SqlDataAdapter da = new SqlDataAdapter("select Id from Classificatorvalues where ClassificatorId='" + id + "'", conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    SqlCommand cmd1 = new SqlCommand("update Classificatorvalues set ValidTo='" + vldto + "',ValidToHash='" + validto.Split('_')[1] + "' where Id='" + Convert.ToString(dr["Id"]) + "'", conn);
                    cmd1.ExecuteNonQuery();
                }
                conn.Close();
            }
        }

        public void checkcertexist()
        {
            if (System.IO.Directory.Exists(@"C:\Program Files (x86)\Windows Kits\"))
            {
                foreach (string subDir in Directory.GetDirectories(@"C:\Program Files (x86)\Windows Kits\", "*", SearchOption.AllDirectories))
                {
                    if (File.Exists(Path.Combine(subDir, "makecert.exe")))
                        chkhttps.Show();
                }
            }
            else
            {
                chkhttps.Hide();
            }
        }


        //methods are used to enable IIS features
        #region
        static void SetupIIS()
        {
            try
            {
                string command = @"start /w pkgmgr /iu:IIS-WebServerRole;IIS-WebServer;IIS-CommonHttpFeatures;IIS-StaticContent;IIS-DefaultDocument;IIS-DirectoryBrowsing;IIS-HttpErrors;IIS-ApplicationDevelopment;IIS-ISAPIExtensions;IIS-ISAPIFilter;IIS-NetFxExtensibility45;IIS-ASPNET45;IIS-NetFxExtensibility;IIS-ASPNET;IIS-HealthAndDiagnostics;IIS-HttpLogging;IIS-RequestMonitor;IIS-Security;IIS-RequestFiltering;IIS-HttpCompressionStatic;IIS-WebServerManagementTools;IIS-ManagementConsole;WAS-WindowsActivationService;WAS-ProcessModel;WAS-NetFxEnvironment;WAS-ConfigurationAPI";
                ProcessStartInfo pStartInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
                Process p = new Process();
                p.StartInfo = pStartInfo;
                p.Start();
            }
            catch (Exception ex)
            {

            }
        }
        #endregion


    }
}