﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Diagnostics;
using System.ServiceProcess;
using System.IO;
using System.IO.Compression;
using FoxSecWebSetup;
using System.Reflection;
using System.Security.Principal;
using System.Net;
using System.Configuration.Install;
using FoxSecWebSetup.License;
using FoxSecWebSetup.Conf;

namespace SetupWebSetup
{
    public partial class Check : Form
    {

        public static DateTime saveNow = DateTime.Now;
        public string msg = "";
        public static string projectsPath = @"C:\FoxSec";

        public Check()
        {
            InitializeComponent();
            string icon = "icon1.ico";
            this.Icon = new Icon(icon, 60, 60);
            this.Text = "FoxSecWeb Installer";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            //IISstart();
            Version version = GetIISVersion();
            textBoxIIS.Text = version.ToString();
            Version vers = GetNETVersion();
            textBoxNET.Text = vers.ToString();
            try
            {
                using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                {
                    RegistryKey instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", true);

                    if (instanceKey != null)
                    {
                        foreach (var instanceName in instanceKey.GetValueNames())
                        {
                            listBox1.Items.Add(instanceName);
                        }
                    }
                    hklm.Close();
                    instanceKey.Close();
                }
                msg = "Your system meets the system requirements. ";
                Finish.lgcheck = msg;

            }
            catch (Exception ex)
            {
                msg = "Your System Does Not Meet the Minimum System Requirements. SQL Server is not installed";
                MessageBox.Show("Your System Does Not Meet the Minimum System Requirements. SQL Server is not installed. Please click Back and install Sql Express.");
                Finish.lgcheck1 = msg;
                FileWriter.UpdateEventsLogFile(saveNow + " " + msg + " " + ex.Message);
                this.btnNext1.Enabled = false;
            }

        }
        void Sql()
        {
            try
            {
                using (RegistryKey hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                {
                    RegistryKey instanceKey = hklm.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\Instance Names\SQL", true);

                    if (instanceKey != null)
                    {
                        foreach (var instanceName in instanceKey.GetValueNames())
                        {
                            listBox1.Items.Add(instanceName);
                        }
                    }
                    hklm.Close();
                    instanceKey.Close();
                }
                msg = "Your system meets the system requirements. ";
                Finish.lgcheck = msg;

            }
            catch (Exception ex)
            {
                msg = "Your System Does Not Meet the Minimum System Requirements. SQL Server is not installed";
                MessageBox.Show("Your System Does Not Meet the Minimum System Requirements. SQL Server is not installed. Please click Back and install Sql Express.");
                Finish.lgcheck1 = msg;
                FileWriter.UpdateEventsLogFile(saveNow + " " + msg + " " + ex.Message);
                this.btnNext1.Enabled = false;
            }
        }

        public Version GetIISVersion()
        {

            using (RegistryKey key =
            Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\InetStp", false))
            {
                //    if (key == null) {  }
                int majorVersion = (int)key.GetValue("MajorVersion", -1);
                int minorVersion = (int)key.GetValue("MinorVersion", -1);
                //  if (majorVersion == -1 || minorVersion == -1) return new Version("IIS is turn of!!!");
                //  if (majorVersion <= 7) return new Version("IIS version is old!!!");
                return new Version(majorVersion, minorVersion);
            }

        }
        public static void GetWinName()
        {
            string key = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion";

            using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(key))
            {
                if (regKey != null)
                {
                    try
                    {
                        string name = regKey.GetValue("ProductName").ToString();

                        //if (name == "") return "значение отсутствует";

                        //if (name.Contains("XP"))
                        //    return "XP";

                        //else 
                        if (name.Contains("7"))
                        {
                            startasp();
                        }

                        // return "Windows 7";


                        //else
                        //    return "неизвестная версия Windows";
                    }

                    catch
                    {
                        // return ex.Message;
                    }
                }
                //else
                //return "Не удалось получить значение ключа в реестре";
            }
        }
        public static void startasp()
        {
            try
            {
                string bat = System.IO.Path.Combine(Install.projectsPath + @"\bat\bin");
                string batdelete = System.IO.Path.Combine(Install.projectsPath + @"\bat");
                Directory.CreateDirectory(bat);

                string battext = @"@echo on
                C:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis -i";
                // System.IO.File.AppendAllText(System.IO.Path.Combine(bat +@"\test.bat"), battext);
                if (File.Exists(System.IO.Path.Combine(bat + @"\asp.bat")))
                    File.Delete(System.IO.Path.Combine(bat + @"\asp.bat"));
                System.IO.File.AppendAllText(System.IO.Path.Combine(bat + @"\asp.bat"), battext);
                System.Diagnostics.ProcessStartInfo p = new
     System.Diagnostics.ProcessStartInfo(System.IO.Path.Combine(bat + @"\asp.bat"));
                p.UseShellExecute = false;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = p;
                proc.Start();
                proc.WaitForExit();
                proc.Close();
                Directory.Delete(batdelete, true);
            }
            catch (Exception ex)
            {

                FileWriter.UpdateEventsLogFile(saveNow + " " + ex.ToString());
            }
        }
        public static void IISstart()
        {
            try
            {
                string bat = System.IO.Path.Combine(Install.projectsPath + @"\bat\bin");
                string batdelete = System.IO.Path.Combine(Install.projectsPath + @"\bat");
                Directory.CreateDirectory(bat);

                string battext = @"@echo off
                echo ----------------------------------------------------
                echo      Enabling IIS and required components...   
                echo 		    PLEASE WAIT...            
                echo ----------------------------------------------------
                
                echo Installing 1 of 32...
                @echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-WebServerRole
                @echo off
                echo Installing 2 of 32...
                @echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-WebServer
                @echo off
                echo Installing 3 of 32...
                @echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-CommonHttpFeatures
                @echo off
                @echo Installing 4 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-HttpErrors
@echo off
                @echo Installing 5 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-ApplicationDevelopment
@echo off
                @echo Installing 6 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-NetFxExtensibility
@echo off
                @echo Installing 7 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-HealthAndDiagnostics
@echo off
                @echo Installing 8 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-HttpLogging
@echo off
                @echo Installing 9 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-LoggingLibraries
@echo off
                @echo Installing 10 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-Security
@echo off
                @echo Installing 11 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-RequestFiltering
@echo off
                @echo Installing 12 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-Performance
@echo off
                @echo Installing 13 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-WebServerManagementTools
@echo off
                @echo Installing 14 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-ManagementScriptingTools
@echo off
                @echo Installing 15 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-HostableWebCore
@echo off
                @echo Installing 16 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-StaticContent
@echo off
                @echo Installing 17 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-DefaultDocument
@echo off
                @echo Installing 18 of 32...
@echo on
                %windir%\system32\dism.esxe /online /quiet /enable-feature /featurename:IIS-DirectoryBrowsing
@echo off
                @echo Installing 19 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-ISAPIExtensions
@echo off
                @echo Installing 20 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-ISAPIFilter
@echo off
                @echo Installing 21 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-ASPNET
@echo off
                @echo Installing 22 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-ASP
@echo off
                @echo Installing 23 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-CustomLogging
@echo off
                @echo Installing 24 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-BasicAuthentication
@echo off
                @echo Installing 25 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-HttpCompressionStatic
@echo off
                @echo Installing 26 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-ManagementConsole
@echo off
                @echo Installing 27 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:IIS-WindowsAuthentication
@echo off
                @echo Installing 28 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:NetFx3
@echo off
                @echo Installing 29 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /featurename:WCF-TCPPortSharing
@echo off
                @echo Installing 30 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /FeatureName:IIS-ASPNET45
@echo off
                @echo Installing 31 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /FeatureName:IIS-NetFxExtensibility45
@echo off
                @echo Installing 32 of 32...
@echo on
                %windir%\system32\dism.exe /online /quiet /enable-feature /FeatureName:NetFx4Extended-ASPNET45

                @echo off
                echo ----------------------------------------------------
                echo      Windows completed the requested changes.        
                echo ----------------------------------------------------
                ";
                // System.IO.File.AppendAllText(System.IO.Path.Combine(bat +@"\test.bat"), battext);
                if (File.Exists(System.IO.Path.Combine(bat + @"\testiis.bat")))
                    File.Delete(System.IO.Path.Combine(bat + @"\testiis.bat"));
                System.IO.File.AppendAllText(System.IO.Path.Combine(bat + @"\testiis.bat"), battext);
                System.Diagnostics.ProcessStartInfo p = new System.Diagnostics.ProcessStartInfo(System.IO.Path.Combine(bat + @"\testiis.bat"));
                p.UseShellExecute = false;
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo = p;
                proc.Start();
                proc.WaitForExit();
                proc.Close();
                Directory.Delete(batdelete, true);
                SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " IIS congigurations done successfully....");
            }
            catch (Exception ex)
            {
                FileWriter.UpdateEventsLogFile(saveNow + " " + ex.ToString());
            }
        }

        public static Version GetNETVersion()
        {

            var keyName = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Client";
            var key = Registry.LocalMachine.OpenSubKey(keyName);

            if (key != null)
            {
                var version = (string)key.GetValue("Version");
                var installed = (int)key.GetValue("Install");


                if (version.StartsWith("4.5") && installed == 1)
                {
                }
                else
                {
                    dotNet45();
                }

                return new Version(version);
            }
            else
            {

                dotNet45();
            }

            throw new Exception("Impossible!");

        }

        private static void dotNet45()
        {
            try
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                string zipPath = System.IO.Path.Combine(appPath + "dotNetFx45_Full_setup.zip");
                string extractPath = Install.projectsPath + @"\bin";

                ZipFile.ExtractToDirectory(zipPath, extractPath);
            }
            catch (Exception ex)
            {
                FileWriter.UpdateEventsLogFile(saveNow + " Error extract dotNetFx45_Full_setup.exe " + ex.Message);
            }
            try
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(@"C:\FoxSec\Web\bin\dotNetFx45_Full_setup.exe");
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;
                System.Diagnostics.Process listFiles;
                listFiles = System.Diagnostics.Process.Start(psi);
                System.IO.StreamReader myOutput = listFiles.StandardOutput;
                listFiles.WaitForExit();
                if (listFiles.HasExited)
                {
                    string output = myOutput.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                FileWriter.UpdateEventsLogFile(saveNow + " Error start dotNetFx45_Full_setup.exe " + ex.Message);
            }
        }

        private void btnNext1_Click(object sender, EventArgs e)
        {

            this.Hide();

            InstallConf6 form4 = new InstallConf6();
            form4.Show();
        }

        private void btnBack1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Sql form1 = new Sql();
            form1.Show();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not yet complete. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
