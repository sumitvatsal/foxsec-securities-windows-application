﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Management;
using System.Management.Automation.Runspaces;
using System.Management.Automation;
using System.IO;
using CERTENROLLLib;
using System.Net;
using Microsoft.Web.Administration;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Security.Cryptography;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using System.Reflection;
using System.Net.Sockets;
using Microsoft.Win32;
using System.Data.Sql;

namespace FoxSecWebSetup
{
    public partial class Form1 : Form
    {
        RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Classes\Installer\Products");
        private const string keyBase = @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths";

        public string ProcessToKill { get; set; }
        public Form1()
        {
            InitializeComponent();
            RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Uninstall");
            string location = FindByDisplayName(regKey, "SetupFoxsec1");

        }

        private string FindByDisplayName(RegistryKey parentKey, string name)
        {
            string[] nameList = parentKey.GetSubKeyNames();
            for (int i = 0; i < nameList.Length; i++)
            {
                RegistryKey regKey = parentKey.OpenSubKey(nameList[i]);
                try
                {
                    if (regKey.GetValue("DisplayName").ToString() == name)
                    {
                        return regKey.GetValue("InstallLocation").ToString();
                    }
                }
                catch { }
            }
            return "";
        }


        public static string IsApplictionInstalled(string p_name)
        {
            string displayName;
            RegistryKey key;
            string location = "";
            string registryKey = "";
            // search in: LocalMachine_32
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Classes\Installer\Products");
            foreach (String keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName + @"\SourceList");
                //subkey = subkey + @"\SourceList";

                displayName = subkey.GetValue("PackageName") as string;
                if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    string path = subkey.GetValue("LastUsedSource") as string;
                    string[] arr = path.Split(';');
                    location = arr[arr.Length - 1];
                }
            }

            registryKey = @"SOFTWARE\Microsoft\Windows\CurrentVersion\Installer\UserData";
            key = Registry.LocalMachine.OpenSubKey(registryKey);
            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains(p_name))
                    {
                        location = subkey.GetValue("InstallSource") as string;
                    }
                }
                key.Close();
            }

            registryKey = @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall";
            key = Registry.LocalMachine.OpenSubKey(registryKey);
            if (key != null)
            {
                foreach (RegistryKey subkey in key.GetSubKeyNames().Select(keyName => key.OpenSubKey(keyName)))
                {
                    displayName = subkey.GetValue("DisplayName") as string;
                    if (displayName != null && displayName.Contains(p_name))
                    {
                        location = subkey.GetValue("InstallSource") as string;
                    }
                }
                key.Close();
            }
            return location;
        }

        public string[] GetSqlInstances()
        {
            DataRowCollection rows = SqlDataSourceEnumerator.Instance.GetDataSources().Rows;
            string[] instances = new string[rows.Count];

            for (int i = 0; i < rows.Count; i++)
            {
                instances[i] = Convert.ToString(rows[i]["InstanceName"]);
            }

            return instances;
        }
        private void ListSqlServers()
        {
            DataTable dt = SqlDataSourceEnumerator.Instance.GetDataSources();
            foreach (DataRow dr in dt.Rows)
            {
                comboBoxSQL.Items.Add(string.Concat(dr["ServerName"], "\\", dr["InstanceName"]));
            }

        }
        public void createserverinstance()
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "cmd.exe";
            startInfo.Arguments = "/c sqllocaldb create FoxSecMSSQLExpress";
            process.StartInfo = startInfo;
            process.Start();
        }
        private bool TestForServer(string address, int port)
        {
            int timeout = 500;
            if (ConfigurationManager.AppSettings["RemoteTestTimeout"] != null)

                timeout = int.Parse(ConfigurationManager.AppSettings["RemoteTestTimeout"]);
            var result = false;
            try
            {
                using (var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, System.Net.Sockets.ProtocolType.Tcp))
                {

                    IAsyncResult asyncResult = socket.BeginConnect(address, port, null, null);
                    result = asyncResult.AsyncWaitHandle.WaitOne(timeout, true);
                    socket.Close();
                }
                return result;
            }
            catch { return false; }
        }

        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation)
        {
            string directory = Path.Combine(Install.projectsPath + @"\FoxSecConf\FoxSecConf.exe");
            string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcut.Description = "FoxSecConf";   // The description of the shortcut
            shortcut.TargetPath = directory;                 // The path of the file that will launch when the shortcut is run
            shortcut.Save();

            using (var fs = new FileStream(shortcutLocation, FileMode.Open, FileAccess.ReadWrite))
            {
                fs.Seek(21, SeekOrigin.Begin);
                fs.WriteByte(0x22);
            }

        }
        public void checkdiskdetails()
        {
            string[] drives = Environment.GetLogicalDrives();

            foreach (string drive in drives)
            {
                DriveInfo di = new DriveInfo(drive);
                if (di.Name == "C:\\")
                {

                }

            }
        }
        public void checkcertexist()
        {
            string msg = "";
            if (System.IO.Directory.Exists(@"C:\Program Files (x86)\Windows Kits\"))
            {
                foreach (string subDir in Directory.GetDirectories(@"C:\Program Files (x86)\Windows Kits\", "*", SearchOption.AllDirectories))
                {
                    if (File.Exists(Path.Combine(subDir, "makecert.exe")))
                        msg = "1";
                }
                if (msg == "")
                {
                    msg = "0";
                }
            }
            else
            {
                msg = "0";
            }
            if (msg == "1")
            {
                checkBox1.Show();
            }
            else
            {
                checkBox1.Hide();
            }
        }

        private int CheckAvailableServerPort()
        {
            // Evaluate current system tcp connections. This is the same information provided
            // by the netstat command line application, just in .Net strongly-typed object
            // form.  We will look through the list, and if our port we would like to use
            // in our TcpClient is occupied, we will set isAvailable to false.

            int port = 443;
            for (int i = 0; i < 20; i++)
            {
                bool isAvailable = true;
                IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
                IPEndPoint[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpListeners();

                foreach (IPEndPoint endpoint in tcpConnInfoArray)
                {
                    if (endpoint.Port == port)
                    {
                        isAvailable = false;
                        break;
                    }
                }

                if (isAvailable == false)
                {
                    port = port + 1;
                }
                else
                {
                    break;
                }
            }

            return (port);
        }

        public void createcertificate()
        {
            WindowsPrincipal pricipal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            bool hasAdministrativeRight = pricipal.IsInRole(WindowsBuiltInRole.Administrator);
            if (hasAdministrativeRight)
            {
                Random generator = new Random();
                var shell = PowerShell.Create();
                shell.AddCommand("Get-Process");
                shell.Invoke();
                string savepath = Path.Combine(Install.projectsPath + @"\Web");
                string certname = "MyFoxSecCert" + generator.Next(1, 10000).ToString("D4");
                string cerpath = savepath + @"\" + certname + ".cer";
                string pfxpath = savepath + @"\" + certname + ".pfx";
                string strscript = (@"$certificate = New-SelfSignedCertificate `
                -Subject localhost `
                -DnsName localhost `
                -KeyAlgorithm RSA `
                -KeyLength 2048 `
                -NotBefore (Get-Date) `
                -NotAfter (Get-Date).AddYears(20) `
                -CertStoreLocation ‘cert:CurrentUser\My’ `
                -FriendlyName ‘FoxSecSSLCertName’ `
                -HashAlgorithm SHA256 `
                -KeyUsage DigitalSignature, KeyEncipherment, DataEncipherment `
                -TextExtension @(‘2.5.29.37={text}1.3.6.1.5.5.7.3.1’) 
                $certificatePath = 'Cert:\CurrentUser\My\' + ($certificate.ThumbPrint)  

                # create temporary certificate path
                $tmpPath = ‘certsavepath’
                If(!(test-path $tmpPath))
                {
                New-Item -ItemType Directory -Force -Path $tmpPath
                }
                
                # set certificate password here
                $pfxPassword = ConvertTo-SecureString -String ‘foxsecsslcert@123’ -Force -AsPlainText
                $pfxFilePath = ‘pfxfilepath’
                $cerFilePath = ‘certfilepath’
                
                # create pfx certificate
                Export-PfxCertificate -Cert $certificatePath -FilePath $pfxFilePath -Password $pfxPassword
                Export-Certificate -Cert $certificatePath -FilePath $cerFilePath
                
                # import the pfx certificate
                Import-PfxCertificate -FilePath $pfxFilePath Cert:\LocalMachine\My -Password $pfxPassword -Exportable
                
                # trust the certificate by importing the pfx certificate into your trusted root
                Import-Certificate -FilePath $cerFilePath -CertStoreLocation Cert:\CurrentUser\Root
                
                # optionally delete the physical certificates (don’t delete the pfx file as you need to copy this to your app directory)
                # Remove-Item $pfxFilePath
                Remove-Item $cerFilePath").Replace("certsavepath", savepath).Replace("pfxfilepath", pfxpath).Replace("certfilepath", cerpath).Replace("FoxSecSSLCertName", certname);
                // Add the script to the PowerShell object
                shell.Commands.AddScript(strscript);

                // Execute the script
                var results = shell.Invoke();

                // display results, with BaseObject converted to string
                // Note : use |out-string for console-like output
                if (results.Count > 0)
                {
                    // We use a string builder ton create our result text
                    var builder = new StringBuilder();

                    foreach (var psObject in results)
                    {
                        // Convert the Base Object to a string and append it to the string builder.
                        // Add \r\n for line breaks
                        builder.Append(psObject.BaseObject.ToString() + "\r\n");
                    }

                    // Encode the string in HTML (prevent security issue with 'dangerous' caracters like < >

                }

            }
            else
            {
                if (MessageBox.Show("This application requires admin privilages.\nClick Ok to elevate or Cancel to exit.", "Elevate?", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                {
                    ProcessStartInfo processInfo = new ProcessStartInfo();
                    processInfo.Verb = "runas";
                    processInfo.FileName = System.Windows.Forms.Application.ExecutablePath;
                    try
                    {
                        Process.Start(processInfo);
                    }
                    catch (Win32Exception ex)
                    {
                        System.Windows.Forms.Application.Exit();
                    }
                    System.Windows.Forms.Application.Exit();
                }
                else
                {
                    System.Windows.Forms.Application.Exit();
                }
            }
        }

        public void ImportCertificatetoIIS()
        {
            string directory = Path.Combine(Install.projectsPath + @"\Web");
            string pfxpath = directory + @"\" + "MyFoxSecCert1322" + ".pfx";
            string pass = "foxsecsslcert@123";

            String xMachineName = Environment.MachineName;
            String xSSLCertificate = pfxpath;
            String xSSLCertificatePassword = pass;
            using (Microsoft.Web.Administration.ServerManager iisManager = new Microsoft.Web.Administration.ServerManager())
            {
                X509Store store = new X509Store("WebHosting", StoreLocation.LocalMachine);
                store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadWrite);
                X509Certificate2 certificate = new X509Certificate2(xSSLCertificate, xSSLCertificatePassword);
                store.Add(certificate);
                store.Close();
                iisManager.CommitChanges();
            }
        }

        public void IIS()
        {
            //string name = "FoxSecWeb";
            //string physicalPath = Path.Combine(Install.projectsPath + @"\Web");
            //string pool = "FoxSecWebApplicationPool";
            //using (ServerManager mgr = new ServerManager())
            //{
            //    if (mgr.Sites[name] != null)
            //        mgr.Sites.Remove(mgr.Sites[name]);

            //    // Create site
            //    Site site = mgr.Sites.Add(name, "http", "*:80:", physicalPath);

            //    // Set app pool
            //    foreach (var app in site.Applications)
            //        app.ApplicationPoolName = pool;

            //    // Add extra bindings
            //    foreach (string hostname in extraBindings)
            //        site.Bindings.Add("*:80:" + hostname, "http");

            //    mgr.CommitChanges();
            //}

            try
            {
                string HTTPSInstallation = "1";
                string projectspath = Path.Combine(Install.projectsPath + @"\Web");
                string name = "FoxSecWeb";
                string directory = Path.Combine(Install.projectsPath + @"\Web");
                string pfxpath = directory + @"\" + "MyFoxSecCert0996" + ".pfx";
                string password = "foxsecsslcert@123";
                using (ServerManager serverManager = new ServerManager())
                {
                    try
                    {
                        Site s1 = serverManager.Sites["FoxSecWeb"];
                        serverManager.Sites.Remove(s1);
                        serverManager.CommitChanges();
                    }
                    catch
                    {

                    }
                    Site mySite = serverManager.Sites.Add(name, projectspath, 9191);
                    mySite.ServerAutoStart = true;

                    serverManager.CommitChanges();
                    Site site = serverManager.Sites[name];
                    site.Name = name;

                    site.Applications[0].VirtualDirectories[0].PhysicalPath = projectspath;

                    if (serverManager.ApplicationPools["FoxSecWebApplicationPool"] != null)
                        serverManager.ApplicationPools.Remove(serverManager.ApplicationPools["FoxSecWebApplicationPool"]);

                    serverManager.ApplicationPools.Add("FoxSecWebApplicationPool");
                    serverManager.Sites[name].Applications[0].ApplicationPoolName = "FoxSecWebApplicationPool";
                    ApplicationPool apppool = serverManager.ApplicationPools["FoxSecWebApplicationPool"];
                    apppool.ManagedRuntimeVersion = "v4.0";
                    apppool.ManagedPipelineMode = ManagedPipelineMode.Integrated;
                    if (HTTPSInstallation == "1")
                    {
                        try
                        {
                            // Create a collection object and populate it using the PFX file
                            X509Certificate2Collection collection = new X509Certificate2Collection();
                            collection.Import(pfxpath, password, X509KeyStorageFlags.MachineKeySet);

                            foreach (X509Certificate2 cert in collection)
                            {
                                if (cert.Subject.Equals("CN=TestServer, O=Test"))
                                {
                                    X509Store store1 = new X509Store(StoreName.Root, StoreLocation.LocalMachine);
                                    store1.Open(OpenFlags.ReadWrite);
                                    store1.Add(cert);
                                    store1.Close();
                                }

                                if (cert.Subject.Equals("CN=TestClient, OU=Applications, O=Test"))
                                {
                                    X509Store store1 = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                                    store1.Open(OpenFlags.ReadWrite);
                                    store1.Add(cert);
                                    store1.Close();
                                }
                            }
                            //string hostName = Dns.GetHostName();
                            //string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
                            //int port = CheckAvailableServerPort();
                            //var site1 = serverManager.Sites[name];
                            //site1.Bindings.Add("" + myIP + ":" + port + ":", collection[0].GetCertHash(), "MY");
                            //serverManager.CommitChanges();

                            //var site2 = serverManager.Sites[name];
                            //for (int i = 0; i < site2.Bindings.Count; i++)
                            //{
                            //    if (site2.Bindings[i].Protocol == "http")
                            //    {
                            //        site2.Bindings.RemoveAt(i);
                            //        break;
                            //    }
                            //}
                            //site2.ServerAutoStart = true;
                            //serverManager.CommitChanges();
                        }
                        catch (Exception ex)
                        {

                        }

                    }
                    // FileWriter.UpdateEventsLogFile(msg);
                }


                string msg = "IIS installed";
                // FileWriter.UpdateEventsLogFile(msg);
                Finish.lgiis = msg;

            }
            catch (Exception ex)
            {

            }
        }

        static string PrintHash(byte[] cert)
        {
            StringBuilder builder = new StringBuilder();

            foreach (byte b in cert)
            {
                builder.AppendFormat("{0:x2}", b);
            }

            return builder.ToString();
        }

        static void PrintCerts(X509Store store)
        {
            store.Open(OpenFlags.OpenExistingOnly);
            foreach (var cert in store.Certificates)
            {
                Console.Write("{0} - {1}", cert.FriendlyName, PrintHash(cert.GetCertHash()));
                Console.WriteLine();
            }
        }

        public void RegisterCertificateWithIIS6(string webSiteName, string certificateFilePath, string certificatePassword)
        {
            // USE WMI TO DERIVE THE INSTANCE NAME
            ManagementScope managementScope = new ManagementScope(@"\\.\root\MicrosoftIISv2");
            managementScope.Connect();
            ObjectQuery queryObject = new ObjectQuery("SELECT Name FROM IISWebServerSetting WHERE ServerComment = '" + webSiteName + "'");
            ManagementObjectSearcher searchObject = new ManagementObjectSearcher(managementScope, queryObject);
            var instanceNameCollection = searchObject.Get();
            var instanceName = (from i in instanceNameCollection.Cast<ManagementObject>() select i).FirstOrDefault();

            // USE IIS CERT OBJ TO IMPORT CERT - THIS IS A COM OBJECT
            //var IISCertObj = new CERTENROLLLib.IISCertObjClass();
            //IISCertObj.InstanceName = instanceName["Name"].ToString();
            //IISCertObj.Import(certificateFilePath, certificatePassword, false, true); // OVERWRITE EXISTING
        }


        //methods are used to enable IIS features
        #region
        public void cmdiis()
        {
            string command = @"start /w pkgmgr /iu:IIS-WebServerRole;IIS-WebServer;IIS-CommonHttpFeatures;IIS-StaticContent;IIS-DefaultDocument;IIS-DirectoryBrowsing;IIS-HttpErrors;IIS-ApplicationDevelopment;IIS-ISAPIExtensions;IIS-ISAPIFilter;IIS-NetFxExtensibility45;IIS-ASPNET45;IIS-NetFxExtensibility;IIS-ASPNET;IIS-HealthAndDiagnostics;IIS-HttpLogging;IIS-RequestMonitor;IIS-Security;IIS-RequestFiltering;IIS-HttpCompressionStatic;IIS-WebServerManagementTools;IIS-ManagementConsole;WAS-WindowsActivationService;WAS-ProcessModel;WAS-NetFxEnvironment;WAS-ConfigurationAPI";
            ProcessStartInfo pStartInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            Process p = new Process();
            p.StartInfo = pStartInfo;
            p.Start();
        }

        static string SetupIIS()
        {
            // In command prompt run this command to see all the features names which are equivalent to UI features.
            // c:\>dism /online /get-features /format:table 
            var featureNames = new List<string>
                {
                    "IIS-ApplicationDevelopment",
                    "IIS-ISAPIExtensions",
                    "IIS-ISAPIFilter",
                    "IIS-CommonHttpFeatures",
                    "IIS-DefaultDocument",
                    "IIS-HttpErrors",
                    "IIS-StaticContent",
                    "IIS-HealthAndDiagnostics",
                    "IIS-HttpLogging",
                    "IIS-HttpTracing",
                    "IIS-WebServer",
                    "IIS-WebServerRole",
                    "IIS-ManagementConsole",
                };

            Console.WriteLine("Checking the Operating System...\n");

            ManagementObjectSearcher obj = new ManagementObjectSearcher("select * from Win32_OperatingSystem");
            try
            {
                foreach (ManagementObject wmi in obj.Get())
                {
                    string Name = wmi.GetPropertyValue("Caption").ToString();

                    // Remove all non-alphanumeric characters so that only letters, numbers, and spaces are left.
                    // Imp. for 32 bit window server 2008
                    Name = Regex.Replace(Name.ToString(), "[^A-Za-z0-9 ]", "");

                    if (Name.Contains("Server 2012 R2") || Name.Contains("Windows 81"))
                    {
                        featureNames.Add("IIS-ASPNET45");
                        featureNames.Add("IIS-NetFxExtensibility45");
                    }
                    else if (Name.Contains("Server 2008 R2") || Name.Contains("Windows 7"))
                    {
                        featureNames.Add("IIS-ASPNET");
                        featureNames.Add("IIS-NetFxExtensibility");
                    }
                    else
                    {
                        //featureNames.Clear();
                        featureNames.Add("IIS-ASPNET");
                        featureNames.Add("IIS-NetFxExtensibility");
                    }

                    string Version = (string)wmi["Version"];
                    string Architecture = (string)wmi["OSArchitecture"];

                    Console.WriteLine("Operating System details:");
                    Console.WriteLine("OS Name: " + Name);
                    Console.WriteLine("Version: " + Version);
                    Console.WriteLine("Architecture: " + Architecture + "\n");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception occurred:" + ex.Message);
            }

            return Run(
                "dism",
                string.Format(
                    "/NoRestart /Online /Enable-Feature {0}",
                    string.Join(
                        " ",
                        featureNames.Select(name => string.Format("/FeatureName:{0}", name)))));
        }

        static string Run(string fileName, string arguments)
        {
            Console.WriteLine("Enabling IIS features...");
            Console.WriteLine(arguments);

            using (var process = Process.Start(new ProcessStartInfo
            {
                FileName = fileName,
                Arguments = arguments,
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                RedirectStandardOutput = true,
                UseShellExecute = false,
            }))
            {
                process.WaitForExit();
                return process.StandardOutput.ReadToEnd();
            }
        }
        #endregion

        /// <summary>
        /// Opens a new RegistryKey that represents the requested key on the local machine with the specified view.
        /// </summary>
        /// <param name="rootName">The HKEY to open.</param>
        /// <returns>The requested registry key.</returns>
        public static Func<string, Microsoft.Win32.RegistryKey> RegOpenSubKey = new Func<string, Microsoft.Win32.RegistryKey>((rootName) =>
        {
            Microsoft.Win32.RegistryKey localKey = null;
            Microsoft.Win32.RegistryHive hKey = Microsoft.Win32.RegistryHive.CurrentUser;
            Microsoft.Win32.RegistryView view = Microsoft.Win32.RegistryView.Default;

            if (rootName.Substring(Math.Max(0, rootName.Length - 2)) == "64")
            {
                if (Environment.Is64BitOperatingSystem)
                {
                    view = Microsoft.Win32.RegistryView.Registry64;
                }
                else
                {
                    MessageBox.Show("RegOpenSubKey: OS Arch mismatch.");
                }
            }

            switch (rootName.Replace("64", ""))
            {
                case "HKEY_CLASSES_ROOT":
                case "HKCR":
                    hKey = Microsoft.Win32.RegistryHive.ClassesRoot;
                    break;

                case "HKEY_CURRENT_CONFIG":
                case "HKCC":
                    hKey = Microsoft.Win32.RegistryHive.CurrentConfig;
                    break;

                case "HKEY_CURRENT_USER":
                case "HKCU":
                    hKey = Microsoft.Win32.RegistryHive.CurrentUser;
                    break;

                case "HKEY_LOCAL_MACHINE":
                case "HKLM":
                    hKey = Microsoft.Win32.RegistryHive.LocalMachine;
                    break;

                case "HKEY_USERS":
                case "HKU":
                    hKey = Microsoft.Win32.RegistryHive.Users;
                    break;

                default:
                    MessageBox.Show("Root = (Unknown)");
                    break;
            }

            localKey = Microsoft.Win32.RegistryKey.OpenBaseKey(hKey, view);
            return localKey;
        });

        /// <summary>
        /// Creates a new subkey or opens an existing subkey for write access.
        /// </summary>
        /// <param name="rootName">The HKEY to open.</param>
        /// <param name="keyName">The name or path of the subkey to create or open. This string is not case-sensitive.</param>
        /// <returns>Returns false if the operation failed.</returns>
        public static Func<string, string, bool> RegCreateKey = new Func<string, string, bool>((rootName, keyName) =>
        {
            try
            {

                Microsoft.Win32.RegistryKey localKey = RegOpenSubKey(rootName);
                // localKey = localKey.OpenSubKey(keyName, writable: true);

                using (localKey)
                {
                    if (localKey != null)
                    {
                        localKey.CreateSubKey(keyName);
                    }
                    else
                    {
                        // Abort("Key " + rootName + @"\" + keyName + " not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                // Abort(ex.ToString());
                return false;
            }

            return true;
        });


        /// <summary>
        /// Retrieves an array of strings that contains all the subkey names.
        /// </summary>
        /// <param name="rootName">The HKEY to open.</param>
        /// <param name="subKeyName">The name or path of the subkey to open. This string is not case-sensitive.</param>
        /// <returns>An array of strings that contains the names of the subkeys for the current key. This method does not recursively find names. It returns the names on the base level from which it was called.</returns>
        public static Func<string, string, string[]> RegGetSubKeyNames = new Func<string, string, string[]>((rootName, keyName) =>
        {
            try
            {
                string[] subKeys;
                bool LogDebug = false;

                Microsoft.Win32.RegistryKey localKey = RegOpenSubKey(rootName);
                localKey = localKey.OpenSubKey(keyName);
                if (LogDebug)
                {

                }

                using (localKey)
                {
                    if (localKey != null)
                    {
                        subKeys = localKey.GetSubKeyNames();
                        if (LogDebug)
                        {

                        }

                        for (int i = 0; i <= subKeys.Length - 1; i++)
                        {
                            subKeys[i] = keyName + @"\" + subKeys[i];
                        }
                        return subKeys;
                    }
                    else
                    {
                        if (LogDebug)
                        {

                        }
                        // Abort("Key " + rootName + @"\" + keyName + " not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return null;
        });

        /// <summary>
        /// Retrieves an array of strings that contains all the subkey names and any child subkeys names recursively.
        /// </summary>
        /// <param name="rootName">The HKEY to open.</param>
        /// <param name="subKeyName">The name or path of the subkey to open. This string is not case-sensitive.</param>
        /// <returns>An array of strings that contains the names of the subkeys and any child subkeys for the current key.</returns>
        Func<string, string, string[]> RegGetAllSubKeyNames = new Func<string, string, string[]>((rootName, keyName) =>
        {
            try
            {
                int index = 0;
                string[] subKeys = new string[index];
                string tempFileName = System.IO.Path.GetTempFileName();
                string previousKeyName = keyName;
                bool LogDebug = false;

                RegGetAllSubKeysStart:
                if (LogDebug)
                {
                    using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(tempFileName, true))
                    {
                        outfile.Write("\r\nRegGetAllSubKeyNames[" + index + "](" + rootName + ", " + keyName + ")\r\n");
                    }

                }

                string[] subkeynames;
                subkeynames = RegGetSubKeyNames(rootName, keyName);

                if (subkeynames == null || subkeynames.Length <= 0) //has no more subkey, process
                {
                    goto RegGetAllSubKeysForEach;
                }

                string subKeyName = keyName.Substring(previousKeyName.Length, keyName.Length - previousKeyName.Length);
                if (LogDebug)
                {

                }

                foreach (string key in subkeynames) //has subkeys, go deeper
                {
                    keyName = previousKeyName + subKeyName + @"\" + key;
                    Array.Resize(ref subKeys, subKeys.Length + 1);
                    subKeys[subKeys.Length - 1] = keyName;

                    if (LogDebug)
                    {
                        using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(tempFileName, true))
                        {
                            outfile.Write(subKeys[subKeys.Length - 1] + "\r\n");
                        }

                    }
                }

                RegGetAllSubKeysForEach:
                if (index < subKeys.Length)
                {
                    keyName = subKeys[index];
                    index += 1;
                    goto RegGetAllSubKeysStart;
                }

                if (LogDebug)
                {

                }
                return subKeys;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return null;
        });

        /// <summary>
        /// Retrieves an array of strings that contains all the value names associated with this key.
        /// </summary>
        /// <param name="rootName">The HKEY to open.</param>
        /// <param name="subKeyName">The name or path of the subkey to open. This string is not case-sensitive.</param>
        /// <returns>An array of strings that contains the value names for the current key. If no value names for the key are found, an empty array is returned. A registry key can have a default value — that is, a name/value pair in which the name is the empty string (""). If a default value has been set for a registry key, the array returned by the GetValueNames method includes the empty string.</returns>
        Func<string, string, string[]> RegGetValueNames = new Func<string, string, string[]>((rootName, keyName) =>
        {
            try
            {
                string[] valueNames;
                bool LogDebug = false;

                Microsoft.Win32.RegistryKey localKey = RegOpenSubKey(rootName);
                localKey = localKey.OpenSubKey(keyName);
                if (LogDebug)
                {

                }

                using (localKey)
                {
                    if (localKey != null)
                    {
                        valueNames = localKey.GetValueNames();
                        if (LogDebug)
                        {

                        }
                        return valueNames;
                    }
                    else
                    {
                        if (LogDebug)
                        {

                        }
                        // Abort("Key " + rootName + @"\" + keyName + " not found.");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            return null;
        });

        public static Func<string, string, string, object> RegGetValue = new Func<string, string, string, object>((rootName, keyName, valueName) =>
         {
             try
             {
                 Microsoft.Win32.RegistryKey localKey = RegOpenSubKey(rootName);
                 localKey = localKey.OpenSubKey(keyName);

                 using (localKey)
                 {
                     if (localKey != null)
                     {
                         if (localKey.GetValue(valueName) != null) // Value exists
                         {
                             switch (localKey.GetValueKind(valueName))
                             {
                                 case Microsoft.Win32.RegistryValueKind.String:
                                 case Microsoft.Win32.RegistryValueKind.ExpandString:
                                     Object o = localKey.GetValue(valueName);
                                     return (o as String);

                                 case Microsoft.Win32.RegistryValueKind.Binary:
                                     byte[] bytes = (byte[])localKey.GetValue(valueName);
                                     // result == "\x74\x00\x65\x00\x73\x00\x74\x00" == "t\0e\0s\0t\0"
                                     // Notice the Null \0 characters? When you display such a string in a textbox, only the part of the string until the first Null character is displayed.
                                     //                                  								Unicode
                                     return System.Text.Encoding.ASCII.GetString(bytes);
                                 case Microsoft.Win32.RegistryValueKind.DWord:
                                     return Convert.ToString((Int32)localKey.GetValue(valueName));

                                 case Microsoft.Win32.RegistryValueKind.QWord:
                                     return Convert.ToString((Int64)localKey.GetValue(valueName));

                                 case Microsoft.Win32.RegistryValueKind.MultiString:
                                     return (string[])localKey.GetValue(valueName);
                                 /*
                                 foreach (string s in (string[])valueName)
                                 {
                                     valueMultiString += ("[{s:s}], ");
                                 }
                                 */
                                 default:
                                     break;
                             }
                         }
                     }
                     else
                     {
                     }
                 }
             }
             catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
             {
             }

             return null;
         });

        public static Func<string, string, string, string> RegGetValueKind = new Func<string, string, string, string>((rootName, keyName, valueName) =>
        {
            try
            {
                Microsoft.Win32.RegistryKey localKey = RegOpenSubKey(rootName);
                localKey = localKey.OpenSubKey(keyName);

                using (localKey)
                {
                    if (localKey != null)
                    {
                        if (localKey.GetValue(valueName) != null) // Value exists
                        {
                            switch (localKey.GetValueKind(valueName))
                            {
                                case Microsoft.Win32.RegistryValueKind.String:
                                    return "REG_SZ";
                                case Microsoft.Win32.RegistryValueKind.ExpandString:
                                    return "REG_EXPAND_SZ";
                                case Microsoft.Win32.RegistryValueKind.Binary:
                                    return "REG_BINARY";
                                case Microsoft.Win32.RegistryValueKind.DWord:
                                    return "REG_DWORD";
                                case Microsoft.Win32.RegistryValueKind.QWord:
                                    return "REG_QWORD";
                                case Microsoft.Win32.RegistryValueKind.MultiString:
                                    return "REG_MULTI_SZ";
                                default:
                                    break;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // Ketarin.Forms.LogDialog.Log(ex.ToString());
            }

            return null;
        });

    }

}

