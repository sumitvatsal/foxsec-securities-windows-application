﻿using SetupWebSetup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.ServiceProcess;
using System.IO.Compression;
using Ionic.Zip;
using FoxSecWebSetup.Conf;
using FoxSecWebLicense;
using System.Diagnostics;
using System.Reflection;
using WindowsFormsApplication1;

namespace FoxSecWebSetup
{
    public partial class Install : Form
    {

        public static string ListLog;
        public static string projectsPath = @"C:\FoxSec";
        public string msg = "";
        public static DateTime saveNow = DateTime.Now;
        public Install()
        {
            InitializeComponent();
            string icon = "icon1.ico";
            this.Icon = new Icon(icon, 60, 60);
            this.Text = "FoxSecWeb Installer";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        public void button1_Click(object sender, EventArgs e)
        {

            FolderBrowserDialog folderBrowserDlg = new FolderBrowserDialog();
            // A new folder button will display in FolderBrowserDialog.
            folderBrowserDlg.ShowNewFolderButton = true;
            //Show FolderBrowserDialog
            DialogResult dlgResult = folderBrowserDlg.ShowDialog();
            projectsPath = folderBrowserDlg.SelectedPath + @"FoxSec";
            txtPath.Text = projectsPath + @"\Web\";
            if (dlgResult.Equals(DialogResult.OK))
            {
                if (DialogResult.OK == MessageBox.Show("You agree to install" + txtPath.Text)) { }
                Environment.SpecialFolder rootFolder = folderBrowserDlg.RootFolder;
            }
            if (dlgResult.Equals(DialogResult.Cancel))
            {
                txtPath.Text= @"C:\FoxSec\Web\";
            }


        }
        public static void DB()
        {
            string pathWeb = System.IO.Path.Combine(Install.projectsPath + "\\Web");
            ServiceController controller = null;
            try
            {
                ServiceController sc = new ServiceController("FoxSecWebService");
                if ((sc.Status.Equals(ServiceControllerStatus.StartPending)) ||
     (sc.Status.Equals(ServiceControllerStatus.Running)))
                {
                    sc.Stop();
                }

            }
            catch (InvalidOperationException)
            {

            }
            finally
            {
                if (controller != null)
                {
                    controller.Dispose();
                }
            }
            if (Welcome.Replase == null || !Directory.Exists(pathWeb))
            {
                try
                {
                    try
                    {

                        string appPath = AppDomain.CurrentDomain.BaseDirectory;
                        string zipPath = System.IO.Path.Combine(appPath + "Web.zip");

                        string extractPath = Install.projectsPath;

                        Searcher.ExtractToDirectory(System.IO.Compression.ZipFile.OpenRead(zipPath), extractPath, true);
                        //        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);
                        //  msg = "Copy Web success";
                        //  Finish.lgcopy = msg;
                        //  FileWriter.UpdateEventsLogFile("");

                        SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " Web.zip extracted successfully....");
                    }
                    catch (Exception ex)
                    {
                        //  msg = "Copy Web not success";
                        //  Finish.lgcopy1 = msg;
                        SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error extract Web.zip " + ex.Message);
                    }
                    try
                    {
                        string appPath = AppDomain.CurrentDomain.BaseDirectory;
                        //string zipPath1 = System.IO.Path.Combine(appPath + "FoxSecDb.zip");
                        string zipPath1 = System.IO.Path.Combine(appPath + "Sql\\FoxSecDb.zip");

                        //string extractPath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA";
                        //string deletePath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FoxSecDb";
                        string extractPath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin");
                        //       string deletePath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin\FoxSecDb");
                        //  Directory.Delete(deletePath1, true); //true - если директория не пуста (удалит и файлы и папки)
                        //  Directory.CreateDirectory(deletePath1);

                        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath1, extractPath1);
                        SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " FoxSecDb.zip extracted successfully....");
                    }
                    catch (Exception ex)
                    {
                        string msg = ex.ToString();
                        //   Finish.lgcheck1 = msg;
                        SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " " + msg + ex.Message);
                    }
                    try
                    {
                        string appPath = AppDomain.CurrentDomain.BaseDirectory;
                        string zipPath = System.IO.Path.Combine(appPath + "Portal.zip");

                        string extractPath = Install.projectsPath;

                        Searcher.ExtractToDirectory(System.IO.Compression.ZipFile.OpenRead(zipPath), extractPath, true);
                        SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " Portal.zip extracted successfully....");
                        //        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);
                        //  msg = "Copy Web success";
                        //  Finish.lgcopy = msg;
                        //  FileWriter.UpdateEventsLogFile("");
                    }
                    catch (Exception ex)
                    {
                        //  msg = "Copy Web not success";
                        //  Finish.lgcopy1 = msg;
                        SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error extract Portal.zip " + ex.Message);
                    }

                }

                catch (Exception ex)
                {
                    string msg = ex.ToString();
                    //   Finish.lgcheck1 = msg;
                    SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " " + msg + " " + ex.Message);
                }
            }

            if (Change.Replace != null || Directory.Exists(pathWeb))
            {
                try
                {

                    string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    string zipPath = System.IO.Path.Combine(appPath + "WebRepair.zip");

                    string extractPath = System.IO.Path.Combine(Install.projectsPath);

                    //   ZipFile.ExtractToDirectory(zipPath, extractPath);

                    //  FileWriter.UpdateEventsLogFile("");
                    using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath))
                    {
                        zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
                    }
                    //  msg = "Repair Web success";
                    //  Finish.lgcopy = msg;

                    SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " WebRepair.zip extracted successfully....");

                }
                catch (Exception ex)
                {
                    //  msg = "Repair Web not success";
                    //  Finish.lgcopy1 = msg;
                    SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error extract WebRepair.zip " + ex.Message);
                }
                try
                {
                    string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    string zipPath = System.IO.Path.Combine(appPath + "FoxSecService.zip");

                    string extractPath = System.IO.Path.Combine(Install.projectsPath + "\\Web");

                    //   ZipFile.ExtractToDirectory(zipPath, extractPath);

                    //  FileWriter.UpdateEventsLogFile("");
                    using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath))
                    {
                        zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
                    }
                    //  msg = "Repair Web success";
                    //  Finish.lgcopy = msg;
                    SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " FoxSecService.zip extracted successfully....");

                }
                catch (Exception ex)
                {
                    //  msg = "Repair Web not success";
                    //  Finish.lgcopy1 = msg;
                    SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error extract FoxSecService.zip " + ex.Message);
                }
                try
                {
                    string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    string zipPath = System.IO.Path.Combine(appPath + "Portal.zip");

                    string extractPath = Install.projectsPath;

                    using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath))
                    {
                        zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
                    }

                    string sourceFile = System.IO.Path.Combine(extractPath + "\\Portal\\Portaal");

                    if (Directory.Exists(sourceFile) == false)
                    {
                    }
                    else
                    {
                        string skipFileName = "";
                        var sourceDi = new DirectoryInfo(sourceFile);
                        var targetDi = new DirectoryInfo(System.IO.Path.Combine(extractPath + "\\Portaal"));
                        CopyAll(sourceDi, targetDi, skipFileName);

                        var sourceDi1 = new DirectoryInfo(System.IO.Path.Combine(extractPath + "\\Portal\\PortalService"));
                        var targetDi1 = new DirectoryInfo(System.IO.Path.Combine(extractPath + "\\Web\\PortalService"));
                        CopyAll(sourceDi1, targetDi1, skipFileName);

                        string sourceFiletxt = System.IO.Path.Combine(extractPath + "\\Portal");

                        string[] filePaths = Directory.GetFiles(System.IO.Path.Combine(extractPath + "\\Portal"));
                        foreach (var filename in filePaths)
                        {
                            string file = filename.ToString();

                            //Do your job with "file"

                            string str = System.IO.Path.Combine(extractPath + "\\Web") + file.Replace(sourceFiletxt, "");
                            if (!File.Exists(str))
                            {
                                File.Copy(file, str);
                            }
                        }
                    }
                    Directory.Delete(System.IO.Path.Combine(extractPath + "\\Portal"), true);
                    SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " Portal.zip extracted successfully....");
                }
                catch (Exception ex)
                {
                    //  msg = "Copy Web not success";
                    //  Finish.lgcopy1 = msg;
                    SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error extract Portal.zip " + ex.Message);
                }

                try
                {
                    string appPath = AppDomain.CurrentDomain.BaseDirectory;
                    string zipPath = System.IO.Path.Combine(appPath + "FoxSecConf.zip");
                    string extractPath = System.IO.Path.Combine(Install.projectsPath);

                    using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath))
                    {
                        zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
                    }

                    SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " FoxSecConf.zip extracted successfully....");
                    if (Program.Repair == false)
                    {
                        try
                        {
                            var currentpath = System.IO.Path.Combine(Install.projectsPath + @"\FoxSecConf\SecConf.ini");
                            if (File.Exists(currentpath))
                            {
                                string[] lines = FileReader.ReadLines(currentpath);
                                using (StreamWriter stream = new StreamWriter(currentpath, false))
                                {
                                    for (int i = 0; i < lines.Length; i++)
                                    {
                                        string line = lines[i];

                                        if (line.Contains("Last project="))
                                        {
                                            stream.WriteLine("Last project=" + Welcome.eprFilePath);
                                        }
                                        else
                                        {
                                            stream.WriteLine(line);
                                        }

                                    }
                                    stream.Close();
                                    stream.Dispose();
                                }
                            }
                            SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " .epr file copied successfully....");
                        }
                        catch
                        {

                        }
                    }

                    CreateShortcut("FoxSecConf", Environment.GetFolderPath(Environment.SpecialFolder.Desktop), Assembly.GetExecutingAssembly().Location);

                }
                catch (Exception ex)
                {
                    SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error while extracting FoxSecConf " + ex.Message);
                }
            }
        }

        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation)
        {
            try
            {
                string directory = Path.Combine(Install.projectsPath + @"\FoxSecConf\FoxSecConf.exe");
                string shortcutLocation = System.IO.Path.Combine(shortcutPath, shortcutName + ".lnk");
                IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
                IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

                shortcut.Description = "FoxSecConf";   // The description of the shortcut
                shortcut.TargetPath = directory;                 // The path of the file that will launch when the shortcut is run
                shortcut.Save();

                using (var fs = new FileStream(shortcutLocation, FileMode.Open, FileAccess.ReadWrite))
                {
                    fs.Seek(21, SeekOrigin.Begin);
                    fs.WriteByte(0x22);
                }
                SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " FoxSecConf.exe shortcut successfully....");
            }
            catch (Exception ex)
            {
                SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + " Error occurred while creating shortcut for FoxSecConf.exe " + ex.Message);
            }
        }
        public static void CopyAll(DirectoryInfo source, DirectoryInfo target, string skipFileName)
        {
            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it’s new directory, except files to skip.
            foreach (var fileInfo in source.GetFiles())
            {
                if (fileInfo.Name != skipFileName)
                {
                    Debug.Print(@"Copying {0}\{1}", target.FullName, fileInfo.Name);
                    //using (var tw = new StreamWriter(Path.Combine(target.ToString(), fileInfo.Name), true))
                    //{
                    fileInfo.CopyTo(Path.Combine(target.ToString(), fileInfo.Name), true);
                    //}                       
                }
            }

            // Copy each subdirectory using recursion.
            foreach (var subDir in source.GetDirectories())
            {
                if (subDir.Name != target.Name)
                {
                    var nextTargetSubDir = target.CreateSubdirectory(subDir.Name);
                    CopyAll(subDir, nextTargetSubDir, skipFileName);
                }
            }
        }

        //   public static void Online()
        //   {
        //       string pathWeb = System.IO.Path.Combine(Install.projectsPath + "\\Web");
        //       ServiceController controller = null;
        //       try
        //       {
        //           ServiceController sc = new ServiceController("FoxSecWebOnline);
        //           if ((sc.Status.Equals(ServiceControllerStatus.StartPending)) ||
        //(sc.Status.Equals(ServiceControllerStatus.Running)))
        //           {
        //               sc.Stop();
        //           }

        //       }
        //       catch (InvalidOperationException)
        //       {

        //       }
        //       finally
        //       {
        //           if (controller != null)
        //           {
        //               controller.Dispose();
        //           }
        //       }
        //       if (Welcome.Replase == null || !Directory.Exists(pathWeb))
        //       {
        //           try
        //           {
        //               try
        //               {

        //                   string appPath = AppDomain.CurrentDomain.BaseDirectory;
        //                   string zipPath = System.IO.Path.Combine(appPath + "Portal.zip");

        //                   string extractPath = Install.projectsPath;

        //                   Searcher.ExtractToDirectory(System.IO.Compression.ZipFile.OpenRead(zipPath), extractPath, true);
        //                   //        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath, extractPath);
        //                   //  msg = "Copy Web success";
        //                   //  Finish.lgcopy = msg;
        //                   //  FileWriter.UpdateEventsLogFile("");
        //               }
        //               catch (Exception ex)
        //               {
        //                   //  msg = "Copy Web not success";
        //                   //  Finish.lgcopy1 = msg;
        //                   SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + "Error extract Web.zip" + ex.Message);
        //               }
        //               //    try
        //               //    {
        //               //        string appPath = AppDomain.CurrentDomain.BaseDirectory;
        //               //        string zipPath1 = System.IO.Path.Combine(appPath + "FoxSecDb.zip");

        //               //        //string extractPath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA";
        //               //        //string deletePath1 = @"C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FoxSecDb";
        //               //        string extractPath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin");
        //               //        //       string deletePath1 = System.IO.Path.Combine(Install.projectsPath + @"\Web\bin\FoxSecDb");
        //               //        //  Directory.Delete(deletePath1, true); //true - если директория не пуста (удалит и файлы и папки)
        //               //        //  Directory.CreateDirectory(deletePath1);

        //               //        System.IO.Compression.ZipFile.ExtractToDirectory(zipPath1, extractPath1);

        //               //    }
        //               //    catch (Exception ex)
        //               //    {
        //               //        string msg = ex.ToString();
        //               //        //   Finish.lgcheck1 = msg;
        //               //        SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + msg + ex.Message);
        //               //    }
        //               //}

        //               catch (Exception ex)
        //               {
        //                   string msg = ex.ToString();
        //                   //   Finish.lgcheck1 = msg;
        //                   SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + msg + ex.Message);
        //               }
        //           }

        //       if (Change.Replace != null || Directory.Exists(pathWeb))
        //           {
        //               try
        //               {

        //                   string appPath = AppDomain.CurrentDomain.BaseDirectory;
        //                   string zipPath = System.IO.Path.Combine(appPath + "OnlineRepair.zip");

        //                   string extractPath = System.IO.Path.Combine(Install.projectsPath);

        //                   //   ZipFile.ExtractToDirectory(zipPath, extractPath);

        //                   //  FileWriter.UpdateEventsLogFile("");
        //                   using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath))
        //                   {
        //                       zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
        //                   }
        //                   //  msg = "Repair Web success";
        //                   //  Finish.lgcopy = msg;


        //               }
        //               catch (Exception ex)
        //               {
        //                   //  msg = "Repair Web not success";
        //                   //  Finish.lgcopy1 = msg;
        //                   SetupWebSetup.FileWriter.UpdateEventsLogFile(saveNow + "Error extract OnlineRepair.zip" + ex.Message);
        //               }

        //           }
        //       }
        //   }
        private void button2_Click(object sender, EventArgs e)
        {
            var driven = txtPath.Text;
            driven = driven.Split(':')[0] + ":";
            if (!Directory.Exists(driven))
            {
                return;
            }
            button2.Enabled = false;
            //if repear to Finish
            //if (Change.Replace != null)
            //{
            //    DB();
            //    this.Hide();
            //    Finish form5 = new Finish();
            //    form5.Show();

            //}

            //DB();
            // Online();
            //projectsPath = txtPath.Text;
            //this.Hide();

            //Sql form2 = new Sql();
            //form2.Show();

            try
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                string zipPath = System.IO.Path.Combine(appPath + "SQLTEST.zip");

                string extractPath = Install.projectsPath;

                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath))
                {
                    zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
                }
            }
            catch (Exception ex)
            {
                SetupWebSetup.FileWriter.UpdateEventsLogFile(DateTime.Now + " Error extract Portal.zip " + ex.Message);
            }
            try
            {

                ProcessStartInfo procInfo = new ProcessStartInfo();

                procInfo.UseShellExecute = true;

                procInfo.FileName = @"install.bat";  //The file in that DIR.

                //procInfo.WorkingDirectory = @"C:\SQLTEST\"; //The working DIR.
                procInfo.WorkingDirectory = Path.Combine(Install.projectsPath + @"\SQLTEST\");

                procInfo.Verb = "runas";

                Process.Start(procInfo);  //Start that process.
                SetupWebSetup.FileWriter.UpdateInstallationLogFile(DateTime.Now + " install.bat copied successfully....");
            }

            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message.ToString());
                SetupWebSetup.FileWriter.UpdateEventsLogFile(DateTime.Now + " install.bat file: " + ex.Message);
            }

            if (Program.Repair == true)
            {
                if (Change.LicDriveType == "1")
                {
                    string basefilepath = Change.licenseFilePath;
                    string strpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                    if (basefilepath.ToLower() == strpath.ToLower()) { }
                    else
                    {
                        if (!File.Exists(strpath))
                        {
                            System.IO.Directory.CreateDirectory(Install.projectsPath + @"\Web");
                            try
                            {
                                File.Copy(basefilepath, strpath);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        else
                        {
                            try
                            {
                                File.Delete(strpath);
                            }
                            catch
                            {

                            }
                            try
                            {
                                File.Copy(basefilepath, strpath);
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        Change.licenseFilePath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                    }
                }
            }
            else
            {
                if (Welcome.LicDriveType == "1")
                {
                    string basefilepath = Welcome.licenseFilePath;
                    string strpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                    //string strpath = System.IO.Path.Combine(licenseFilePath + "\\Web") + "FoxSecLicense.ini".Replace(basefilepath, "");
                    if (!File.Exists(strpath))
                    {
                        System.IO.Directory.CreateDirectory(Install.projectsPath + @"\Web");
                        try
                        {
                            File.Copy(basefilepath, strpath);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        try
                        {
                            File.Delete(strpath);
                        }
                        catch
                        {

                        }
                        try
                        {
                            File.Copy(basefilepath, strpath);
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    Welcome.licenseFilePath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                }
            }

            this.Hide();
            Check.IISstart();
            Check.GetWinName();
            Install.DB();
            Sql form2 = new Sql();
            form2.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string serviceName = "FoxSecWebService";
            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
            if (service != null)
            {
                this.Hide();
                Change form2 = new Change();
                form2.Show();
            }
            else
            {
                this.Hide();
                Welcome form1 = new Welcome();
                form1.Show();
            }


        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not yet complete. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
