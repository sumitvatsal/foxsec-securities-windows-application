﻿using SetupWebSetup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoxSecWebSetup
{
    public partial class Iis : Form
    {
        public Iis()
        {
            InitializeComponent();
            this.button1.Enabled = false;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Check.IISstart();
            Check.GetWinName();
            this.button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Install form1 = new Install();
            form1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Welcome form1 = new Welcome();
        }
    }
}
