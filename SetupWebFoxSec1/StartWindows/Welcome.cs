﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using FoxSecWebSetup;
using Microsoft.Win32;
using System.ServiceProcess;
using System.Configuration.Install;
using System.IO;
using FoxSecWebSetup.License;
using FoxSecWebLicense;
using System.Text.RegularExpressions;
using System.Management;
using System.Configuration;
using System.Security.Cryptography;

namespace SetupWebSetup
{
    public partial class Welcome : Form
    {
        public static string Replase;
        private static bool appIsLocked = true;
        public static string deviceID = string.Empty;
        private const RegexOptions regexOptions =
        RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture;
        private const string dateRegexp = "(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])";
        private static readonly Regex dateRegex =
        new Regex(dateRegexp, regexOptions);
        private const string digitsRegexPattern = @"^[0-9]+$";
        private static readonly Regex digitsRegex = new Regex(digitsRegexPattern, regexOptions);
        private static readonly string CR = Environment.NewLine;
        public static string serialnember = string.Empty;
        public static string licenseFilePath = "";
        private static string appPath = string.Empty;
        private static string error = string.Empty;
        public const string ENCRYPTION_KEY = "A456E4DA104F960563A66DDC";
        public static string eprFilePath = "";
        public static string LicDriveType = "";
        public Welcome()
        {
            InitializeComponent();
            string icon = "icon1.ico";
            this.Icon = new Icon(icon, 60, 60);
            this.Text = "FoxSecWeb Installer";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            try
            {
                foreach (Process Proc in Process.GetProcesses())
                {
                    if (Proc.ProcessName.Equals("msiexec"))
                    {
                        try
                        {
                            Proc.Kill();
                        }
                        catch (Exception ex)
                        {
                            SetupWebSetup.FileWriter.UpdateInstallationErrorLogFile(DateTime.Now + " Error occurred while trying to kill msi: " + ex.Message + "\r\n" + ex.StackTrace + "\r\n" + "------------------------------------------------------------------------------");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SetupWebSetup.FileWriter.UpdateInstallationErrorLogFile(DateTime.Now + " Error occurred while trying to kill msi: " + ex.Message + "\r\n" + ex.StackTrace + "\r\n" + "------------------------------------------------------------------------------");
            }        
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                string location = IsApplictionInstalled("FoxsecSetup.msi");
                string[] filePaths = Directory.GetFiles(location, "*.epr", SearchOption.TopDirectoryOnly);
                if (filePaths.Length == 0)
                {
                    MessageBox.Show("Project file does not exist!!");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Project file does not exist!!");
                return;
            }
            Replase = "No";

            GetSerFlashDisk();
            if (licenseFilePath == "")
            {
                GetSerHardDisk();
            }

            RecognizeAppState();
            if (!String.IsNullOrEmpty(licenseFilePath))
            {
                int returnlicflash = Spec.CheckLicenseFlash(deviceID);

                switch (returnlicflash)
                {
                    case '1':
                    case 2:
                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        break;
                    case 0:
                        break;
                }

                if (returnlicflash == 1)
                {
                    this.Hide();
                    Activation form1 = new Activation();
                    form1.Show();
                }
                else
                {
                    this.Hide();
                    NotFound form2 = new NotFound();
                    form2.Show();
                }
            }
            else
            {
                this.Hide();
                NotFound form2 = new NotFound();
                form2.Show();
            }
        }

        public void GetSerFlashDisk()
        {
            LicDriveType = "";
            string diskName = string.Empty;
            string testser = string.Empty;
            var numint = string.Empty;
            StringBuilder volumename = new StringBuilder(256);
            try
            {
                foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
                {
                    foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
                    {

                        foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
                        {

                            diskName = disk["Name"].ToString().Trim();
                            testser = disk["VolumeSerialNumber"].ToString().Trim();
                            numint = Convert.ToInt64(testser, 16).ToString();
                        }

                        licenseFilePath = Path.Combine(diskName + "//FoxSecLicense.ini");

                        try
                        {
                            File.Copy("FoxSecLicense.ini", licenseFilePath);
                        }
                        catch
                        {
                        }
                        if (File.Exists(licenseFilePath))
                        {
                            if (partition != null)
                            {
                                // associate partitions with logical disks (drive letter volumes)
                                ManagementObject logical = new ManagementObjectSearcher(String.Format(
                                    "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
                                    partition["DeviceID"])).First();

                                if (logical != null)
                                {
                                    List<string> list = new List<string>();

                                    ManagementObject volume = new ManagementObjectSearcher(String.Format(
                                        "select FreeSpace, Size from Win32_LogicalDisk where Name='{0}'",
                                        logical["Name"])).First();

                                    UsbDisk disk = new UsbDisk(logical["Name"].ToString());
                                    DriveInfo[] allDrives = DriveInfo.GetDrives();
                                    foreach (DriveInfo d in allDrives)
                                    {
                                        string namedisk = diskName + @"\";
                                        string name = d.Name;
                                        if (namedisk == name)
                                        {
                                            if (d.IsReady == true)
                                            {
                                                d.VolumeLabel = "FoxSec";
                                            }
                                        }
                                    }
                                    string pnpdeviceid = parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim());
                                    var conpnp = pnpdeviceid.Substring(0, 5);

                                    var conpnpn = converttoascii(conpnp);
                                    var pnpdevidint = Convert.ToUInt64(conpnpn, 16).ToString();

                                    list.Add(pnpdevidint.Substring(0, 4));

                                    disk.Size = (ulong)volume["Size"];
                                    string size = disk.Size.ToString();
                                    size = volume["Size"].ToString();
                                    list.Add(size.Substring(0, 4));

                                    list.Add(numint.Substring(0, 7));

                                    string str = "f";
                                    string flashser = Encrypt(str, true);
                                    list.Add(flashser);

                                    StringBuilder builder = new StringBuilder();
                                    foreach (string cat in list)
                                    {
                                        builder.Append(cat).Append("");
                                    }
                                    string result = builder.ToString();
                                    deviceID = result;
                                }
                            }
                            try
                            {
                                string[] filePaths = Directory.GetFiles(diskName + "\\", "*.epr", SearchOption.TopDirectoryOnly);
                                if (filePaths.Length > 0)
                                {
                                    string[] arr = filePaths[0].Split('\\');
                                    string filename = arr[arr.Length - 1];
                                    string strfpath = System.IO.Path.Combine(Install.projectsPath + @"\FoxSecConf\" + filename);
                                    eprFilePath = strfpath;
                                    if (!File.Exists(strfpath))
                                    {
                                        System.IO.Directory.CreateDirectory(Install.projectsPath + @"\FoxSecConf");
                                        File.Copy(filePaths[0], strfpath);
                                    }
                                    else
                                    {
                                        File.Delete(strfpath);
                                        File.Copy(filePaths[0], strfpath);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                            string compuniqnumber = Spec.ReadCompNr();
                            if (deviceID == compuniqnumber)
                            {
                                return;
                            }
                            else
                            {
                                deviceID = string.Empty;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        string converttoascii(string text)
        {
            string asciitxt = "";
            for (var i = 0; i < text.Length; i++)
            {
                char current = text[i];
                if (!(Char.IsDigit(current)))
                {
                    asciitxt = asciitxt + Convert.ToString((int)(current));
                }
                else
                {
                    asciitxt = asciitxt + current;
                }
            }
            return asciitxt;
        }
        protected static void RecognizeAppState()
        {
            try
            {
                string rTime = "";
                if (Registry.GetValue("HKEY_LOCAL_MACHINE\\System\\Inith", "1", null) == null)
                {
                    rTime = null;
                }
                else
                {
                    rTime = Registry.GetValue("HKEY_LOCAL_MACHINE\\System\\Inith", "1", null).ToString();
                }
                //Try
                //    rTime = Registry.GetValue("HKEY_LOCAL_MACHINE\System\Inith", "1", Nothing).ToString
                //Catch ex As NullReferenceException
                //    rTime = Nothing
                //End Try
                //kontrollime kas aeg on õiges formaadis(pikkus vähemalt
                //10, sisaldab ainult numbrid) ja paneme õigesse järjekorda DDHHYYMiMiMoMo -> YYMoMoDDHHMiMi
                if (!String.IsNullOrEmpty(rTime))
                {
                    if (rTime.Length > 9)
                    {
                        if (Digits(rTime))
                        {
                            if (ValidateDate(String.Concat(
                                "20" + rTime.Substring(4, 2), "-",
                                rTime.Substring(8, 2), "-",
                                rTime.Substring(0, 2))))
                            {
                                rTime =
                                   "20" + rTime.Substring(4, 2) +
                                   rTime.Substring(8, 2) +
                                   rTime.Substring(0, 2) +
                                   rTime.Substring(2, 2) +
                                   rTime.Substring(6, 2);
                                //yy = ToInt32(rTime.Substring(4, 2))
                                //mm = ToInt32(rTime.Substring(8, 2))
                                //dd = ToInt32(rTime.Substring(0, 2))
                                //mi = ToInt32(rTime.Substring(6, 2))
                                //hh = ToInt32(rTime.Substring(2, 2))
                            }
                            else
                                return;
                        }
                    }
                    else
                    {
                        return;
                    }

                    DateTime currentTime = DateTime.Now;
                    DateTime rTimeDate = DateTime.ParseExact(rTime, "yyyyMMddHHmm", null);
                    TimeSpan span = currentTime.Subtract(rTimeDate);

                    if (span.TotalHours > 2)
                        appIsLocked = false;
                    else
                        appIsLocked = true;
                }
                else
                {
                    //siia tuleb kontroll , kas registris on kirje et oli lukustatud.
                    //kui selline kirje on ja rTime=Nothing siis on võimalik et registrist kustutati rTime
                    appIsLocked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        internal static bool ValidateDate(string yyyyMMdd)
        {
            return dateRegex.IsMatch(yyyyMMdd);
        }
        internal static bool Digits(params string[] values)
        {
            bool valid = ValidateString(digitsRegex, values);
            return valid;
        }
        internal static bool ValidateString(Regex r, params string[] values)
        {
            if (r == null)
                return false;

            bool valid = true;
            foreach (string s in values)
            {
                valid &= r.IsMatch(s);
                if (!valid)
                    return false;
            }
            return valid;
        }

        internal const string LICENSE_SERNUM =
        "[Users]\r\nCompUniqNr={0}\r\n";
        private static object l = new object();
        internal static void WriteSerialNumber(string sernum)
        {

            string backup = licenseFilePath + ".copy";
            if (File.Exists(backup))
                File.Delete(backup);
            if (File.Exists(licenseFilePath))
                File.Move(licenseFilePath, backup);
            string data = String.Format(LICENSE_SERNUM, serialnember);
            WriteToFile(licenseFilePath, data);
        }
        internal static void WriteToFile(string path, string arg)
        {
            bool append = false;
            WriteToFile(path, arg, append);
        }
        internal static void WriteToFile(string path, string arg, bool append)
        {
            lock (l)
            {
                StreamWriter sw = new StreamWriter(path, append);
                sw.WriteLine(arg);
                sw.Close();
            }
        }
        public string displayMembers(List<String> vegetables)
        {
            foreach (String s in vegetables)
            {
                return s.ToString();
            }
            return null;
        }
        private string parseSerialFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string[] serialArray; string serial;
            int arrayLen = splitDeviceId.Length - 1;
            serialArray = splitDeviceId[arrayLen].Split('&');
            serial = serialArray[0]; return serial;
        }
        private string parseVenFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string Ven;
            //Разбиваем строку на несколько частей.    
            //Каждая чаcть отделяется по символу &  
            string[] splitVen = splitDeviceId[1].Split('&');
            Ven = splitVen[1].Replace("VEN_", "");
            Ven = Ven.Replace("_", " "); return Ven;
        }
        private string parseProdFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string Prod;
            //Разбиваем строку на несколько частей.     //Каждая чаcть отделяется по символу & 
            string[] splitProd = splitDeviceId[1].Split('&');
            Prod = splitProd[2].Replace("PROD_", ""); ;
            Prod = Prod.Replace("_", " "); return Prod;
        }
        private string parseRevFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string Rev;     //Разбиваем строку на несколько частей.     //Каждая чаcть отделяется по символу &   
            string[] splitRev = splitDeviceId[1].Split('&');
            Rev = splitRev[3].Replace("REV_", ""); ;
            Rev = Rev.Replace("_", " ");
            return Rev;
        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not completed yet. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        public string identifier(string wmiClass, string wmiProperty)
        //Return a hardware identifier
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }
        public void GetSerHardDisk()
        {
            try
            {
                string location = IsApplictionInstalled("FoxsecSetup.msi");
                string basefilepath = location + "FoxSecLicense.ini";
                if (!File.Exists(basefilepath))
                {
                }
                else
                {
                    //string strpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                    ////string strpath = System.IO.Path.Combine(licenseFilePath + "\\Web") + "FoxSecLicense.ini".Replace(basefilepath, "");
                    //if (!File.Exists(strpath))
                    //{
                    //    System.IO.Directory.CreateDirectory(Install.projectsPath + @"\Web");                       
                    //    try
                    //    {
                    //        File.Copy(basefilepath, strpath);
                    //    }
                    //    catch (Exception ex)
                    //    {

                    //    }
                    //}
                    //else
                    //{
                    //    try
                    //    {
                    //        File.Delete(strpath);
                    //    }
                    //    catch
                    //    {

                    //    }
                    //    try
                    //    {
                    //        File.Copy(basefilepath, strpath);
                    //    }
                    //    catch (Exception ex)
                    //    {

                    //    }
                    //}

                    //licenseFilePath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                    LicDriveType = "1";
                    licenseFilePath = basefilepath;
                    string serial = "";
                    List<string> list = new List<string>();
                    string model = "";
                    ManagementObjectSearcher moSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                    long totalSize = 0;
                    foreach (ManagementObject wmi_HD in moSearcher.Get())
                    {
                        if (wmi_HD.Properties["InterfaceType"].Value.ToString() != "USB")
                        {
                            model = wmi_HD["Model"].ToString();  //Model Number
                            try
                            {
                                serial = wmi_HD.GetPropertyValue("SerialNumber").ToString();
                            }
                            catch
                            {
                                serial = identifier("Win32_NetworkAdapterConfiguration", "MacAddress");
                            }
                            totalSize += Convert.ToInt64(wmi_HD.Properties["Size"].Value.ToString());
                        }
                    }

                    byte[] ba = System.Text.Encoding.ASCII.GetBytes(model);
                    string ba0 = ba[0].ToString();
                    string ba1 = ba[1].ToString();
                    string ba2 = ba[2].ToString();

                    long intba0 = Convert.ToInt64(ba0) % 10;
                    long intba1 = Convert.ToInt64(ba1) % 10;
                    long intba2 = Convert.ToInt64(ba2) % 10;
                    string intstrba0 = intba0.ToString();
                    string intstrba1 = intba1.ToString();
                    string intstrba2 = intba2.ToString();

                    list.Add(intstrba0);
                    list.Add(intstrba1);
                    list.Add(intstrba2);

                    string name = identifier("Win32_LogicalDisk", "Name");

                    //string Size = identifier("Win32_DiskDrive", "Size");
                    string Size = Convert.ToString(totalSize);
                    list.Add(Size.Substring(0, 5)); //Jelena Ver67          

                    // string serial = identifier("Win32_DiskDrive", "SerialNumber");
                    String numint = serial.Substring(0, 6); //Jelena Ver67

                    byte[] baser = System.Text.Encoding.ASCII.GetBytes(serial);
                    string baser0 = baser[0].ToString();
                    string baser1 = baser[1].ToString();
                    string baser2 = baser[2].ToString();
                    string baser3 = baser[3].ToString();
                    string baser4 = baser[4].ToString();
                    string baser5 = baser[5].ToString();
                    string baser6 = baser[6].ToString();
                    // string baser7 = baser[7].ToString();     //Jelena Ver67

                    int intbaser0 = Convert.ToInt32(baser0) % 10;
                    int intbaser1 = Convert.ToInt32(baser1) % 10;
                    int intibaser2 = Convert.ToInt32(baser2) % 10;
                    int intbaser3 = Convert.ToInt32(baser3) % 10;
                    int intbaser4 = Convert.ToInt32(baser4) % 10;
                    int intibaser5 = Convert.ToInt32(baser5) % 10;
                    int intbaser6 = Convert.ToInt32(baser6) % 10;
                    //int intbaser7 = Convert.ToInt32(baser7) % 10;  //Jelena Ver67

                    string intser0 = intbaser0.ToString();
                    string intser1 = intbaser1.ToString();
                    string intser2 = intibaser2.ToString();
                    string intser3 = intbaser3.ToString();
                    string intser4 = intbaser4.ToString();
                    string intser5 = intibaser5.ToString();
                    string intser6 = intbaser6.ToString();
                    // string intser7 = intbaser7.ToString();    //Jelena Ver67

                    string str = "h";
                    string hardser = Encrypt(str, true);

                    list.Add(intser0);
                    list.Add(intser1);
                    list.Add(intser2);
                    list.Add(intser3);
                    list.Add(intser4);
                    list.Add(intser5);
                    list.Add(intser6);

                    list.Add(hardser);
                    // list.Add(intser7);   //Jelena Ver67

                    StringBuilder builder = new StringBuilder();
                    foreach (string cat in list) // Loop through all strings
                    {
                        builder.Append(cat).Append(""); // Append string to StringBuilder
                    }
                    string result = builder.ToString();
                    deviceID = result;

                    try
                    {
                        string[] filePaths = Directory.GetFiles(location, "*.epr", SearchOption.TopDirectoryOnly);
                        if (filePaths.Length > 0)
                        {
                            string[] arr = filePaths[0].Split('\\');
                            string filename = arr[arr.Length - 1];
                            string strfpath = System.IO.Path.Combine(Install.projectsPath + @"\FoxSecConf\" + filename);
                            eprFilePath = strfpath;
                            if (!File.Exists(strfpath))
                            {
                                System.IO.Directory.CreateDirectory(Install.projectsPath + @"\FoxSecConf");
                                File.Copy(filePaths[0], strfpath);
                            }
                            else
                            {
                                File.Delete(strfpath);
                                File.Copy(filePaths[0], strfpath);
                            }
                        }
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex) { error = ex.ToString(); }
        }

        public static string IsApplictionInstalled(string p_name)
        {
            string displayName;
            RegistryKey key;
            string location = "";

            // search in: LocalMachine_32
            key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Classes\Installer\Products");
            foreach (String keyName in key.GetSubKeyNames())
            {
                RegistryKey subkey = key.OpenSubKey(keyName + @"\SourceList");
                //subkey = subkey + @"\SourceList";

                displayName = subkey.GetValue("PackageName") as string;
                if (p_name.Equals(displayName, StringComparison.OrdinalIgnoreCase) == true)
                {
                    string path = subkey.GetValue("LastUsedSource") as string;
                    string[] arr = path.Split(';');
                    location = arr[arr.Length - 1];
                }
            }
            return location;
        }

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            //   string key = (string)settingsReader.GetValue(ENCRYPTION_KEY, typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
    }
}



