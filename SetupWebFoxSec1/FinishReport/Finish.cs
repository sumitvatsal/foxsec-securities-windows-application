﻿using SetupWebSetup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoxSecWebSetup
{

    public partial class Finish : Form
    {
        public DateTime saveNow = DateTime.Now;

        public static string lgcheck;
        public static string lgcheck1;
        public static string lgserv;
        public static string lgserv1;
        public static string lgcopy;
        public static string lgcopy1;
        public static string lgsql;
        public static string lgsql1;
        public static string lgdb;
        public static string lgdb1;
        public static string lglogin;
        public static string lglogin1;
        //public static string lgrole;
        //public static string lgrole1;
        public static string lgconf;
        public static string lgconf1;
        public static string lgiis;
        public static string lgiis1;
        public static string lgconf6;
        public static string lgconf61;
        
        public Finish()
        {
            InitializeComponent();
            string icon = "icon1.ico";
            this.Icon = new Icon(icon, 60, 60);
            this.Text = "FoxSecWeb Installer";
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
      
          //  DataTable dt = new DataTable();
            //dt.Columns.Add(lgcheck);
            //dt.Columns.Add("OK");
            //dt.Columns.Add(lgcheck1);
            //dt.Columns.Add("BAD");
            //dt.Columns.Add(lgserv);
            
            //dt.Columns.Add(lgserv1);
           // i = 0;


          

            dataGridView1.Columns.Add("", "");
         
            dataGridView1.Columns.Add("", "");
        
          
            if (lgcheck == null)
            { dataGridView1.Rows.Add(lgcheck1.ToString(), "Not installed"); }
            else { dataGridView1.Rows.Add(lgcheck.ToString(), "OK"); }

            if (lgserv == null)
            { dataGridView1.Rows.Add(lgserv1.ToString(), "Not installed"); }
            else { dataGridView1.Rows.Add(lgserv.ToString(), "OK");}

            if (lgdb == null)
            { dataGridView1.Rows.Add(lgdb1.ToString(), "OK"); }
            else { dataGridView1.Rows.Add(lgdb.ToString(), "OK"); }

            if (lgcopy == null)
            { dataGridView1.Rows.Add(lgcopy1.ToString(), "Not installed"); }
            else { dataGridView1.Rows.Add(lgcopy.ToString(), "OK"); }

            if (lgconf6 == null)
            { dataGridView1.Rows.Add(lgconf61.ToString(), "Not installed"); }
            else { dataGridView1.Rows.Add(lgconf6.ToString(), "OK"); }       

            if (lgsql == null)
            { dataGridView1.Rows.Add(lgsql1.ToString(), "Not installed"); }
            else { dataGridView1.Rows.Add(lgsql.ToString(), "OK"); }

            if (lglogin == null)
            { dataGridView1.Rows.Add(lglogin1.ToString(), "OK"); }
            else { dataGridView1.Rows.Add(lglogin.ToString(), "OK"); }

            if (lgconf == null)
            { dataGridView1.Rows.Add(lgconf1.ToString(), "OK"); }
            else { dataGridView1.Rows.Add(lgconf.ToString(), "OK"); }

            if (lgiis == null)
            { dataGridView1.Rows.Add(lgiis1.ToString(), "Not installed"); }
            else { dataGridView1.Rows.Add(lgiis.ToString(), "OK"); }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not yet complete. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }

        }
        private void button2_Click(object sender, EventArgs e)
        {
           
            try
            {

                ServiceController sc = new ServiceController("FoxSecWebService");

                if (sc.Status.Equals(ServiceControllerStatus.Stopped) | sc.Status.Equals(ServiceControllerStatus.StopPending))
                {
                   
                    sc.Start();
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString ());
                // ListBoxLog.Items.Add(DateAndTime.Now + " " + "FoxSecTamoService is not registred ,Run as administrator \"FoxSecTamoServiceInstall .bat\" and FoxSecTamo.exe.config  <add key=\"RunAsService\" value=\"Yes\" /> ");
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {

                ServiceController sc = new ServiceController("FoxSecOnline");

                if (sc.Status.Equals(ServiceControllerStatus.Stopped) | sc.Status.Equals(ServiceControllerStatus.StopPending))
                {

                    sc.Start();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                // ListBoxLog.Items.Add(DateAndTime.Now + " " + "FoxSecTamoService is not registred ,Run as administrator \"FoxSecTamoServiceInstall .bat\" and FoxSecTamo.exe.config  <add key=\"RunAsService\" value=\"Yes\" /> ");
            }
        }
    }
}

