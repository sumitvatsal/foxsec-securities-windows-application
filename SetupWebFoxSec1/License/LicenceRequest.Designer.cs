﻿namespace FoxSecWebSetup.License
{
    partial class LicenceRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtcompname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtpath = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SmartPhoneRegistrators = new System.Windows.Forms.Label();
            this.txtvalidto = new System.Windows.Forms.TextBox();
            this.txtterminal = new System.Windows.Forms.TextBox();
            this.txtvideo = new System.Windows.Forms.TextBox();
            this.txtvisitor = new System.Windows.Forms.TextBox();
            this.txttt = new System.Windows.Forms.TextBox();
            this.txtzone = new System.Windows.Forms.TextBox();
            this.txtdoor = new System.Windows.Forms.TextBox();
            this.txtcomp = new System.Windows.Forms.TextBox();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtuserreq = new System.Windows.Forms.TextBox();
            this.txtcompreq = new System.Windows.Forms.TextBox();
            this.txtdoorreq = new System.Windows.Forms.TextBox();
            this.txtzonereq = new System.Windows.Forms.TextBox();
            this.txtttreq = new System.Windows.Forms.TextBox();
            this.txtvisitorreq = new System.Windows.Forms.TextBox();
            this.txtvideoreq = new System.Windows.Forms.TextBox();
            this.txtterminalreq = new System.Windows.Forms.TextBox();
            this.rdbharddisk = new System.Windows.Forms.RadioButton();
            this.rdbflashdisk = new System.Windows.Forms.RadioButton();
            this.btnsendlicenserequest = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.validtoreq = new System.Windows.Forms.DateTimePicker();
            this.txtsmartphone = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Company Name:";
            // 
            // txtcompname
            // 
            this.txtcompname.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcompname.Location = new System.Drawing.Point(157, 6);
            this.txtcompname.Name = "txtcompname";
            this.txtcompname.Size = new System.Drawing.Size(330, 20);
            this.txtcompname.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Installation Path:";
            // 
            // txtpath
            // 
            this.txtpath.Enabled = false;
            this.txtpath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpath.Location = new System.Drawing.Point(157, 43);
            this.txtpath.Name = "txtpath";
            this.txtpath.ReadOnly = true;
            this.txtpath.Size = new System.Drawing.Size(330, 20);
            this.txtpath.TabIndex = 7;
            this.txtpath.Text = "C:\\FoxSec\\Web";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(20, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "FoxSec License";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(201, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Counts from existing license";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(395, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Required license count";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.SmartPhoneRegistrators);
            this.groupBox1.Controls.Add(this.txtvalidto);
            this.groupBox1.Controls.Add(this.txtterminal);
            this.groupBox1.Controls.Add(this.txtvideo);
            this.groupBox1.Controls.Add(this.txtvisitor);
            this.groupBox1.Controls.Add(this.txttt);
            this.groupBox1.Controls.Add(this.txtzone);
            this.groupBox1.Controls.Add(this.txtdoor);
            this.groupBox1.Controls.Add(this.txtcomp);
            this.groupBox1.Controls.Add(this.txtuser);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(25, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(354, 338);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(179, 75);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(138, 21);
            this.textBox1.TabIndex = 32;
            this.textBox1.Text = "0";
            // 
            // SmartPhoneRegistrators
            // 
            this.SmartPhoneRegistrators.AutoSize = true;
            this.SmartPhoneRegistrators.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SmartPhoneRegistrators.Location = new System.Drawing.Point(7, 75);
            this.SmartPhoneRegistrators.Name = "SmartPhoneRegistrators";
            this.SmartPhoneRegistrators.Size = new System.Drawing.Size(148, 15);
            this.SmartPhoneRegistrators.TabIndex = 31;
            this.SmartPhoneRegistrators.Text = "SmartPhoneRegistrators  ";
            // 
            // txtvalidto
            // 
            this.txtvalidto.Enabled = false;
            this.txtvalidto.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvalidto.Location = new System.Drawing.Point(179, 295);
            this.txtvalidto.Name = "txtvalidto";
            this.txtvalidto.ReadOnly = true;
            this.txtvalidto.Size = new System.Drawing.Size(138, 21);
            this.txtvalidto.TabIndex = 30;
            // 
            // txtterminal
            // 
            this.txtterminal.Enabled = false;
            this.txtterminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtterminal.Location = new System.Drawing.Point(179, 264);
            this.txtterminal.Name = "txtterminal";
            this.txtterminal.ReadOnly = true;
            this.txtterminal.Size = new System.Drawing.Size(138, 21);
            this.txtterminal.TabIndex = 28;
            this.txtterminal.Text = "0";
            // 
            // txtvideo
            // 
            this.txtvideo.Enabled = false;
            this.txtvideo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvideo.Location = new System.Drawing.Point(179, 232);
            this.txtvideo.Name = "txtvideo";
            this.txtvideo.ReadOnly = true;
            this.txtvideo.Size = new System.Drawing.Size(138, 21);
            this.txtvideo.TabIndex = 26;
            this.txtvideo.Text = "0";
            // 
            // txtvisitor
            // 
            this.txtvisitor.Enabled = false;
            this.txtvisitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvisitor.Location = new System.Drawing.Point(179, 199);
            this.txtvisitor.Name = "txtvisitor";
            this.txtvisitor.ReadOnly = true;
            this.txtvisitor.Size = new System.Drawing.Size(138, 21);
            this.txtvisitor.TabIndex = 24;
            this.txtvisitor.Text = "0";
            // 
            // txttt
            // 
            this.txttt.Enabled = false;
            this.txttt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttt.Location = new System.Drawing.Point(179, 167);
            this.txttt.Name = "txttt";
            this.txttt.ReadOnly = true;
            this.txttt.Size = new System.Drawing.Size(138, 21);
            this.txttt.TabIndex = 22;
            this.txttt.Text = "0";
            // 
            // txtzone
            // 
            this.txtzone.Enabled = false;
            this.txtzone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtzone.Location = new System.Drawing.Point(179, 133);
            this.txtzone.Name = "txtzone";
            this.txtzone.ReadOnly = true;
            this.txtzone.Size = new System.Drawing.Size(138, 21);
            this.txtzone.TabIndex = 20;
            this.txtzone.Text = "0";
            // 
            // txtdoor
            // 
            this.txtdoor.Enabled = false;
            this.txtdoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdoor.Location = new System.Drawing.Point(179, 105);
            this.txtdoor.Name = "txtdoor";
            this.txtdoor.ReadOnly = true;
            this.txtdoor.Size = new System.Drawing.Size(138, 21);
            this.txtdoor.TabIndex = 18;
            this.txtdoor.Text = "0";
            // 
            // txtcomp
            // 
            this.txtcomp.Enabled = false;
            this.txtcomp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcomp.Location = new System.Drawing.Point(179, 46);
            this.txtcomp.Name = "txtcomp";
            this.txtcomp.ReadOnly = true;
            this.txtcomp.Size = new System.Drawing.Size(138, 21);
            this.txtcomp.TabIndex = 17;
            this.txtcomp.Text = "0";
            // 
            // txtuser
            // 
            this.txtuser.Enabled = false;
            this.txtuser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuser.Location = new System.Drawing.Point(179, 19);
            this.txtuser.Name = "txtuser";
            this.txtuser.ReadOnly = true;
            this.txtuser.Size = new System.Drawing.Size(138, 21);
            this.txtuser.TabIndex = 16;
            this.txtuser.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(6, 295);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(51, 15);
            this.label14.TabIndex = 15;
            this.label14.Text = "Valid To";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 264);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 15);
            this.label13.TabIndex = 14;
            this.label13.Text = "Terminal";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 232);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(38, 15);
            this.label12.TabIndex = 13;
            this.label12.Text = "Video";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 199);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 15);
            this.label11.TabIndex = 12;
            this.label11.Text = "Visitors";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 167);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 15);
            this.label10.TabIndex = 11;
            this.label10.Text = "TimeAndAttendense";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 133);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 15);
            this.label9.TabIndex = 10;
            this.label9.Text = "Zones";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 105);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 15);
            this.label8.TabIndex = 9;
            this.label8.Text = "Doors";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 15);
            this.label7.TabIndex = 8;
            this.label7.Text = "Companies";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 15);
            this.label6.TabIndex = 7;
            this.label6.Text = "Users";
            // 
            // txtuserreq
            // 
            this.txtuserreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtuserreq.Location = new System.Drawing.Point(387, 111);
            this.txtuserreq.Name = "txtuserreq";
            this.txtuserreq.Size = new System.Drawing.Size(141, 20);
            this.txtuserreq.TabIndex = 17;
            this.txtuserreq.Text = "0";
            // 
            // txtcompreq
            // 
            this.txtcompreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcompreq.Location = new System.Drawing.Point(387, 138);
            this.txtcompreq.Name = "txtcompreq";
            this.txtcompreq.Size = new System.Drawing.Size(141, 20);
            this.txtcompreq.TabIndex = 18;
            this.txtcompreq.Text = "0";
            // 
            // txtdoorreq
            // 
            this.txtdoorreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdoorreq.Location = new System.Drawing.Point(389, 200);
            this.txtdoorreq.Name = "txtdoorreq";
            this.txtdoorreq.Size = new System.Drawing.Size(141, 20);
            this.txtdoorreq.TabIndex = 20;
            this.txtdoorreq.Text = "0";
            // 
            // txtzonereq
            // 
            this.txtzonereq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtzonereq.Location = new System.Drawing.Point(389, 228);
            this.txtzonereq.Name = "txtzonereq";
            this.txtzonereq.Size = new System.Drawing.Size(141, 20);
            this.txtzonereq.TabIndex = 21;
            this.txtzonereq.Text = "0";
            // 
            // txtttreq
            // 
            this.txtttreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtttreq.Location = new System.Drawing.Point(389, 262);
            this.txtttreq.Name = "txtttreq";
            this.txtttreq.Size = new System.Drawing.Size(141, 20);
            this.txtttreq.TabIndex = 22;
            this.txtttreq.Text = "0";
            // 
            // txtvisitorreq
            // 
            this.txtvisitorreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvisitorreq.Location = new System.Drawing.Point(389, 294);
            this.txtvisitorreq.Name = "txtvisitorreq";
            this.txtvisitorreq.Size = new System.Drawing.Size(141, 20);
            this.txtvisitorreq.TabIndex = 23;
            this.txtvisitorreq.Text = "0";
            // 
            // txtvideoreq
            // 
            this.txtvideoreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtvideoreq.Location = new System.Drawing.Point(389, 327);
            this.txtvideoreq.Name = "txtvideoreq";
            this.txtvideoreq.Size = new System.Drawing.Size(141, 20);
            this.txtvideoreq.TabIndex = 27;
            this.txtvideoreq.Text = "0";
            // 
            // txtterminalreq
            // 
            this.txtterminalreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtterminalreq.Location = new System.Drawing.Point(389, 359);
            this.txtterminalreq.Name = "txtterminalreq";
            this.txtterminalreq.Size = new System.Drawing.Size(141, 20);
            this.txtterminalreq.TabIndex = 29;
            this.txtterminalreq.Text = "0";
            // 
            // rdbharddisk
            // 
            this.rdbharddisk.AutoSize = true;
            this.rdbharddisk.Checked = true;
            this.rdbharddisk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbharddisk.Location = new System.Drawing.Point(25, 439);
            this.rdbharddisk.Name = "rdbharddisk";
            this.rdbharddisk.Size = new System.Drawing.Size(385, 17);
            this.rdbharddisk.TabIndex = 32;
            this.rdbharddisk.TabStop = true;
            this.rdbharddisk.Text = "License for hard disk  (you can\'t use this hard disk license for another server)";
            this.rdbharddisk.UseVisualStyleBackColor = true;
            // 
            // rdbflashdisk
            // 
            this.rdbflashdisk.AutoSize = true;
            this.rdbflashdisk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbflashdisk.Location = new System.Drawing.Point(25, 462);
            this.rdbflashdisk.Name = "rdbflashdisk";
            this.rdbflashdisk.Size = new System.Drawing.Size(318, 17);
            this.rdbflashdisk.TabIndex = 33;
            this.rdbflashdisk.TabStop = true;
            this.rdbflashdisk.Text = "License for flash disk (You can\'t remove flash disk from server)";
            this.rdbflashdisk.UseVisualStyleBackColor = true;
            this.rdbflashdisk.CheckedChanged += new System.EventHandler(this.rdbflashdisk_CheckedChanged);
            // 
            // btnsendlicenserequest
            // 
            this.btnsendlicenserequest.Location = new System.Drawing.Point(144, 494);
            this.btnsendlicenserequest.Name = "btnsendlicenserequest";
            this.btnsendlicenserequest.Size = new System.Drawing.Size(186, 23);
            this.btnsendlicenserequest.TabIndex = 34;
            this.btnsendlicenserequest.Text = "Send License Request Via E-mail";
            this.btnsendlicenserequest.UseVisualStyleBackColor = true;
            this.btnsendlicenserequest.Click += new System.EventHandler(this.btnsendlicenserequest_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(336, 494);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 23);
            this.button2.TabIndex = 35;
            this.button2.Text = "Open File Path";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(445, 494);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(85, 23);
            this.button3.TabIndex = 36;
            this.button3.Text = "Exit";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // validtoreq
            // 
            this.validtoreq.CustomFormat = "yyyy-MM-dd";
            this.validtoreq.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.validtoreq.Location = new System.Drawing.Point(387, 393);
            this.validtoreq.Name = "validtoreq";
            this.validtoreq.Size = new System.Drawing.Size(143, 20);
            this.validtoreq.TabIndex = 37;
            // 
            // txtsmartphone
            // 
            this.txtsmartphone.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsmartphone.Location = new System.Drawing.Point(387, 170);
            this.txtsmartphone.Name = "txtsmartphone";
            this.txtsmartphone.Size = new System.Drawing.Size(141, 20);
            this.txtsmartphone.TabIndex = 19;
            this.txtsmartphone.Text = "0";
            // 
            // LicenceRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 529);
            this.Controls.Add(this.txtsmartphone);
            this.Controls.Add(this.validtoreq);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.btnsendlicenserequest);
            this.Controls.Add(this.rdbflashdisk);
            this.Controls.Add(this.rdbharddisk);
            this.Controls.Add(this.txtterminalreq);
            this.Controls.Add(this.txtvideoreq);
            this.Controls.Add(this.txtvisitorreq);
            this.Controls.Add(this.txtttreq);
            this.Controls.Add(this.txtzonereq);
            this.Controls.Add(this.txtdoorreq);
            this.Controls.Add(this.txtcompreq);
            this.Controls.Add(this.txtuserreq);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtpath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtcompname);
            this.Name = "LicenceRequest";
            this.Text = "License";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtcompname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtpath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtvalidto;
        private System.Windows.Forms.TextBox txtterminal;
        private System.Windows.Forms.TextBox txtvideo;
        private System.Windows.Forms.TextBox txtvisitor;
        private System.Windows.Forms.TextBox txttt;
        private System.Windows.Forms.TextBox txtzone;
        private System.Windows.Forms.TextBox txtdoor;
        private System.Windows.Forms.TextBox txtcomp;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtuserreq;
        private System.Windows.Forms.TextBox txtcompreq;
        private System.Windows.Forms.TextBox txtdoorreq;
        private System.Windows.Forms.TextBox txtzonereq;
        private System.Windows.Forms.TextBox txtttreq;
        private System.Windows.Forms.TextBox txtvisitorreq;
        private System.Windows.Forms.TextBox txtvideoreq;
        private System.Windows.Forms.TextBox txtterminalreq;
        private System.Windows.Forms.RadioButton rdbharddisk;
        private System.Windows.Forms.RadioButton rdbflashdisk;
        private System.Windows.Forms.Button btnsendlicenserequest;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DateTimePicker validtoreq;
        private System.Windows.Forms.Label SmartPhoneRegistrators;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox txtsmartphone;
    }
}