﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoxSecWebSetup.License
{
    public partial class NotFound : Form
    {
        public NotFound()
        {
            InitializeComponent();
            string icon = "icon1.ico";
            this.Icon = new Icon(icon, 60, 60);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not yet complete. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void btngetlicense_Click(object sender, EventArgs e)
        {
            LicenceRequest gl = new LicenceRequest();
            gl.Show();
            this.Hide();
        }
    }
}
