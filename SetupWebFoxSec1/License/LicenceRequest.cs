﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.NetworkInformation;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Management;
using FoxSecWebLicense;
using System.IO;
using System.Diagnostics;

namespace FoxSecWebSetup.License
{
    public partial class LicenceRequest : Form
    {
        public static string deviceID = string.Empty;
        public static string licenseFilePath = string.Empty;
        private static object _lock = new object();
        public static string drivetype = string.Empty;
        public static string dblicenseFilePath = string.Empty;

        SqlConnection conn = new SqlConnection();
        public static string currentpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\");
        string connectionweb = DBConnectionSettings.ReadWebXMLConnectionString(currentpath, "FoxSecDBContext");
        public static string serviceName = "FoxSecWebService";
        ServiceController[] services = ServiceController.GetServices();

        public LicenceRequest()
        {
            InitializeComponent();
            validtoreq.Value = DateTime.Now.AddYears(50);
            validtoreq.Format = DateTimePickerFormat.Custom;
            validtoreq.CustomFormat = "yyyy-MM-dd";
            validtoreq.Value = DateTime.Today.AddYears(1000);
            getcompname();
            checkexistinglicense();
        }

        public void getcompname()
        {
            var currentpath = System.IO.Path.Combine(Install.projectsPath + @"\FoxSecConf\SecConf.ini");
            if (File.Exists(currentpath))
            {
                string[] lines = FileReader.ReadLines(currentpath);
                for (int i = 0; i < lines.Length; i++)
                {
                    string line = lines[i];
                    if (line.Length > 10)
                    {
                        if (line.Contains("Last project="))
                        {
                            string datacomnr = Searcher.TrimEnd(line, "Last project=");
                            string[] arr = datacomnr.Split('\\');
                            string compnname = arr[arr.Length - 1];
                            compnname = compnname.Split('.')[0];
                            txtcompname.Text = compnname;
                        }
                    }
                }
            }
            else
            {
                txtcompname.Text = "";
            }
        }

        public void checkexistinglicense()
        {
            try
            {
                bool flashstatus = false;
                string licpath = "";
                foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
                {
                    flashstatus = true;
                }
                var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
                if (service != null)
                {
                    try
                    {
                        conn.ConnectionString = connectionweb;
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("select Value,Legal,validto,Comments from Classificatorvalues where ClassificatorId=(select top 1 id from Classificators where description='Licence' or description='License')", conn);
                        DataTable dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();
                        string Users = "";
                        string Companies = "";
                        string Doors = "";
                        string Zones = "";
                        string Visitors = "";
                        string Video = "";
                        string Terminals = "";
                        string Timeandattendense = "";
                        string validto = "";
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["Value"].ToString() == "Users")
                            {
                                Users = Convert.ToString(dr["Legal"]);
                            }
                            if (dr["Value"].ToString() == "Companies")
                            {
                                Companies = dr["Legal"].ToString();
                            }
                            if (dr["Value"].ToString() == "Doors")
                            {
                                Doors = dr["Legal"].ToString();
                            }
                            if (dr["Value"].ToString() == "Zones")
                            {
                                Zones = dr["Legal"].ToString();
                            }
                            if (dr["Value"].ToString() == "Visitors")
                            {
                                Visitors = dr["Legal"].ToString();
                            }
                            if (dr["Value"].ToString() == "Video")
                            {
                                Video = dr["Legal"].ToString();
                            }
                            if (dr["Value"].ToString() == "Terminals")
                            {
                                Terminals = dr["Legal"].ToString();
                            }
                            if (dr["Value"].ToString() == "Time&attendense")
                            {
                                Timeandattendense = dr["Legal"].ToString();
                            }
                            if (!String.IsNullOrEmpty(dr["validto"].ToString()))
                            {
                                validto = Convert.ToDateTime(dr["validto"]).ToString("yyyy-MM-dd");
                            }

                            if (dr["Value"].ToString() == "Licence Path")
                            {
                                licpath = dr["Comments"].ToString();
                                dblicenseFilePath = licpath;
                            }
                        }
                        txtuser.Text = Users.ToString();
                        txtcomp.Text = Companies.ToString();
                        txtdoor.Text = Doors.ToString();
                        txtzone.Text = Zones.ToString();
                        txtvisitor.Text = Visitors.ToString();
                        txtvideo.Text = Video.ToString();
                        txtterminal.Text = Terminals.ToString();
                        txttt.Text = Timeandattendense.ToString();
                        txtvalidto.Text = validto.ToString();
                        txtpath.Text = licpath;
                        if (licpath.Split(':')[0].ToLower() + ":" == "c:")
                        {
                            licpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                        }
                        else
                        {
                            if (flashstatus == true)
                            {
                                FlashDriveList.Drive = licpath.Split(':')[0] + ":";
                            }
                            else
                            {
                                licpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                                FlashDriveList.Drive = "";
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (!string.IsNullOrEmpty(FlashDriveList.Drive))
                        {
                            licpath = FlashDriveList.Drive;
                        }
                        else
                        {
                            licpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                            FlashDriveList.Drive = "";
                        }
                    }
                }
                else
                {
                    licpath = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                }

                if (!string.IsNullOrEmpty(FlashDriveList.Drive))
                {
                    licpath = FlashDriveList.Drive;
                    rdbflashdisk.Checked = true;
                }

                txtpath.Text = licpath;
                rdbflashdisk.Enabled = flashstatus;
            }
            catch (Exception ex)
            {
                txtpath.Text = System.IO.Path.Combine(Install.projectsPath + @"\Web\FoxSecLicense.ini");
                SetupWebSetup.FileWriter.UpdateEventsLogFile(DateTime.Now + " Error occurred while checking licence: " + ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnsendlicenserequest_Click(object sender, EventArgs e)
        {
            try
            {
                bool chkinternet = CheckConnectivity();

                if (chkinternet == true)
                {
                    if (!String.IsNullOrEmpty(txtcompname.Text))
                    {
                        GetSerFlashDisk();
                        if (string.IsNullOrEmpty(deviceID))
                        {
                            GetSerHardDisk();
                        }

                        if (File.Exists(licenseFilePath))
                        {
                            File.Delete(licenseFilePath);
                        }

                        // Create a new file     
                        using (FileStream fs = File.Create(licenseFilePath))
                        {
                            fs.Dispose();
                        }

                        string compname = "CompanieName=" + txtcompname.Text;
                        string uniquecode = "CompUniqNr=" + deviceID;
                        string struser = "Users=" + txtuserreq.Text;
                        string strdoor = "Doors=" + txtdoorreq.Text;
                        string strzones = "Zones=" + txtzonereq.Text;
                        string comp = "Companies=" + txtcompreq.Text;
                        string strWorktime = "TimeAndAttendense=" + txtttreq.Text;
                        string strportal = "Terminals=" + txtterminalreq.Text;
                        string strvideo = "Video=" + txtvideoreq.Text;
                        string strvisitor = "Visitors=" + txtvisitorreq.Text;
                        string strValidTo = "ValidTo=" + validtoreq.Text;
                        string drivename = FlashDriveList.Drive + @"\FoxSecLicense.ini";
                        string smartphone = "SmartPhoneRegistrators=" + txtsmartphone.Text;
                        var txt = "";
                        dblicenseFilePath = "";
                        if (!String.IsNullOrEmpty(dblicenseFilePath))
                        {
                            StreamReader sr = new StreamReader(dblicenseFilePath);
                            txt = sr.ReadToEnd();
                            sr.Close();
                        }
                        string filepath = "LicenseDrive=" + txtpath.Text;
                        using (StreamWriter stream = new StreamWriter(licenseFilePath, false))
                        {
                            stream.WriteLine("[CountRequest]");
                            stream.WriteLine(filepath);
                            stream.WriteLine(compname);
                            stream.WriteLine(uniquecode);
                            stream.WriteLine(struser);
                            stream.WriteLine(smartphone);
                            stream.WriteLine(strdoor);
                            stream.WriteLine(strzones);
                            stream.WriteLine(comp);
                            stream.WriteLine(strWorktime);
                            stream.WriteLine(strportal);
                            stream.WriteLine(strvideo);
                            stream.WriteLine(strvisitor);
                            stream.WriteLine(strValidTo);
                            stream.Close();
                            stream.Dispose();
                        }

                        MailMessage mail = new MailMessage();
                        mail.To.Add("info@foxsec.eu");                        
                        mail.From = new MailAddress("info@foxsec.eu");
                        mail.Subject = "Foxsec License request from " + txtcompname.Text;
                        if (drivetype != "1")
                        {
                            drivename = System.IO.Path.Combine(Install.projectsPath + @"\Web\SecConf.ini");
                        }
                        mail.Body = "Please download attached license request file.";
                        System.Net.Mail.Attachment attachment;
                        attachment = new System.Net.Mail.Attachment(licenseFilePath);
                        mail.Attachments.Add(attachment);

                        SmtpClient smtp = new SmtpClient("smtp.zone.ee", 25);
                        smtp.UseDefaultCredentials = false;
                        smtp.EnableSsl = true;
                        smtp.Credentials = new System.Net.NetworkCredential("info@foxsec.eu", "2015InfoFoxSecmail");
                        smtp.Send(mail);
                        mail.Dispose();
                        smtp.Dispose();
                        var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
                        if (service != null)
                        {
                            string lic_path = "";
                            if (FlashDriveList.Drive == "")
                            {
                                lic_path = txtpath.Text;
                            }
                            else
                            {
                                lic_path = txtpath.Text + @"\FoxSecLicense.ini";
                            }
                            try
                            {
                                conn.ConnectionString = connectionweb;
                                conn.Open();
                                SqlCommand da = new SqlCommand("update ClassificatorValues set Comments='" + lic_path + "' where value='Licence Path' and ClassificatorId=(select top 1 id from Classificators where description='Licence' or description='License')", conn);
                                da.ExecuteNonQuery();
                                conn.Close();
                            }
                            catch
                            {

                            }
                        }
                        File.Delete(licenseFilePath);

                        MessageBox.Show("License request has been sent successfully..!!");
                        NotFound obj = new NotFound();
                        obj.Show();
                        this.Hide();
                    }
                    else
                    {
                        txtcompname.Focus();
                        MessageBox.Show("Please enter Company Name!!");
                    }
                }
                else
                {
                    MessageBox.Show("Please connect your device to Internet!!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Some error occured. Please try again later.");
            }
        }

        protected bool CheckConnectivity()
        {
            bool connectionExists = false;
            string ipAddress = "8.8.8.8";
            try
            {
                System.Net.NetworkInformation.Ping pingSender = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingOptions options = new System.Net.NetworkInformation.PingOptions();
                options.DontFragment = true;
                if (!string.IsNullOrEmpty(ipAddress))
                {
                    System.Net.NetworkInformation.PingReply reply = pingSender.Send(ipAddress);
                    connectionExists = reply.Status == System.Net.NetworkInformation.IPStatus.Success ? true : false;
                }
            }
            catch (PingException ex)
            {

            }
            return connectionExists;
        }

        public static void GetSerHardDisk()
        {
            try
            {
                licenseFilePath = System.AppDomain.CurrentDomain.BaseDirectory + "FoxSecLicense.ini";
                string serial = "";
                List<string> list = new List<string>();
                string model = "";
                ManagementObjectSearcher moSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
                long totalSize = 0;
                foreach (ManagementObject wmi_HD in moSearcher.Get())
                {
                    if (wmi_HD.Properties["InterfaceType"].Value.ToString() != "USB")
                    {
                        model = wmi_HD["Model"].ToString();  //Model Number
                        try
                        {
                            serial = wmi_HD.GetPropertyValue("SerialNumber").ToString();
                        }
                        catch
                        {
                            serial = identifier("Win32_NetworkAdapterConfiguration", "MacAddress");
                        }
                        totalSize += Convert.ToInt64(wmi_HD.Properties["Size"].Value.ToString());
                    }
                }

                byte[] ba = System.Text.Encoding.ASCII.GetBytes(model);
                string ba0 = ba[0].ToString();
                string ba1 = ba[1].ToString();
                string ba2 = ba[2].ToString();

                long intba0 = Convert.ToInt64(ba0) % 10;
                long intba1 = Convert.ToInt64(ba1) % 10;
                long intba2 = Convert.ToInt64(ba2) % 10;
                string intstrba0 = intba0.ToString();
                string intstrba1 = intba1.ToString();
                string intstrba2 = intba2.ToString();

                list.Add(intstrba0);
                list.Add(intstrba1);
                list.Add(intstrba2);

                string name = identifier("Win32_LogicalDisk", "Name");

                //string Size = identifier("Win32_DiskDrive", "Size");
                string Size = Convert.ToString(totalSize);
                list.Add(Size.Substring(0, 5)); //Jelena Ver67          

                // string serial = identifier("Win32_DiskDrive", "SerialNumber");
                String numint = serial.Substring(0, 6); //Jelena Ver67

                byte[] baser = System.Text.Encoding.ASCII.GetBytes(serial);
                string baser0 = baser[0].ToString();
                string baser1 = baser[1].ToString();
                string baser2 = baser[2].ToString();
                string baser3 = baser[3].ToString();
                string baser4 = baser[4].ToString();
                string baser5 = baser[5].ToString();
                string baser6 = baser[6].ToString();
                // string baser7 = baser[7].ToString();     //Jelena Ver67

                int intbaser0 = Convert.ToInt32(baser0) % 10;
                int intbaser1 = Convert.ToInt32(baser1) % 10;
                int intibaser2 = Convert.ToInt32(baser2) % 10;
                int intbaser3 = Convert.ToInt32(baser3) % 10;
                int intbaser4 = Convert.ToInt32(baser4) % 10;
                int intibaser5 = Convert.ToInt32(baser5) % 10;
                int intbaser6 = Convert.ToInt32(baser6) % 10;
                //int intbaser7 = Convert.ToInt32(baser7) % 10;  //Jelena Ver67

                string intser0 = intbaser0.ToString();
                string intser1 = intbaser1.ToString();
                string intser2 = intibaser2.ToString();
                string intser3 = intbaser3.ToString();
                string intser4 = intbaser4.ToString();
                string intser5 = intibaser5.ToString();
                string intser6 = intbaser6.ToString();
                // string intser7 = intbaser7.ToString();    //Jelena Ver67

                string str = "h";
                string hardser = Encrypt(str, true);

                list.Add(intser0);
                list.Add(intser1);
                list.Add(intser2);
                list.Add(intser3);
                list.Add(intser4);
                list.Add(intser5);
                list.Add(intser6);

                list.Add(hardser);
                // list.Add(intser7);   //Jelena Ver67

                StringBuilder builder = new StringBuilder();
                foreach (string cat in list) // Loop through all strings
                {
                    builder.Append(cat).Append(""); // Append string to StringBuilder
                }
                string result = builder.ToString();
                deviceID = result;
            }
            catch (Exception ex) { }
        }

        public static void GetSerFlashDisk()
        {
            string diskName = string.Empty;
            string testser = string.Empty;
            var numint = string.Empty;
            StringBuilder volumename = new StringBuilder(256);
            try
            {
                foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
                {

                    foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
                    {

                        foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
                        {
                            string seldiskName = disk["Name"].ToString().Trim();
                            if (seldiskName == FlashDriveList.Drive)
                            {
                                diskName = disk["Name"].ToString().Trim();
                                testser = disk["VolumeSerialNumber"].ToString().Trim();
                                numint = Convert.ToInt64(testser, 16).ToString();
                                licenseFilePath = Path.Combine(diskName + @"\FoxSecLicense.ini");
                                drivetype = "1";
                                if (partition != null)
                                {
                                    // associate partitions with logical disks (drive letter volumes)
                                    ManagementObject logical = new ManagementObjectSearcher(String.Format(
                                        "associators of {{Win32_DiskPartition.DeviceID='{0}'}} where AssocClass = Win32_LogicalDiskToPartition",
                                        partition["DeviceID"])).First();

                                    if (logical != null)
                                    {
                                        List<string> list = new List<string>();

                                        ManagementObject volume = new ManagementObjectSearcher(String.Format(
                                            "select FreeSpace, Size from Win32_LogicalDisk where Name='{0}'",
                                            logical["Name"])).First();

                                        UsbDisk diskn = new UsbDisk(logical["Name"].ToString());

                                        string pnpdeviceid = parseSerialFromDeviceID(drive["PNPDeviceID"].ToString().Trim());
                                        //   listBox1.Items.Add("pnpdeviceid=" + pnpdeviceid);
                                        var conpnp = pnpdeviceid.Substring(0, 5);

                                        var conpnpn = converttoascii(conpnp);
                                        var pnpdevidint = Convert.ToUInt64(conpnpn, 16).ToString();

                                        list.Add(pnpdevidint.Substring(0, 4));

                                        diskn.Size = (ulong)volume["Size"];
                                        string size = diskn.Size.ToString();
                                        size = volume["Size"].ToString();
                                        list.Add(size.Substring(0, 4));

                                        list.Add(numint.Substring(0, 7));

                                        string str = "f";
                                        string flashser = Encrypt(str, true);
                                        list.Add(flashser);

                                        StringBuilder builder = new StringBuilder();
                                        foreach (string cat in list)
                                        {
                                            builder.Append(cat).Append("");
                                        }
                                        string result = builder.ToString();

                                        deviceID = result;
                                    }
                                }

                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public static string converttoascii(string text)
        {
            string asciitxt = "";
            for (var i = 0; i < text.Length; i++)
            {
                char current = text[i];
                if (!(Char.IsDigit(current)))
                {
                    asciitxt = asciitxt + Convert.ToString((int)(current));
                }
                else
                {
                    asciitxt = asciitxt + current;
                }
            }
            return asciitxt;
        }
        private static string parseSerialFromDeviceID(string deviceId)
        {
            string[] splitDeviceId = deviceId.Split('\\');
            string[] serialArray; string serial;
            int arrayLen = splitDeviceId.Length - 1;
            serialArray = splitDeviceId[arrayLen].Split('&');
            serial = serialArray[0]; return serial;
        }
        public static string identifier(string wmiClass, string wmiProperty)
        //Return a hardware identifier
        {
            string result = "";
            System.Management.ManagementClass mc = new System.Management.ManagementClass(wmiClass);
            System.Management.ManagementObjectCollection moc = mc.GetInstances();
            foreach (System.Management.ManagementObject mo in moc)
            {
                //Only get the first one
                if (result == "")
                {
                    try
                    {
                        result = mo[wmiProperty].ToString();
                        break;
                    }
                    catch
                    {
                    }
                }
            }
            return result;
        }

        public const string ENCRYPTION_KEY = "A456E4DA104F960563A66DDC";
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            System.Configuration.AppSettingsReader settingsReader = new AppSettingsReader();
            // Get the key from config file
            //   string key = (string)settingsReader.GetValue(ENCRYPTION_KEY, typeof(String));
            //System.Windows.Forms.MessageBox.Show(key);
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY));
                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(ENCRYPTION_KEY);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            tdes.Key = keyArray;
            tdes.Mode = CipherMode.ECB;
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            tdes.Clear();
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                string drv = (txtpath.Text).Replace(@"\FoxSecLicense.ini", "");
                Process.Start(drv);
            }
            catch
            {
                MessageBox.Show("Requested path not found!!");
            }
        }

        private void rdbflashdisk_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbflashdisk.Checked == true)
            {
                if (string.IsNullOrEmpty(FlashDriveList.Drive))
                {
                    FlashDriveList obj = new FlashDriveList();
                    obj.Show();
                    this.Hide();
                }
            }
            else
            {
                FlashDriveList.Drive = "";
                txtpath.Text = @"C:\FoxSec\Web";
            }
        }
    }
}
