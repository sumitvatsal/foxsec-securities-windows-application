﻿using FoxSecWebLicense;
using Ionic.Zip;
using Microsoft.Win32;
using SetupWebSetup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Management.Automation;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoxSecWebSetup.License
{
    public partial class Activation : Form
    {
        private static bool appIsLocked = true;
        public static string deviceID = string.Empty;
        private const RegexOptions regexOptions =
        RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.ExplicitCapture;
        private const string dateRegexp = "(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])";
        private static readonly Regex dateRegex =
        new Regex(dateRegexp, regexOptions);
        private const string digitsRegexPattern = @"^[0-9]+$";
        private static readonly Regex digitsRegex = new Regex(digitsRegexPattern, regexOptions);
        private static readonly string CR = Environment.NewLine;
        public static string serialnember = string.Empty;
        public static string licenseFilePath = "";
        public const string ENCRYPTION_KEY = "A456E4DA104F960563A66DDC";
        private static string appPath = string.Empty;
        public Activation()
        {
            InitializeComponent();
            string icon = "icon1.ico";
            this.Icon = new Icon(icon, 60, 60);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            checkcertexist();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Check.IISstart();
            //Check.GetWinName();
            this.Hide();
            try
            {
                string appPath = AppDomain.CurrentDomain.BaseDirectory;
                string zipPath = System.IO.Path.Combine(appPath + "SQLTEST.zip");

                string extractPath = Install.projectsPath;

                using (Ionic.Zip.ZipFile zip = Ionic.Zip.ZipFile.Read(zipPath))
                {
                    zip.ExtractAll(extractPath, ExtractExistingFileAction.OverwriteSilently);
                }
            }
            catch (Exception ex)
            {
                SetupWebSetup.FileWriter.UpdateEventsLogFile(DateTime.Now + " Error extract Portal.zip " + ex.Message);
            }
            try
            {

                ProcessStartInfo procInfo = new ProcessStartInfo();

                procInfo.UseShellExecute = true;

                procInfo.FileName = @"install.bat";  //The file in that DIR.

                //procInfo.WorkingDirectory = @"C:\SQLTEST\"; //The working DIR.
                procInfo.WorkingDirectory = Path.Combine(Install.projectsPath + @"\SQLTEST\");

                procInfo.Verb = "runas";

                Process.Start(procInfo);  //Start that process.

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            Check.IISstart();
            Check.GetWinName();
            Install.DB();
            this.Hide();
            Sql form2 = new Sql();
            form2.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not yet complete. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        public void checkcertexist()
        {
            if (System.IO.Directory.Exists(@"C:\Program Files (x86)\Windows Kits\"))
            {
                foreach (string subDir in Directory.GetDirectories(@"C:\Program Files (x86)\Windows Kits\", "*", SearchOption.AllDirectories))
                {
                    if (File.Exists(Path.Combine(subDir, "makecert.exe")))
                        groupBox1.Hide();
                }
            }
            else
            {
                groupBox1.Show();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string basefilepath = System.AppDomain.CurrentDomain.BaseDirectory + "windowssdksetup.exe";
            Process process = Process.Start(basefilepath);
            int id = process.Id;
            Process tempProc = Process.GetProcessById(id);
            this.Visible = false;
            tempProc.WaitForExit();
            this.Visible = true;
        }
    }
}
