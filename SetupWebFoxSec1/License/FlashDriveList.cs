﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoxSecWebSetup.License
{
    public partial class FlashDriveList : Form
    {
        public static string Drive { get; set; }
        public FlashDriveList()
        {
            InitializeComponent();
            GetFlashDriveList();
        }

        public class DiskName
        {
            public string DriveName { get; set; }
        }

        public void GetFlashDriveList()
        {
            dataGridView1.AutoGenerateColumns = true;
            string[] arr = new string[] { };
            List<DiskName> disklist = new List<DiskName>();

            foreach (ManagementObject drive in new ManagementObjectSearcher("select * from Win32_DiskDrive where InterfaceType='USB'").Get())
            {
                foreach (System.Management.ManagementObject partition in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskDrive.DeviceID='" + drive["DeviceID"] + "'} WHERE AssocClass = Win32_DiskDriveToDiskPartition").Get())
                {

                    foreach (System.Management.ManagementObject disk in new System.Management.ManagementObjectSearcher("ASSOCIATORS OF {Win32_DiskPartition.DeviceID='" + partition["DeviceID"] + "'} WHERE AssocClass = Win32_LogicalDiskToPartition").Get())
                    {
                        string drivename = disk["Name"].ToString().Trim();
                        disklist.Add(new DiskName()
                        {
                            DriveName = drivename,
                        });
                    }
                }
            }

            DataGridViewButtonColumn button = new DataGridViewButtonColumn();
            {
                button.Name = "Select";
                button.HeaderText = "Select";
                button.Text = "Select";
                button.UseColumnTextForButtonValue = true; 
                this.dataGridView1.Columns.Add(button);
            }
            dataGridView1.DataSource = disklist;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridView1.Columns["Select"].Index)
            {
                int ind = e.ColumnIndex;
                int rowind = e.RowIndex;
                Drive = dataGridView1.CurrentRow.Cells["DriveName"].Value.ToString(); 
            }
            LicenceRequest obj1 = new LicenceRequest();
            this.Close();
            obj1.Show();
        }
    }
}
