﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace FoxSecWebSetup
{
    [RunInstaller(true)]
    public partial class InstallerClass : System.Configuration.Install.Installer
    {
        //public InstallerClass()
        //{
        //    InitializeComponent();
        //}

        public InstallerClass()
      : base()
        {
            // Attach the 'Committed' event.
            this.Committed += new InstallEventHandler(MyInstaller_Committed);
            // Attach the 'Committing' event.
            this.Committing += new InstallEventHandler(MyInstaller_Committing);
          //this. += new InstallEventHandler(MyInstaller_AfterInstall);
        }

        // Event handler for 'Committing' event.
        private void MyInstaller_Committing(object sender, InstallEventArgs e)
        {
            //Console.WriteLine("");
            //Console.WriteLine("Committing Event occurred.");
            //Console.WriteLine("");
        }

        // Event handler for 'Committed' event.
        private void MyInstaller_Committed(object sender, InstallEventArgs e)
        {
            try
            {

                Directory.SetCurrentDirectory(Path.GetDirectoryName
                (Assembly.GetExecutingAssembly().Location));
                Process.Start(Path.GetDirectoryName(
                  Assembly.GetExecutingAssembly().Location) + @"\FoxSecWebSetup.exe");
            }
            catch
            {
                // Do nothing... 
            }
        }

        // Override the 'Install' method.
        public override void Install(IDictionary savedState)
        {
            base.Install(savedState);
        }

        // Override the 'Commit' method.
        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
        }

        //public override void Install(System.Collections.IDictionary stateSaver)
        //{
        //    base.Install(stateSaver);
        //    RegistrationServices regsrv = new RegistrationServices();
        //    regsrv.RegisterAssembly(base.GetType().Assembly, AssemblyRegistrationFlags.SetCodeBase);
        //    // --------- adding installation directory to stateSaver ----------
        //    stateSaver.Add("myTargetDir", Context.Parameters["pDir"].ToString());
        //}


        //public override void Commit(System.Collections.IDictionary savedState)
        //{
        //    base.Commit(savedState);
        //    // 'messagebox.show("salam")
        //    string InstallAddress = savedState["myTargetDir"].ToString();
        //    RegistryKey regKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"software\pourab\Sanjande", true);
        //    regKey.SetValue("InstalledFolder", InstallAddress);
        //}

        // Override the 'Rollback' method.
        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }
    }
}
