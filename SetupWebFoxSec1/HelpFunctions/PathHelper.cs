﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSecWebSetup;
using System.Web.UI.WebControls;
namespace SetupWebSetup
{
    class PathHelper
    {
        public static string projectsPath = Install.projectsPath + @"\setup";

        static internal void UpdatePaths(string project)
        {
            projectsPath = Path.Combine(Path.Combine(projectsPath), project);
            VerifyProjectsPath(project);
        }

        static internal string VerifyProjectsPath(string file1)
        {

            string path1 = VerifyPath(projectsPath, file1);
            return path1;
        }

        private static string VerifyReportsPath(string file)
        {
            //string projectsPath = string.Empty;
            string path1 = Path.Combine(projectsPath, "Reports");
            path1 = VerifyPath(path1, file);
            return path1;
        }

        /// <summary>
        /// Creates the file.
        /// </summary>
        /// <param name="path">The path.</param>
        static internal void CreateFile(string path)
        {
            StreamWriter fs = File.CreateText(path);
            fs.Close();
        }

        static internal string VerifyPath(string path1, string file1)
        {
            if (!Directory.Exists(path1))
            {
                Directory.CreateDirectory(path1);
            }

            path1 = Path.Combine(path1, file1);

            if (file1 != string.Empty)
            {
                if (!File.Exists(path1))
                {
                    CreateFile(path1);
                }
            }
            return path1;
        }

        static internal string VerifyEventsLogFilePath()
        {
            string logsFileName = string.Format("Events_{0}.txt", DateTime.Now.ToString("yyyyMMdd"));
            string path = PathHelper.VerifyPath(projectsPath, logsFileName);
            return path;
        }

        static internal string VerifyInstallationLogPath()
        {
            string logsFileName = string.Format("InstallationLog_{0}.txt", DateTime.Now.ToString("yyyyMMdd"));
            string path = PathHelper.VerifyPath(projectsPath, logsFileName);
            return path;
        }
    }
}
