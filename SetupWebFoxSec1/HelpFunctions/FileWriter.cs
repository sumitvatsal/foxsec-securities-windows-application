﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SetupWebSetup
{
    class FileWriter
    {
        private static object l = new object();

        public static void UpdateEventsLogFile(string arg)
        {
            lock (l)
            {
                if (arg == "00:00:00  0")
                {
                    // Beep()
                }
                string path = PathHelper.VerifyEventsLogFilePath();
                bool append = true;
                WriteToFile(path, arg, append);
               
            }
           
        }

        public static void UpdateInstallationLogFile(string arg)
        {
            lock (l)
            {
                if (arg == "00:00:00  0")
                {
                    // Beep()
                }
                string path = PathHelper.VerifyInstallationLogPath();
                bool append = true;
                WriteToFile(path, arg, append);

            }

        }

        static internal void WriteToFile(string path, string arg, bool append)
        {
            lock (l)
            {
                StreamWriter sw = new StreamWriter(path, append);
                sw.WriteLine(arg);
                sw.Close();
            }
        }

        public static void UpdateInstallationErrorLogFile(string error)
        {
            lock (l)
            {
                string path1 = @"C:\FoxSec\setup\errorLog.txt";
                if (!File.Exists(path1))
                {
                    StreamWriter fs = File.CreateText(path1);
                    fs.Close();
                    StreamWriter sw = new StreamWriter(path1, false);
                    sw.WriteLine(error);
                    sw.Close();
                }
                else
                {
                    StreamReader sr = new StreamReader(path1);
                    var txt = sr.ReadToEnd();
                    sr.Close();
                    StreamWriter sw = new StreamWriter(path1, false);
                    sw.WriteLine(txt);
                    sw.WriteLine(error);
                    sw.Close();
                }
            }

        }
    }
}
