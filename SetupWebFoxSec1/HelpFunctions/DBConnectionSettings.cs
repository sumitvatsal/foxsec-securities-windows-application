﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using System.IO;
using SetupWebSetup;

namespace FoxSecWebSetup
{
    class DBConnectionSettings
    {
        public static DateTime saveNow = DateTime.Now;
        public static string ReadWebXMLConnectionString(string apppath, string key)
        {
            dynamic vastus = "";
            try
            {
                dynamic path3 = Path.Combine(apppath, "Web.config");

                if (File.Exists(path3))
                {
                    dynamic oConfig = OpenConfigFile(path3);
                    if ((oConfig.ConnectionStrings.ConnectionStrings[key] != null))
                    {
                        vastus = oConfig.ConnectionStrings.ConnectionStrings[key].ConnectionString;

                    }
                }

            }
            catch (Exception ex)
            {
                FileWriter.UpdateEventsLogFile(ex.Message);
            }
            return vastus;
        }
        public static Configuration OpenConfigFile(string configPath)
        {
            var configFile = new FileInfo(configPath);
            var vdm = new VirtualDirectoryMapping(configFile.DirectoryName, true, configFile.Name);
            var wcfm = new WebConfigurationFileMap();
            wcfm.VirtualDirectories.Add("/", vdm);
            return WebConfigurationManager.OpenMappedWebConfiguration(wcfm, "/");
        }
        public static string WriteWebXMLConnectionString(string apppath, string key, string connnectionstring)
        {
            dynamic vastus = "";
            try
            {
                
                dynamic path3 = Path.Combine(apppath, "Web.config");

                if (File.Exists(path3))
                {
                    dynamic oConfig = OpenConfigFile(path3);
                    if ((oConfig.ConnectionStrings.ConnectionStrings[key] != null))
                    {
                        oConfig.ConnectionStrings.ConnectionStrings[key].ConnectionString = connnectionstring;
                        oConfig.Save();

                    }
                }

            }
            catch (Exception ex)
            {
                FileWriter.UpdateEventsLogFile(saveNow + " " + ex.Message);
            }

            return connnectionstring;
        }
    }
}
