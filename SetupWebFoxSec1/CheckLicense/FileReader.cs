﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSecWebLicense
{
    class FileReader
    {
        private const string NEW_LINE = "\r\n";
        internal static String[] ReadLines(string path)
        {
            string[] lines = new string[0];
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path, Encoding.UTF8, true);
                string result = sr.ReadToEnd();
                sr.Close();
                lines = result.Replace(NEW_LINE, "\n").Split('\n');
            }
            return lines;
        }

        internal const string LICENSE_INI = "FoxSecLicense.ini";
        internal const string ALARM_WAV = "alarm.wav";
        internal const string KEYPAD_SETTINGS = "fsglobalkpd.xml";

        internal const string DEMO_LICENSE_TEMPLATE =
            "[Users]\r\nCompUniqNr={0}\r\nEncryptLicenceNr=Demo\r\n";

        internal const string KEYPAD_INITIAL_COMMON_XML_CONFIG =
            "<?xml version='1.0' encoding='utf-8'?>" +
            "<Settings>" +
            "<Buttons></Buttons>" +
            "<LastProject></LastProject>" +
            "<LastPerson></LastPerson>" +
            "<Language>2</Language>" +
            "</Settings>";
    }
}
