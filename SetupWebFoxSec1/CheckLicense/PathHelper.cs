﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FoxSecWebLicense
{
    class PathHelper
    {
        private static string licenseFilePath = string.Empty;
        private static string appPath = string.Empty;
        private static string projectsPath = string.Empty;
        private static string settingsPath = string.Empty;
        private static string commonSettingsPath = string.Empty;
        private static string alarmSoundPath = string.Empty;

        internal static void InitializePaths()
        {
            appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);

            if (appPath.Substring(0, 6).Equals("file:\\"))
                appPath = appPath.Substring(6);

            projectsPath = Path.Combine(appPath, "Projects");
            settingsPath = Path.Combine(appPath, "Settings");
            alarmSoundPath = Path.Combine(appPath, FileReader.ALARM_WAV);
            licenseFilePath = Path.Combine(appPath, FileReader.LICENSE_INI);
            commonSettingsPath = Path.Combine(appPath, FileReader.KEYPAD_SETTINGS);

            //must be run one time to ensure that fsglobalkpd.xml file exists in app root folder
            VerifyCommonSettingsPath();
        }
       
        internal static string VerifyCommonSettingsPath()
        {
            if (!File.Exists(commonSettingsPath))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(KEYPAD_INITIAL_COMMON_XML_CONFIG);
                CreateFile(commonSettingsPath);
                FileWriter.WriteXml(doc, commonSettingsPath);
            }
            return commonSettingsPath;
        }
      
        internal static string LicenseFilePath
        {
            get { return licenseFilePath; }
        }
        internal static void CreateFile(string path)
        {
            StreamWriter fs = File.CreateText(path);
            fs.Close();
        }
        private const string NEW_LINE = "\r\n";
        internal static String[] ReadLines(string path)
        {
            string[] lines = new string[0];
            if (File.Exists(path))
            {
                StreamReader sr = new StreamReader(path, Encoding.UTF8, true);
                string result = sr.ReadToEnd();
                sr.Close();
                lines = result.Replace(NEW_LINE, "\n").Split('\n');
            }
            return lines;
        }

        internal const string LICENSE_INI = "FoxSecLicense.ini";
        internal const string ALARM_WAV = "alarm.wav";
        internal const string KEYPAD_SETTINGS = "fsglobalkpd.xml";

        internal const string DEMO_LICENSE_TEMPLATE =
            "[Users]\r\nCompUniqNr={0}\r\nEncryptLicenceNr=Demo\r\n";

        internal const string KEYPAD_INITIAL_COMMON_XML_CONFIG =
            "<?xml version='1.0' encoding='utf-8'?>" +
            "<Settings>" +
            "<Buttons></Buttons>" +
            "<LastProject></LastProject>" +
            "<LastPerson></LastPerson>" +
            "<Language>2</Language>" +
            "</Settings>";
    }
}
