﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace FoxSecWebLicense
{
    public static class Searcher
    {
        public static ManagementObject First(this ManagementObjectSearcher searcher)
        {
            ManagementObject result = null;
            foreach (ManagementObject item in searcher.Get())
            {
                result = item;
                break;
            }
            return result;
        }
        public static void ExtractToDirectory(this ZipArchive archive, string destinationDirectoryName, bool overwrite)
        {
            try
            {
                if (!overwrite)
                {
                    archive.ExtractToDirectory(destinationDirectoryName);
                    return;
                }
                foreach (ZipArchiveEntry file in archive.Entries)
                {
                    string completeFileName = Path.Combine(destinationDirectoryName, file.FullName);
                    if (file.Name == "")
                    {// Assuming Empty for Directory
                        Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
                        continue;
                    }
                    file.ExtractToFile(completeFileName, true);
                }
            }
            catch (Exception ex)
            { }
        }
        //public static string ConvertStringToHex(string asciiString)
        //{
        //    string hex = "";
        //    foreach (char c in asciiString)
        //    {
        //        int tmp = c;
        //        hex += String.Format("{0:x2}", (uint)System.Convert.ToUInt32(tmp.ToString()));
        //    }
        //    return hex;
        //}

        public static string TrimEnd(this string input, string suffixToRemove)
        {
            while (input.StartsWith(suffixToRemove))
            {
                input = input.Substring(suffixToRemove.Length);
            }

            return input;

        }
    }
}
