﻿using FoxSecWebSetup;
using FoxSecWebSetup.License;
using SetupWebSetup;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using WindowsFormsApplication1;

namespace FoxSecWebLicense
{
    class FileWriter
    {
        private static object l = new object();
        internal static void WriteToFile(string path, string arg)
        {
            bool append = false;
            WriteToFile(path, arg, append);
        }
        internal static void WriteToFile(string path, string arg, bool append)
        {
            lock (l)
            {
                StreamWriter sw = new StreamWriter(path, append);
                sw.WriteLine(arg);
                sw.Close();
            }
        }

        internal static void WriteDemoLicense(string sernum)
        {
            string licenseFilePath;
            if (Program.Repair==true)
            {
                 licenseFilePath = Change.licenseFilePath;
            }
            else 
            {   
                 licenseFilePath = Welcome.licenseFilePath;
            }
          
            string backup = licenseFilePath + ".copy";
            if (File.Exists(backup))
                File.Delete(backup);
            if (File.Exists(licenseFilePath))
                File.Move(licenseFilePath, backup);
            string data = String.Format(FileReader.DEMO_LICENSE_TEMPLATE, sernum);
            FileWriter.WriteToFile(licenseFilePath, data);
        }

        internal static void WriteXml(XmlDocument doc, string path)
        {
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
            fs.Position = 0;
            fs.SetLength(0);
            doc.Save(fs);
            fs.Close();
        }
    }
}
