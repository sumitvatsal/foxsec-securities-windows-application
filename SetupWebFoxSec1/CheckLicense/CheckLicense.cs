﻿using FoxSecWebSetup;
using FoxSecWebSetup.License;
using SetupWebSetup;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace FoxSecWebLicense
{
    class Spec
    {
        const string NEW_LINE = "\r\n";
        private static string licenseFilePath = "";
        public static string deviceID = string.Empty;
        public static string deviceIDflash = string.Empty;
        private static string error = string.Empty;

        public static int CheckLicenseFlash(string srno)
        {
            int licensed = 0;
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;

            try
            {
                string sernum;
                string[] lines;
                if (Program.Repair == true)
                {
                    sernum = Change.deviceID.PadRight(15, '0').Substring(0, 15);
                }
                else
                {
                    sernum = Welcome.deviceID.PadRight(15, '0').Substring(0, 15);
                }

                if (Program.Repair == true)
                {
                    lines = FileReader.ReadLines(Change.licenseFilePath);
                    licenseFilePath = Change.licenseFilePath;
                }
                else
                {
                    lines = FileReader.ReadLines(Welcome.licenseFilePath);
                    licenseFilePath = Welcome.licenseFilePath;
                }

                for (i = 0; i < lines.Length; i++)
                {
                    line = lines[i];
                    if (line.Length > 15)
                    {

                        if (string.Compare(line.Substring(0, 10), "CompUniqNr", true) == 0)
                        {
                            intUniqNrIdx = i;
                            strUniqNr = line.Substring(11, line.Length - 11);
                        }
                    }
                    if (intUniqNrIdx > 0)
                        break;
                }

                if (intUniqNrIdx > 0)
                {
                    licensed = checkusb(srno);
                }
                return licensed;
            }
            catch
            {
                licensed = 0;
            }
            return licensed;
        }
        /// <returns>Value of modified number</returns>
        internal static bool BitValue(UInt64 source, ushort bit)
        {
            return Convert.ToBoolean(source & (ulong)Math.Pow(2, bit));
        }
        internal static ulong ResetBit(UInt64 source, ushort bit)
        {
            if (BitValue(source, bit))
                source -= (ulong)Math.Pow(2, bit);
            return source;
        }
        internal static ulong SetBit(UInt64 source, ushort bit)
        {
            source |= (ulong)Math.Pow(2, bit);
            return source;
        }
        private static string DecryptLicense(string level, string pword)
        {
            int KasutajaLevelx12 = 0;
            int KasutajaLevelx11 = 0;
            int KasutajaLevelx13 = 0;
            int KasutajaLevelx14 = 0;
            string parool = null;
            int PswLen = 0;
            int Sala = 0;
            int[] AsciiK = new int[21];
            char[] See_chrx = new char[11];
            int j = 0;

            //
            KasutajaLevelx11 = Convert.ToInt32(level.Substring(0, 5), 16);
            KasutajaLevelx12 = Convert.ToInt32(level.Substring(5, 5), 16);
            KasutajaLevelx13 = Convert.ToInt32(level.Substring(10, 5), 16);
            KasutajaLevelx14 = Convert.ToInt32(level.Substring(15, 5), 16);

            if (pword.Length <= 10)
            {
                PswLen = pword.Length;
                parool = pword.Substring(0, 10);
            }
            else
            {
                PswLen = 10;
                parool = pword;
            }
            for (j = 1; j <= parool.Length; j++)
            {
                AsciiK[j] = System.Convert.ToInt32(parool[j - 1]);
                Sala = Sala + (((AsciiK[j] * j) % 300) * KasutajaLevelx14) % 8745 + 7 * ((((AsciiK[j] * 3 * j) % 73) * KasutajaLevelx13 + 9) % 117) + (((KasutajaLevelx12 + 87) % 32 + KasutajaLevelx11 + 7) % 69);
            }
            //Kasutame Parooli nüüd krypteeritud paroolina
            parool = "";
            for (j = 1; j <= 10; j++)
            {
                See_chrx[j] = Convert.ToChar(((Sala * (AsciiK[j] + AsciiK[1]) + PswLen * 18 * j) % 93) + 33);
                PswLen = Convert.ToInt32(See_chrx[j]);
                parool = parool + See_chrx[j];
            }
            return parool;
        }

        public static int checkusb(string srno)
        {
            int chk = 0;
            string disknr = srno;
            string licencedevid = Readlicensenr(licenseFilePath);
            if (String.IsNullOrEmpty(licencedevid))
            {
                chk = 0;//invalid licence
                if (File.Exists(licenseFilePath))
                {
                    File.Delete(licenseFilePath);
                }
            }
            else if (licencedevid == disknr)
            {
                chk = 1;//valid licence
            }
            else
            {
                chk = 0;//invalid licence
                        //chk = 1;
                if (File.Exists(licenseFilePath))
                {
                    File.Delete(licenseFilePath);
                }
            }
            return chk;
        }

        public static string Readlicensenr(string licenseFilePath)
        {
            string datacompnrhash = string.Empty;
            string line = string.Empty;
            string[] lines = FileReader.ReadLines(licenseFilePath);
            for (int i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (line.Contains("[CountRequest]"))
                {
                    break;
                }
                else
                {
                    //   TAtest.Readlicense();
                    if (line.Length > 15)
                    {
                        if (line.Contains("CompUniqNr"))
                        {
                            string datacomnr1 = Searcher.TrimEnd(line, "CompUniqNr=");

                            if (deviceIDflash == string.Empty)
                            {
                                string linedata = Searcher.TrimEnd(line, "CompUniqNr=");
                                datacompnrhash = linedata;
                            }
                            break;
                        }
                    }
                }
            }
            return datacompnrhash;
        }

        public static string ReadCompNr()
        {
            Int32 i = default(Int32);
            Int32 intUniqNrIdx = default(Int32);
            Int32 intEncryptLicNrIdx = default(Int32);
            string strUniqNr = string.Empty;
            string strEncryptLicNr = string.Empty;
            string decryptedLicense = string.Empty;
            string line = string.Empty;
            // string sername;
            string datavalidto = string.Empty;
            string cmpnr = string.Empty;
            string[] lines;
            if (Program.Repair == true)
            {
                lines = FileReader.ReadLines(Change.licenseFilePath);
                licenseFilePath = Change.licenseFilePath;
            }
            else
            {
                lines = FileReader.ReadLines(Welcome.licenseFilePath);
                licenseFilePath = Welcome.licenseFilePath;
            }

            for (i = 0; i < lines.Length; i++)
            {
                line = lines[i];
                if (line.Contains("[CountRequest]"))
                {
                    break;
                }
                else
                {
                    if (line.Length > 1)
                    {
                        if (line.Contains("CompUniqNr"))
                        {
                            string returndata = Searcher.TrimEnd(line, "CompUniqNr=");
                            cmpnr = returndata;
                        }
                    }
                }
            }
            return cmpnr;
        }
    }
}
