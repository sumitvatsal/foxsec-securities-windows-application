﻿using FoxSecWebLicense;
using SetupWebSetup;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FoxSecWebSetup.Conf
{
    public partial class InstallConf6 : Form
    {

        public InstallConf6()
        {
            InitializeComponent();
            this.button1.Enabled = false;

            //  istallConf();
        }

        public static void istallConf()
        {
            string appPath = AppDomain.CurrentDomain.BaseDirectory;
            try
            {
                System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo(Path.Combine(appPath + "setup.exe"));
                psi.WorkingDirectory = appPath;
                psi.RedirectStandardOutput = true;
                psi.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                psi.UseShellExecute = false;
                System.Diagnostics.Process listFiles;
                listFiles = System.Diagnostics.Process.Start(psi);

                //Sql.extractConfexe();
                //Sql.startConfexe();
                string msg = "FoxSecConf installed";
                Finish.lgconf6 = msg;

            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                string msg = "FoxSecConf not installed";
                Finish.lgconf61 = msg;
            }

        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            DialogResult dialogResult = MessageBox.Show("The installation is not yet complete. Are you sure you want to exit?", "FoxSecWeb Installer", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                e.Cancel = false;
                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
            }

        }
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();

            Sql.startConfexe();

            Finish form5 = new Finish();
            form5.Show();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Check form5 = new Check();
            form5.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            istallConf();
            this.button1.Enabled = true;
            this.button3.Enabled = false;
        }


    }
}

